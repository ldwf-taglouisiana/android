ALTER TABLE captures ADD COLUMN length DECIMAL;
ALTER TABLE edited_pending_captures ADD COLUMN length DECIMAL;
ALTER TABLE pending_captures ADD COLUMN length DECIMAL;