package gov.louisiana.ldwf.taglouisiana.gson.responses.publicApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.gson.responses.BaseResponse;
import gov.louisiana.ldwf.taglouisiana.models.options.ShirtSizeOption;

/**
 * Created by danielward on 8/25/16.
 */
public class ShirtSizeOptionsResponse extends BaseResponse {
    @Expose
    @SerializedName("items")
    public List<ShirtSizeOption> items;

    @Override
    public Class getModelClass() {
        return ShirtSizeOption.class;
    }

    public List<ShirtSizeOption> getItems() {
        if (items == null) {
            items = new ArrayList<>(0);
        }

        return items;
    }
}
