package gov.louisiana.ldwf.taglouisiana.adapters;

import android.support.annotation.Nullable;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.raizlabs.android.dbflow.list.FlowQueryList;

import java.io.IOException;

import gov.louisiana.ldwf.taglouisiana.models.BaseItem;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

/**
 * Created by danielward on 10/27/16.
 */

public abstract class FlowQueryListFilterableAdapter<T extends BaseItem> extends BaseAdapter implements Filterable{

    private FlowQueryList<T> initialItems;
    private FlowQueryList<T> filteredItems;

    private Filter filter;
    private FilterQueryConstraintRequest filterQueryContraintRequest;

    public FlowQueryListFilterableAdapter(FlowQueryList<T> items, FilterQueryConstraintRequest<T> filterQueryContraintRequest) {
        this.filterQueryContraintRequest = filterQueryContraintRequest;
        this.initialItems = items;
        this.filteredItems = null;
    }

    // =============================================================================================
    // BaseAdapter Overrides
    // =============================================================================================

    @Override
    public int getCount() {
        return currentList().getCount();
    }

    @Nullable
    @Override
    public T getItem(int position) {
        return currentList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return currentList().getItem(position).id();
    }


    // =============================================================================================
    // Filterable Overrides
    // =============================================================================================

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new FlowQueryListFilter<>(this, new FlowQueryListFilter.FlowQueryListFilterListener<T>() {
                @Override
                public void constraintEmpty() {
                    if (filteredItems != null) {
                        try {
                            filteredItems.close();
                        } catch (IOException e) {
                            Utils.logError(e);
                        }
                    }
                    filteredItems = null;
                }

                @Override
                public void resultsPublished(FlowQueryList<T> results) {
                    filteredItems = results;
                }
            }, filterQueryContraintRequest);
        }

        return filter;
    }

    // =============================================================================================
    // Helpers
    // =============================================================================================

    private FlowQueryList<T> currentList() {
        return filteredItems == null ? initialItems : filteredItems;
    }
}
