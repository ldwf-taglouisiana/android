package gov.louisiana.ldwf.taglouisiana.models.fishEvents.images;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Index;
import com.raizlabs.android.dbflow.annotation.NotNull;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.data.Blob;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.UUID;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.database.UUIDConverter;

/**
 * Created by danielward on 8/23/16.
 */
@Table(database = AppDatabase.class)
public class AttachedPhoto extends BaseModel {
    @PrimaryKey
    @Column(typeConverter = UUIDConverter.class)
    public UUID uuid;

    @Index
    @NotNull
    @Column(typeConverter = UUIDConverter.class)
    public UUID enteredPendingCaptureUuid;

    @NotNull
    @Column
    @SerializedName("raw_data")
    public Blob photoData;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AttachedPhoto) {
            return ((AttachedPhoto)obj).uuid.equals(this.uuid);
        } else {
            return super.equals(obj);
        }
    }
}
