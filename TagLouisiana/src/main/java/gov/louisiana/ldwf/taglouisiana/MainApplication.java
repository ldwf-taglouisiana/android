package gov.louisiana.ldwf.taglouisiana;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import io.fabric.sdk.android.Fabric;

public class MainApplication extends MultiDexApplication {


    private static Context context;

    public MainApplication() {
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // enable Crashlytics when not in debug
        if (!BuildConfig.BUILD_TYPE.equals("debug")) {
            Fabric.with(this, new Crashlytics());
        }

        // initialize the DBFLow db manager
        FlowManager.init(new FlowConfig.Builder(this).openDatabasesOnInit(true).build());

        // Setup the Iconify with the FontAwesome module
        Iconify.with(new FontAwesomeModule());

        // save the context to be accessed in other parts of the application
        context = getApplicationContext();
    }


    public static Context getAppContext() {
        return MainApplication.context;
    }

}
