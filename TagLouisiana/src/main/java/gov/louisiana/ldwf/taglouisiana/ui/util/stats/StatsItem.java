package gov.louisiana.ldwf.taglouisiana.ui.util.stats;

import android.support.annotation.Nullable;

import com.raizlabs.android.dbflow.annotation.NotNull;

import java.util.LinkedHashMap;

/**
 * Created by danielward on 9/30/16.
 */

public class StatsItem {
    public String title;
    public LinkedHashMap<KeyTitle, String> items;

    public StatsItem() {
        this.items =  new LinkedHashMap<>(3, 1.5f, false);
    }

    public StatsItem(String title) {
        this();
        this.title = title;
    }

    public static class Builder {
        private StatsItem item;

        public Builder() {
            this.item = new StatsItem();
        }

        public Builder(String title) {
            this.item = new StatsItem(title);
        }

        public Builder setTitle(String title) {
            this.item.title = title;
            return this;
        }

        public Builder setItem(String title, String value) {
            this.item.items.put(new KeyTitle(title), value);
            return this;
        }

        public Builder setItem(String title, String subTitle, String value) {
            this.item.items.put(new KeyTitle(title, subTitle), value);
            return this;
        }

        public Builder setItem(KeyTitle key, String value) {
            this.item.items.put(key, value);
            return this;
        }

        public StatsItem build() {
            return item;
        }
    }

    public static class KeyTitle {
        private String title;
        private String subTitle;

        public KeyTitle(@NotNull String title) {
            this(title, null);
        }

        public KeyTitle(@NotNull String title, @Nullable String subtitle) {
            this.title = title;
            this.subTitle = subtitle;
        }

        public String getTitle() {
            return title;
        }

        public String getSubTitle() {
            return subTitle;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            KeyTitle keyTitle = (KeyTitle) o;

            if (title != null ? !title.equals(keyTitle.title) : keyTitle.title != null)
                return false;
            return subTitle != null ? subTitle.equals(keyTitle.subTitle) : keyTitle.subTitle == null;

        }

        @Override
        public int hashCode() {
            int result = title != null ? title.hashCode() : 0;
            result = 31 * result + (subTitle != null ? subTitle.hashCode() : 0);
            return result;
        }
    }
}
