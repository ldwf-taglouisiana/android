package gov.louisiana.ldwf.taglouisiana.ui.species.detail;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseActivity;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;

/**
 * Created with IntelliJ IDEA.
 * User: daniel
 * Date: 2/20/13
 * Time: 1:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class SpeciesDetailActivity extends BaseActivity {

    public static final String SPECIES_ID_ARG =  "SpeciesDetailActivity.species_id";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContent(R.layout.species_detail_activity);

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        BaseFragment fragment = SpeciesDetailFragment.newInstance(getIntent().getLongExtra(SPECIES_ID_ARG, -1));
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, fragment);
        ft.commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
