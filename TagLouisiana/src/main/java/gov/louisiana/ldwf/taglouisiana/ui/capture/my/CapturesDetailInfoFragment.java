package gov.louisiana.ldwf.taglouisiana.ui.capture.my;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.gson.GsonUtil;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.AbstractFishEvent;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture_Table;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Recapture;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 9/19/14
 */
public class CapturesDetailInfoFragment extends BaseFragment {

    private static final String FISH_EVENT_CAPTURE_ID = "CapturesDetailInfoFragment.captures_id";
    private static final String FISH_EVENT_RECAPTURE_JSON = "CapturesDetailInfoFragment.recapture_json";

    private AbstractFishEvent fishEvent;

    public static CapturesDetailInfoFragment newInstance(Capture capture) {
        Bundle args = new Bundle();
        args.putLong(FISH_EVENT_CAPTURE_ID, capture._id);

        CapturesDetailInfoFragment fragment = new CapturesDetailInfoFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public static CapturesDetailInfoFragment newInstance(Recapture recapture) {
        Bundle args = new Bundle();
        args.putString(FISH_EVENT_RECAPTURE_JSON, GsonUtil.gson(false).toJson(recapture));

        CapturesDetailInfoFragment fragment = new CapturesDetailInfoFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initCapture();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.capture_detail_info_fragment, container, false);

        AQuery aQuery = new AQuery(view);

        if (this.fishEvent instanceof Recapture) {
            Recapture recapture = ((Recapture)this.fishEvent);
            aQuery.id(R.id.recapture).visible();
            aQuery.id(R.id.angler).text(recapture.anglerName);

            aQuery.id(R.id.travel_distance).text(formatMetersToMiles(recapture.distaneFromCaptureMeters)).visible();
            aQuery.id(R.id.travel_distance_title).visible();

            if (recapture.getRecaptureDispositionOption() != null) {
                aQuery.id(R.id.disposition).text(recapture.getRecaptureDispositionOption().description).visible();
                aQuery.id(R.id.disposition_title).visible();
            }
        }

        aQuery.id(R.id.date).text(Constants.US_DATE_FORMATTER.format(this.fishEvent.date));
        aQuery.id(R.id.species).text(this.fishEvent.getSpecies().commonName);
        aQuery.id(R.id.length).text(this.fishEvent.getLengthValue());

        if (this.fishEvent.getTimeOfDayOption() != null) {
            aQuery.id(R.id.time).text(this.fishEvent.getTimeOfDayOption().description);
        }

        if (this.fishEvent.locationDescription != null) {
            aQuery.id(R.id.location).text(this.fishEvent.locationDescription);
        }

        if (!this.fishEvent.hasShowableLocation()) {
            aQuery.id(R.id.no_location_provided).text(getString(R.string.location_withheld)).visible();

        } else if (!this.fishEvent.hasValidLocation()) {
            aQuery.id(R.id.no_location_provided).visible();

        }

        return view;
    }


    @Override
    public String getTitle() {
        initCapture();

        return Constants.US_DATE_FORMATTER.format(this.fishEvent.date);
    }

    private void initCapture() {
        if (getArguments() != null && this.fishEvent == null) {

            if (getArguments().getLong(FISH_EVENT_CAPTURE_ID, -1) != -1) {
                this.fishEvent = SQLite.select()
                        .from(Capture.class)
                        .where(Capture_Table._id.eq(getArguments().getLong(FISH_EVENT_CAPTURE_ID)))
                        .querySingle();
            }

            if (getArguments().getString(FISH_EVENT_RECAPTURE_JSON, null) != null) {
                this.fishEvent = GsonUtil.gson()
                        .fromJson(getArguments().getString(FISH_EVENT_RECAPTURE_JSON), Recapture.class);
            }
        }

        if (this.fishEvent == null) {
            throw new IllegalArgumentException("Use the newInstance() method for this class");
        }
    }

    private String formatMetersToMiles(double meters) {
        double inches = (39.370078f* meters);
        double miles =  (inches / 63360);

        return  Constants.DECIMAL_FORMATTER.format(miles) + " miles";
    }
}
