package gov.louisiana.ldwf.taglouisiana.utils;

import android.content.Context;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import gov.louisiana.ldwf.taglouisiana.MainApplication;
import gov.louisiana.ldwf.taglouisiana.R;

public class Common {

  public static String getAuthToken(Context context) {
    String token = context.getSharedPreferences(Constants.SHARED_PREFS_TAG_LOUISIANA, Context.MODE_PRIVATE)
            .getString(Constants.SHARED_PREFS_TAG_LOUISIANA_AUTH_TOKEN, "");

    return token;
  }

  public static String getDeviceId(Context context) {
    String device = context.getSharedPreferences(Constants.SHARED_PREFS_TAG_LOUISIANA, Context.MODE_PRIVATE)
            .getString(Constants.SHARED_PREFS_TAG_LOUISIANA_DEVICE_ID, "");

    return device;
  }

  /**
   * From http://www.mkyong.com/java/java-sha-hashing-example/
   *
   * @return The hash of the Application ID and Secret to authenticate the application.
   * @throws NoSuchAlgorithmException
   */
  public static String getOauthHash() throws NoSuchAlgorithmException {

    String clientId = MainApplication.getAppContext().getResources().getString(R.string.client_id);
    String clientSecret = MainApplication.getAppContext().getResources().getString(R.string.client_secret);

    MessageDigest md = MessageDigest.getInstance("SHA-256");
    md.update(clientId.getBytes());
    md.update(clientSecret.getBytes());

    byte byteData[] = md.digest();

    //convert the byte to hex format method 1
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < byteData.length; i++) {
      sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
    }

    return sb.toString();
  }
}
