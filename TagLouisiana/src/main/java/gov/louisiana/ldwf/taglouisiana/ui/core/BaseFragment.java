package gov.louisiana.ldwf.taglouisiana.ui.core;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;

import com.raizlabs.android.dbflow.annotation.NotNull;
import com.squareup.otto.Subscribe;

import gov.louisiana.ldwf.taglouisiana.event_bus.ActivityResultBus;
import gov.louisiana.ldwf.taglouisiana.event_bus.ActivityResultEvent;

/**
 * Created with IntelliJ IDEA.
 * User: danielward
 * Date: 2/21/13
 * Time: 3:16 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseFragment extends Fragment {

    private BaseActivity activity;
    private View contentView;

    private boolean hasTabs;


    private Object mActivityResultSubscriber = new Object() {
        @Subscribe
        public void onActivityResultReceived(ActivityResultEvent event) {
            int requestCode = event.getRequestCode();
            int resultCode = event.getResultCode();
            Intent data = event.getData();
            onActivityResult(requestCode, resultCode, data);
        }
    };


    public BaseFragment() {
        this.hasTabs = false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (BaseActivity) context;
    }

    @Override
    public void onStart() {
        super.onStart();
        ActivityResultBus.getInstance().register(mActivityResultSubscriber);
    }

    @Override
    public void onStop() {
        super.onStop();
        ActivityResultBus.getInstance().unregister(mActivityResultSubscriber);
    }

    protected BaseActivity getBaseActivity() {
        return this.activity;
    }

    public boolean hasTabs() {
        return this.hasTabs;
    }

    public void setHasTabs() {
        this.setHasTabs(true);
    }

    public void setHasTabs(boolean hasTabs) {
        this.hasTabs = hasTabs;
    }

    public String getTitle() {
        return "";
    }

    public void setContentView(@NotNull View contentView) {
        this.contentView = contentView;
    }

    public View getContentView() {
        if (getView() == null) {
            return contentView;
        } else {
            return getView();
        }
    }
}
