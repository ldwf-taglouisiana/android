package gov.louisiana.ldwf.taglouisiana.gson.typeAdapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.io.ByteArrayOutputStream;

import gov.louisiana.ldwf.taglouisiana.models.fishEvents.images.AttachedPhoto;

/**
 * Created by danielward on 8/24/16.
 */
public class AttachedPhotoTypeAdapterFactory extends BeforeAfterTypeAdapterFactory<AttachedPhoto> {

    public AttachedPhotoTypeAdapterFactory() {
        super(AttachedPhoto.class);
    }

    @Override
    protected void beforeWrite(AttachedPhoto source, JsonElement toSerialize) {
        JsonObject custom = toSerialize.getAsJsonObject();

        Bitmap bm = BitmapFactory.decodeByteArray(source.photoData.getBlob(), 0, source.photoData.getBlob().length);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();

        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

        custom.add("raw_data", new JsonPrimitive(encodedImage));
    }

}
