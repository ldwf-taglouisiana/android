package gov.louisiana.ldwf.taglouisiana.models.timeEntry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;

import java.util.Date;
import java.util.UUID;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.database.UUIDConverter;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/28/14
 */
@Table(database = AppDatabase.class)
public class EnteredTimeOnWaterEntry extends BaseModel {

    @PrimaryKey
    @Column(typeConverter = UUIDConverter.class)
    @Expose
    @SerializedName("uuid")
    public UUID uuid;

    @Expose
    @SerializedName("end_date")
    @Column
    public Date endDate;

    @Expose
    @SerializedName("start_date")
    @Column
    public Date startDate;

    @Expose
    @SerializedName("verified")
    @Column
    public Boolean verified;

    @Override
    public void save() {
        setDefaults();
        super.save();
    }

    @Override
    public void save(DatabaseWrapper databaseWrapper) {
        setDefaults();
        super.save(databaseWrapper);
    }

    private void setDefaults() {
        // if the UUID isn't set yet, then create a new rrandom value
        if (this.uuid == null || "".equals(this.uuid.toString())) {
            this.uuid = UUID.randomUUID();
        }

    }
}
