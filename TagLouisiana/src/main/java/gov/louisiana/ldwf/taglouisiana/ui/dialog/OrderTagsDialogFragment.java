package gov.louisiana.ldwf.taglouisiana.ui.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.common.collect.ImmutableMap;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi.PostCombinedDataResponse;
import gov.louisiana.ldwf.taglouisiana.models.tag.EnteredTagRequest;
import gov.louisiana.ldwf.taglouisiana.net.ApiServiceGenerator;
import gov.louisiana.ldwf.taglouisiana.net.ProtectedApiService;
import gov.louisiana.ldwf.taglouisiana.ui.core.dialog.BaseDialogFragment;
import gov.louisiana.ldwf.taglouisiana.ui.util.MaterialDialogBuilder;
import retrofit2.Call;
import retrofit2.Response;

public class OrderTagsDialogFragment extends BaseDialogFragment {

    public static BaseDialogFragment newInstance() {
        return new OrderTagsDialogFragment();
    }


    private View view;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        MaterialDialogBuilder builder = new MaterialDialogBuilder(getActivity());

        builder.setTitle(getTitle());
        builder.setContent(getContentView());

        builder.setPositiveButton("SUBMIT", new MaterialDialogBuilder.OnMaterialDialogItemClickListener() {
            @Override
            public void onClick(Dialog dialog) {
                EnteredTagRequest request = getEnteredTagRequest();
                Map<String, Object> body = prepareSendRequestBody(request);

                AsyncSendTagRequest sendRequest = new AsyncSendTagRequest(dialog, body);
                sendRequest.execute();
            }

        });

        builder.setNegativeButton(getString(R.string.dialog_dismiss), new MaterialDialogBuilder.OnMaterialDialogItemClickListener() {
            @Override
            public void onClick(Dialog dialog) {
                dialog.dismiss();
            }
        });
        return builder.create();

    }

    @Override
    public String getTitle() {
        return "Request Tags";
    }

    private EnteredTagRequest getEnteredTagRequest() {
        String quantity = (String) ((Spinner) view.findViewById(R.id.tag_quantities_spinner)).getSelectedItem();
        String type = (String) ((Spinner) view.findViewById(R.id.tag_type_spinner)).getSelectedItem();

        final EnteredTagRequest request = new EnteredTagRequest();
        request.numberOfTags = Long.parseLong(quantity);
        request.tagType = type;
        if (request.uuid == null || "".equals(request.uuid.toString())) {
            request.uuid = UUID.randomUUID();
        }
        return request;
    }

    private Map<String, Object> prepareSendRequestBody(EnteredTagRequest request) {
        List<EnteredTagRequest> list = new LinkedList<>();
        list.add(request);

        final Map<String, Object> body = new LinkedHashMap<>();
        body.put("tag_requests", ImmutableMap.of("items", list));

        return body;
    }

    @Override
    public View getContentView() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.order_tags_dialog, null);
        return view;
    }


    private class AsyncSendTagRequest extends AsyncTask<Void, Void, Void> {
        private Dialog dialog;
        private Map<String, Object> body;
        private ProgressDialog progressDialog;
        private Response<PostCombinedDataResponse> response;
        String toastMessage;

        public AsyncSendTagRequest(Dialog givenDialog, Map<String, Object> givenBody) {
            dialog = givenDialog;
            body = givenBody;
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Sending request");
            progressDialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                ProtectedApiService apiService = ApiServiceGenerator.createService(ProtectedApiService.class);
                Call<PostCombinedDataResponse> call = apiService.postCombined(body);
                response = call.execute();
            } catch (ConnectException | SocketTimeoutException e) {
                toastMessage = "Cannot connect to the server. Please check your connection and try again later.";
            } catch (IOException e) {
                toastMessage = "There was error while connecting to the server. Please try again later.";
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (response != null) {
                if (response.code() != 200) {
                    try {
                        toastMessage = response.errorBody().string();
                    } catch (IOException e) {
                        toastMessage = "Error submitting the Tag Request. Please try again later.";
                    }
                } else {
                    toastMessage = "Request Submitted";
                }
            } else if (toastMessage == null || "".equals(toastMessage)) {
                toastMessage = "Error submitting the Tag Request. Please try again later.";
            }
            progressDialog.dismiss();
            dialog.dismiss();
            Toast.makeText(OrderTagsDialogFragment.this.getContext(), toastMessage, Toast.LENGTH_LONG).show();
            super.onPostExecute(aVoid);

        }
    }
}
