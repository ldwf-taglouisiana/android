package gov.louisiana.ldwf.taglouisiana.models.tag;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.NotNull;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.queriable.ModelQueriable;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.RemoteItem;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture_Table;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.EnteredPendingCapture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.EnteredPendingCapture_Table;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.PendingCapture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.PendingCapture_Table;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/28/14
 */
@Table(database = AppDatabase.class)
public class Tag extends RemoteItem {

    @Expose
    @Column
    @SerializedName("tag_no")
    public String number;

    @Expose
    @SerializedName("active")
    @Column
    public boolean active;

    @Expose
    @SerializedName("used")
    @Column
    public boolean used;

    public static ModelQueriable<Tag> getAvailableTagsQuery() {
        return getAvailableTagsQuery("");
    }

    public static ModelQueriable<Tag> getAvailableTagsQuery(@NotNull String tagConstraint) {
        StringBuilder builder = new StringBuilder();
        builder.append("%").append(tagConstraint).append("%");

        return SQLite.select()
                .from(Tag.class)
                .where(Tag_Table.used.is(false))
                .and(Tag_Table.number.notIn(SQLite.select(EnteredPendingCapture_Table.tagNumber).from(EnteredPendingCapture.class)))
                .and(Tag_Table.number.notIn(SQLite.select(Capture_Table.tagNumber).from(Capture.class)))
                .and(Tag_Table.number.notIn(SQLite.select(PendingCapture_Table.tagNumber).from(PendingCapture.class)))
                .and(Tag_Table.number.like(builder.toString().toUpperCase()))
                .orderBy(Tag_Table.number, true);
    }
}
