package gov.louisiana.ldwf.taglouisiana.models.profile;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.Table;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.RemoteItem;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/28/14
 */
@Table(database = AppDatabase.class, primaryKeyConflict = ConflictAction.REPLACE)
public class Angler extends RemoteItem {

    @Expose
    @SerializedName("city")
    @Column
    public String city;

    @Expose
    @SerializedName("comments")
    @Column
    public String comments;

    @Expose
    @SerializedName("email")
    @Column
    public String email;

    @Expose
    @SerializedName("angler_id")
    @Column
    public String anglerId;

    @Expose
    @SerializedName("first_name")
    @Column
    public String firstName;

    @Expose
    @SerializedName("last_name")
    @Column
    public String lastName;

    @Expose
    @SerializedName("phone_number")
    @Column
    public String mobileNumber;

    @Expose
    @SerializedName("state")
    @Column
    public String state;

    @Expose
    @SerializedName("street")
    @Column
    public String street;

    @Expose
    @SerializedName("suite")
    @Column
    public String suite;

    @Expose
    @SerializedName("zip")
    @Column
    public String zip;

    // relation attributes
    @Expose
    @SerializedName("shirt_size_id")
    @ForeignKey(tableClass = gov.louisiana.ldwf.taglouisiana.models.options.ShirtSizeOption.class)
    public long shirtSizeOptionId;

    public gov.louisiana.ldwf.taglouisiana.models.profile.Profile profile;
}
