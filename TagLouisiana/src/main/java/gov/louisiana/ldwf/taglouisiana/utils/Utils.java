package gov.louisiana.ldwf.taglouisiana.utils;

import android.support.annotation.NonNull;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.atomic.AtomicInteger;

import gov.louisiana.ldwf.taglouisiana.BuildConfig;
import retrofit2.Call;

/**
 * Created by danielward on 8/31/16.
 *
 */
public class Utils {

    private static final AtomicInteger ATOMIC_INT_GENERATOR = new AtomicInteger();
    public static int getFreshInt() {
        return ATOMIC_INT_GENERATOR.incrementAndGet();
    }

    public static void logError(@NonNull  String message) {
        Log.e(Constants.LOG_TAG, message);

        // when not in debug, also log to Crashlytics.
        if (!BuildConfig.DEBUG) {
            Crashlytics.log(Log.ERROR, Constants.LOG_TAG, message);
        }
    }

    public static void logError(@NonNull Call<?> call, @NonNull Throwable e) {
        Utils.logError("Error from making a call to url: " + call.request().url(), e);

        // if we are in debug mode, then print the stacktrace
        if (BuildConfig.DEBUG) {
            e.printStackTrace();
        }
    }

    public static void logError(@NonNull String message, @NonNull Throwable e) {
        Utils.logError(message);
        Utils.logError((e.getLocalizedMessage()==null)? "Error Occurred": e.getLocalizedMessage());

        // if we are in debug mode, then print the stacktrace
        if (BuildConfig.DEBUG) {
            e.printStackTrace();
        }
    }

    public static void logError(@NonNull Throwable exception) {
        Utils.logError((exception.getMessage()==null)?"Error Occurred":exception.getMessage(), exception);
    }

    public static void log(@NonNull String message) {
        Log.d(Constants.LOG_TAG, message);

        // when not in debug, also log to Crashlytics.
        if (!BuildConfig.DEBUG) {
            Crashlytics.log(Log.DEBUG, Constants.LOG_TAG, message);
        }
    }

    public static String getMD5String(String encTarget){
        MessageDigest mdEnc = null;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception while encrypting to md5");
            e.printStackTrace();
        } // Encryption algorithm
        mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
        while ( md5.length() < 32 ) {
            md5 = "0"+md5;
        }
        return md5;
    }
}
