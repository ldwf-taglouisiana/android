package gov.louisiana.ldwf.taglouisiana.ui.wizard;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;
import gov.louisiana.ldwf.taglouisiana.utils.CustomViewPager;


public abstract class Wizard extends BaseFragment {

    private static final String PAGER_STATE_PREFS = "com.github.dwa012.wizard.pager_state";
    private static final String PAGER_STATE_KEY = "pager_index";

    // Handlers
    private final Handler handler = new Handler();
    private CustomViewPager pager;
    private Button nextButton;
    private Button prevButton;
    private List<Page> pages;
    private SectionsPagerAdapter pagerAdapter;
    private Page confirmationPage;
    private Button submitButton;
    private DetailOnPageChangeListener pagerChangeListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        View myFragmentView = inflater.inflate(R.layout.enter_capture_main_fragment, container, false);
        this.setHasOptionsMenu(true);

        this.pager = (CustomViewPager) myFragmentView.findViewById(R.id.enter_capture_pager);

//    this.progressDots = new ArrayList<TextView>(5);
        this.pages = new ArrayList<>(5);

        this.pagerAdapter = new SectionsPagerAdapter(this.getChildFragmentManager());

        if(savedInstanceState == null) {
            getActivity().getSharedPreferences(PAGER_STATE_PREFS, Context.MODE_PRIVATE).edit().clear().apply();
        }

        nextButton = ((Button) myFragmentView.findViewById(R.id.next_button));
        prevButton = ((Button) myFragmentView.findViewById(R.id.prev_button));
        submitButton = ((Button) myFragmentView.findViewById(R.id.submit_button));

        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                nextButtonClicked();
            }
        });
        prevButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                previousButtonClicked();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                nextButtonClicked();
                onSubmissionSelected();
            }
        });

        prevButton.setVisibility(View.INVISIBLE);

        // this.initDots();
        this.initViewPager();

//        this.pagerAdapter = new SectionsPagerAdapter(this.getFragmentManager());
        new setAdapterTask().execute();

        return myFragmentView;
    }

    //------------------------------------------------------------------------------------------------
    // ABSTRACT METHODS TO BE IMPLEMENTED
    //------------------------------------------------------------------------------------------------

    public abstract void onSubmissionSelected();

    //------------------------------------------------------------------------------------------------
    // MODIFIERS FOR A SUBCLASS
    //------------------------------------------------------------------------------------------------

    protected void addPage(Page page) {
        this.addPages(page);
    }

    protected void addPages(final Page... pages) {
        Collections.addAll(Wizard.this.pages, pages);

        pagerAdapter.notifyDataSetChanged();
    }

    protected void addConfirmationPage(Page page) {
        this.confirmationPage = page;
        this.addPage(page);
    }

    protected void setCurrentPage(int index, boolean animated) {
        pager.setCurrentItem(index, animated);

        adjustButtons(index);
    }

    protected Page getCurrentPage() {
        return this.pages.get(pagerChangeListener.getCurrentPage());
    }

    protected int getCurrentPageIndex() {
        return pagerChangeListener.getCurrentPage();
    }

    //------------------------------------------------------------------------------------------------
    // INITIALIZERS
    //------------------------------------------------------------------------------------------------

    private void initViewPager() {
        this.pager.setPagingEnabled(false);
        this.pager.setOffscreenPageLimit(3);
        this.pagerChangeListener = new DetailOnPageChangeListener();
        this.pager.addOnPageChangeListener(pagerChangeListener);
    }


    @Override
    public void onResume() {
        super.onResume();

    /*
     * Get the save page index, if any.
     * Defaults to first page in the case when we did not save the page state.
     */
        int restoredIndex = getActivity().getSharedPreferences(PAGER_STATE_PREFS, Context.MODE_PRIVATE)
                .getInt(PAGER_STATE_KEY, 0);
        this.setCurrentPage(restoredIndex, false);
    }

    @Override
    public void onPause() {
    /*
     * When the Wizard gets paused, then restored the pager loses the current state.
     * By saving the current index we can then move to the correct page onResume
     */
        getActivity().getSharedPreferences(PAGER_STATE_PREFS, Context.MODE_PRIVATE)
                .edit()
                .putInt(PAGER_STATE_KEY, pagerChangeListener.getCurrentPage())
                .apply();
        super.onPause();

    }


    protected void resetPages() {
        for(Page page : pages) {
            page.onReset();
        }
    }

    protected void setOnSubmitListener(View.OnClickListener listener) {
        submitButton.setOnClickListener(listener);
    }

    protected void hideKeyboard() {
        hideKeyboard(getCurrentPage());
    }

    protected void hideKeyboard(Page page) {
        if (page.getView() != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(page.getView().getWindowToken(), 0);
        }
    }


    //------------------------------------------------------------------------------------------------
    // HELPER METHODS
    //------------------------------------------------------------------------------------------------

    private void nextButtonClicked() {
        // the page the pager is currently on, used to do some UI changes
        // before advancing to the nextPage
        int currentPageIndex = pagerChangeListener.getCurrentPage();

        // tell the current page to commit its values
        Page page = this.pages.get(currentPageIndex);

        page.commitValues();

        if (!page.isValid()) {
            return;
        }

        if (page.getView() != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(page.getView().getWindowToken(), 0);
        }

        // the index of the page we are about to advance to
        int nextPageIndex = currentPageIndex;
        if(currentPageIndex < getSubmitPageIndex()){
            nextPageIndex += 1;
        }



        /*
         * if the next page is before the submit page
         * move to the next page
         */
        if (currentPageIndex <= getSubmitPageIndex()) {
            this.pager.setCurrentItem(nextPageIndex, true);
        }

        /*
         * if the current page is the submit page and
         * the confirmation page has been provided, move to the confirmation page
         */
        else if (currentPageIndex == getSubmitPageIndex() && getConfirmationPageIndex() != -1) {
            pager.setCurrentItem(nextPageIndex, true);
        }

        adjustButtons(nextPageIndex);
    }

    private void previousButtonClicked() {
        int currentPageIndex = pagerChangeListener.getCurrentPage();

        int previousPageIndex = currentPageIndex;
        if(currentPageIndex > 0){
            previousPageIndex--;
        }

        pager.setCurrentItem(previousPageIndex, true);
        adjustButtons(previousPageIndex);
    }

    private void adjustButtons(int targetIndex) {
        if (targetIndex == 0) {
            hidePreviousButton();
        } else {
            showPreviousButton();
        }

    /*
     * Hide the button if the next page will be the
     * confirmation page
     */
        if (targetIndex == getConfirmationPageIndex()) {
            hideButtons();
        }

        if (targetIndex == getSubmitPageIndex()) {
            showSubmitButton();
        } else {
            hideSubmitButton();
        }

    }

    private void hideButtons() {
        getView().findViewById(R.id.button_holder).setVisibility(View.INVISIBLE);
    }

    private void hidePreviousButton() {
        prevButton.setVisibility(View.INVISIBLE);
    }

    private void showPreviousButton() {
        prevButton.setVisibility(View.VISIBLE);
    }

    private void showSubmitButton() {
        nextButton.setVisibility(View.GONE);
        submitButton.setVisibility(View.VISIBLE);
    }

    private void hideSubmitButton() {
        nextButton.setVisibility(View.VISIBLE);
        submitButton.setVisibility(View.GONE);
    }

    private int getSubmitPageIndex() {
        int result;

        if (getConfirmationPageIndex() == -1) {
            result = pages.size() - 1;
        } else {
            result = pages.size() - 2;
        }

        return result;
    }

    private int getConfirmationPageIndex() {
        int result = -1;

        if (confirmationPage != null) {
            result = this.pages.size() - 1;
        }

        return result;
    }

    //------------------------------------------------------------------------------------------------
    // INNER CLASSES
    //------------------------------------------------------------------------------------------------

    private class DetailOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

        private int currentPage;

        @Override
        public void onPageSelected(int position) {
            currentPage = position;
//            Page page = (Page) pagerAdapter.getItem(position);
//            page.restoreValues();
//            page.updateView();
        }

        public final int getCurrentPage() {
            return currentPage;
        }
    }

    /*
     * The adapter needed for this pager used for the wizard.
     * This is a very basic pager adapter for fragments
     */
    private class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return pages.get(i);
        }

        @Override
        public int getCount() {
            return pages.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        //This is a trick to not save the old fragments when the device is rotated.
        //There might be a better way to do it.
        @Override
        public Parcelable saveState() {
            return null;
        }
    }

    /*
     * Used to set the adapter of the pager as a background task.
     * Needed due to that fact that the parent Fragment needs to be fully
     * loaded before setting the inner pager adapter
     */
    private class setAdapterTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            pager.setAdapter(pagerAdapter);
        }
    }
}
