package gov.louisiana.ldwf.taglouisiana.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Date;

import gov.louisiana.ldwf.taglouisiana.gson.deserializers.EpochDateDeserializerSerializer;
import gov.louisiana.ldwf.taglouisiana.gson.typeAdapters.AttachedPhotoTypeAdapterFactory;
import gov.louisiana.ldwf.taglouisiana.gson.typeAdapters.BaseItemTypeAdapterFactory;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/29/14
 */
public class GsonUtil {

    public static Gson gson() {
        return gson(true);
    }

    public static Gson gson(boolean forceEpochPruning) {
        return new GsonBuilder()
                .registerTypeAdapterFactory(new AttachedPhotoTypeAdapterFactory())
                .registerTypeAdapterFactory(new BaseItemTypeAdapterFactory())
//                .registerTypeAdapterFactory(new EnteredPendingCaptureTypeAdapterFactory())
                .registerTypeAdapter(Date.class, new EpochDateDeserializerSerializer())
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }

    /**
     * Traverses a JsonObject to the given path.
     *
     * Adapted from http://stackoverflow.com/a/26087867/2614663
     *
     * @param jsonObject
     * @param path A string path to a nested key. Can "." or "/" separated
     * @return The JsonElement at the given path, if the path exists.
     */
    public static JsonElement getElementFromPath(JsonObject jsonObject, String path) {
        String[] segments = path.split("(\\.|/)");
        for (String segment : segments) {

            if (jsonObject != null) {

                JsonElement jsonElement = jsonObject.get(segment);

                if (!jsonElement.isJsonObject()) {
                    return jsonElement;
                }
                else {
                    jsonObject = jsonElement.getAsJsonObject();
                }

            } else {
                return null;
            }
        }

        return jsonObject;
    }
}
