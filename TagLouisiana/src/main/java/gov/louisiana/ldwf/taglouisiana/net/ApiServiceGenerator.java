package gov.louisiana.ldwf.taglouisiana.net;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import gov.louisiana.ldwf.taglouisiana.MainApplication;
import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.accounts.AccountGeneral;
import gov.louisiana.ldwf.taglouisiana.gson.GsonUtil;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;
import okhttp3.Authenticator;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Adapted from https://futurestud.io/blog/android-basic-authentication-with-retrofit
 *
 * Created by danielward on 8/25/16.
 */
public class ApiServiceGenerator {
    public static final String API_BASE_URL = MainApplication.getAppContext().getResources().getString(R.string.api_server);

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonUtil.gson()));

    public static <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, MainApplication.getAppContext());
    }

    public static <S> S createService(Class<S> serviceClass, @Nullable final Context context) {

        Map<String, String> headers = new LinkedHashMap<>();

        //Set the API Access Token to use for accessing some portions of the API
        headers.put("Api-Access-Token", ApiUtils.getApiAccessToken(MainApplication.getAppContext()));

        //Add the user agent to the requests
        headers.put("User-Agent",System.getProperty("http.agent"));

        // insert the headers into the request
        addHeadersToHTTPRequest(headers);

        /*
            Get the Auth token, if we can and add it to the headers.
            Adapted from http://stackoverflow.com/a/31042681/2614663
         */
        if (context != null) {
            httpClient.authenticator(new Authenticator() {
                @Override
                public Request authenticate(Route route, okhttp3.Response response) throws IOException {
                    AccountManager accountManager = AccountManager.get(context);
                    Account[] accounts;

                /*
                    If the permission to access accounts has not been granted, then return.
                 */
                    try {
                        accounts = accountManager.getAccountsByType(AccountGeneral.ACCOUNT_TYPE);
                    } catch (SecurityException e) {
                        Utils.logError(e);
                        return null;
                    }

                    // No account, do not even try to authenticate
                    if (accounts.length == 0) {
                        return null;
                    }

                    /*
                        Get the first account.
                        TODO if using multiple accoutns, get the account that the user has currently selected
                     */
                    Account account = accounts[0];

                    try {
                        // get the auth token from the acount manager
                        final String mCurrentToken = accountManager.blockingGetAuthToken(account, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS, false);

                        // if the auth token is empty, then we cannot attempt to auth with it.
                        if (TextUtils.isEmpty(mCurrentToken)) {
                            return null;
                        }

                        // the token with with "Bearer" prepended
                        final String bearerToken =  "Bearer " + mCurrentToken;

                        // For now, we just re-install blindly an interceptor
                        httpClient.interceptors().clear();
                        httpClient.interceptors().add(new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Request request = chain.request();
                                Request newReq = request.newBuilder()
                                        .addHeader("Authorization", bearerToken)
                                        .build();

                                return chain.proceed(newReq);
                            }
                        });

                        return response.request().newBuilder()
                                .addHeader("Authorization", bearerToken)
                                .build();

                    } catch (OperationCanceledException | AuthenticatorException | IOException e) {
                        Utils.logError(e);
                        e.printStackTrace();
                        return null;
                    }
                }
            });
        }

        /*
            Build the client adn configure retrofit to get the use the custom http client
         */
        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }

    private static void addHeadersToHTTPRequest(final Map<String, String> headers) {
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder();

                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    requestBuilder.header(entry.getKey(), entry.getValue());
                }

                requestBuilder.method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
    }
}
