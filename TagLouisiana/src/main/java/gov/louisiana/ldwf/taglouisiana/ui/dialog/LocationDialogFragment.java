package gov.louisiana.ldwf.taglouisiana.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.androidquery.AQuery;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.core.dialog.BaseDialogFragment;
import gov.louisiana.ldwf.taglouisiana.utils.EnteredLocation;
import gov.louisiana.ldwf.taglouisiana.utils.GeoCoord;

public class LocationDialogFragment extends BaseDialogFragment {

    private final int DEGREE_FEILD_WIDTH = 280;
    private final int DEGREE_MINUTE_FEILD_WIDTH = 160;
    private final int DEGREE_SECOND_FEILD_WIDTH = 80;

    private AQuery aq;

    EnteredLocation location;

    // Use this instance of the interface to deliver action events
    LocationDialogListener mListener = new LocationDialogListener() {

        @Override
        public void onLocationDialogClick(EnteredLocation location) {
            // left blank

        }

    };

    public interface LocationDialogListener {
        public void onLocationDialogClick(EnteredLocation location);
    }

    public LocationDialogFragment(EnteredLocation location, LocationDialogListener listener) {
        super();
        mListener = listener;

        this.location = location;

        if (this.location == null) {
            this.location.setGPSTypeD();
            this.location = new EnteredLocation(0, 0);
        } else
            this.location = location.copy();

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflator = getActivity().getLayoutInflater();
        // set title
        alertDialogBuilder.setTitle("Enter Location");


        final View v = inflator.inflate(R.layout.enter_location_dialog_fragment, null);

        aq = new AQuery(v);

        if (location.getGPSType() != null) {
            aq.id(R.id.latitude_deg).text(location.getLatitude() + "");
            aq.id(R.id.longitude_deg).text(location.getLongitude() + "");
        }

        final RadioGroup radioGroup = (RadioGroup) v.findViewById(R.id.enter_location_radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                EnteredLocation loc = location;//updateLocationObject(v, location);
                updateLocationObject(location);

                String[] lat_array = {};

                switch (checkedId) {
                    case R.id.enter_location_radio_d:
                        aq.id(R.id.longitude_deg).width(DEGREE_FEILD_WIDTH);
                        aq.id(R.id.latitude_deg).width(DEGREE_FEILD_WIDTH);

                        aq.id(R.id.latitude_deg).getEditText().setInputType(
                                InputType.TYPE_CLASS_NUMBER |
                                        InputType.TYPE_NUMBER_FLAG_DECIMAL |
                                        InputType.TYPE_NUMBER_FLAG_SIGNED);

                        aq.id(R.id.longitude_min).visibility(View.GONE);
                        aq.id(R.id.longitude_sec).visibility(View.GONE);

                        aq.id(R.id.latitude_min).visibility(View.GONE);
                        aq.id(R.id.latitude_sec).visibility(View.GONE);

                        aq.id(R.id.latitude_deg).text("" + loc.getLatitude());
                        aq.id(R.id.longitude_deg).text("" + loc.getLongitude());

                        break;
                    case R.id.enter_location_radio_dm:
                        aq.id(R.id.longitude_deg).width(DEGREE_MINUTE_FEILD_WIDTH);
                        aq.id(R.id.latitude_deg).width(DEGREE_MINUTE_FEILD_WIDTH);

                        aq.id(R.id.latitude_deg).getEditText().setInputType(
                                InputType.TYPE_CLASS_NUMBER |
                                        InputType.TYPE_NUMBER_FLAG_SIGNED);

                        aq.id(R.id.longitude_min).width(DEGREE_MINUTE_FEILD_WIDTH);
                        aq.id(R.id.latitude_min).width(DEGREE_MINUTE_FEILD_WIDTH);

                        aq.id(R.id.longitude_min).visibility(View.VISIBLE);
                        aq.id(R.id.longitude_sec).visibility(View.GONE);

                        aq.id(R.id.latitude_min).visibility(View.VISIBLE);
                        aq.id(R.id.latitude_sec).visibility(View.GONE);

                        lat_array = (new GeoCoord(loc.getLatitude(), loc.getLongitude())).getDM();

                        aq.id(R.id.latitude_deg).text(lat_array[0]);
                        aq.id(R.id.latitude_min).text(lat_array[1]);

                        aq.id(R.id.longitude_deg).text(lat_array[2]);
                        aq.id(R.id.longitude_min).text(lat_array[3]);


                        break;
                    case R.id.enter_location_radio_dms:
                        aq.id(R.id.longitude_deg).width(DEGREE_SECOND_FEILD_WIDTH);
                        aq.id(R.id.latitude_deg).width(DEGREE_SECOND_FEILD_WIDTH);

                        aq.id(R.id.longitude_min).width(DEGREE_SECOND_FEILD_WIDTH);
                        aq.id(R.id.latitude_min).width(DEGREE_SECOND_FEILD_WIDTH);

                        aq.id(R.id.longitude_min).width(DEGREE_SECOND_FEILD_WIDTH);
                        aq.id(R.id.latitude_min).width(DEGREE_SECOND_FEILD_WIDTH);

                        aq.id(R.id.longitude_min).visibility(View.VISIBLE);
                        aq.id(R.id.longitude_sec).visibility(View.VISIBLE);

                        aq.id(R.id.latitude_min).visibility(View.VISIBLE);
                        aq.id(R.id.latitude_sec).visibility(View.VISIBLE);

                        lat_array = (new GeoCoord(loc.getLatitude(), loc.getLongitude())).getDMS();

                        aq.id(R.id.latitude_deg).text(lat_array[0]);
                        aq.id(R.id.latitude_min).text(lat_array[1]);
                        aq.id(R.id.latitude_sec).text(lat_array[2]);

                        aq.id(R.id.longitude_deg).text(lat_array[3]);
                        aq.id(R.id.longitude_min).text(lat_array[4]);
                        aq.id(R.id.longitude_sec).text(lat_array[5]);

                        break;
                }

//                updateLocationObject(v,location);
            }


        });

        if (location.getGPSType().equals("D")) {
            ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
        }

        if (location.getGPSType().equals("DM")) {
            ((RadioButton) radioGroup.getChildAt(1)).setChecked(true);
        }

        if (location.getGPSType().equals("DMS")) {
            ((RadioButton) radioGroup.getChildAt(2)).setChecked(true);
        }

        // set dialog message
        final AlertDialog dialog = alertDialogBuilder
                .setView(v)
                .setPositiveButton("Enter", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // left blank
                    }
                })
                .setNegativeButton("Cancel", null).create();

        return dialog;
    }

    @Override
    public String getTitle() {
        return "Enter Location";
    }


    private boolean fieldsValid() {
        boolean result = true;

        EditText longitudeDegrees = aq.id(R.id.longitude_deg).getEditText();
        EditText latitudeDegrees = aq.id(R.id.latitude_deg).getEditText();

        longitudeDegrees.setError(null);
        latitudeDegrees.setError(null);

        // check if the latitude degree field cannot be positive
        if (Float.parseFloat(latitudeDegrees.getText().toString()) < 0.0f) {
            result = false;
            latitudeDegrees.setError("Must be a positive number");
        }

        // check if the latitude degree field cannot be negative
        if (Float.parseFloat(longitudeDegrees.getText().toString()) > 0.0f) {
            result = false;
            longitudeDegrees.setError("Must be a negative number");
        }

        // check if the latitude degree field cannot be empty
        if (latitudeDegrees.getText().toString().equals("")) {
            result = false;
            latitudeDegrees.setError("Cannot be blank");
        }

        // check if the longitude degree field cannot be empty
        if (longitudeDegrees.getText().toString().equals("")) {
            result = false;
            longitudeDegrees.setError("Cannot be blank");
        }

        return result;
    }

    @Override
    public void onStart() {
        super.onStart();    //super.onStart() is where dialog.show() is actually called on the underlying dialog, so we have to do it after this point
        AlertDialog d = (AlertDialog) getDialog();
        if (d != null) {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new CustomListener(d));
        }
    }


    private EnteredLocation updateLocationObject(EnteredLocation location) {
//        AQuery aq = new AQuery(v);
        float lat_deg = stringToFloat(aq.id(R.id.latitude_deg).getEditText().getText().toString());
        float lat_min = stringToFloat(aq.id(R.id.latitude_min).getEditText().getText().toString());
        float lat_sec = stringToFloat(aq.id(R.id.latitude_sec).getEditText().getText().toString());
        float lon_deg = stringToFloat(aq.id(R.id.longitude_deg).getEditText().getText().toString());
        float lon_min = stringToFloat(aq.id(R.id.longitude_min).getEditText().getText().toString());
        float lon_sec = stringToFloat(aq.id(R.id.longitude_sec).getEditText().getText().toString());

        GeoCoord coord = new GeoCoord(lat_deg, lat_min, lat_sec, lon_deg, lon_min, lon_sec);


        location.setLatitude((float) coord.lat());
        location.setLongitude((float) coord.lon());

        return location;
    }

    private float stringToFloat(String text) {
        float result = 0;
        try {
            result = Float.parseFloat(text);
        } catch (NumberFormatException e) {
        }

        return result;
    }


    private class CustomListener implements View.OnClickListener {
        private final Dialog dialog;

        public CustomListener(Dialog dialog) {
            this.dialog = dialog;
        }

        @Override
        public void onClick(View v) {

            if (fieldsValid()) {
                final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.enter_location_radio_group);

                int index = radioGroup.indexOfChild(v.findViewById(radioGroup.getCheckedRadioButtonId()));

                switch (index) {
                    case 0:
                        location.setGPSTypeD();
                        break;
                    case 1:
                        location.setGPSTypeDM();
                        break;
                    case 2:
                        location.setGPSTypeDMS();
                        break;
                }

                mListener.onLocationDialogClick(updateLocationObject(location));

                dialog.dismiss();
            }
        }
    }
}