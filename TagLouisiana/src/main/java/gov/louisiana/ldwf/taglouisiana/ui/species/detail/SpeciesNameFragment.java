package gov.louisiana.ldwf.taglouisiana.ui.species.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.models.species.Species;
import gov.louisiana.ldwf.taglouisiana.models.species.Species_Table;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 9/18/14
 */
public class SpeciesNameFragment extends BaseFragment {

    private static final String SPECIES_ID = "species";
    private static final String NAME = "name";

    public static SpeciesNameFragment newInstance(String name, Long speciesId) {
        Bundle args = new Bundle();
        args.putLong(SPECIES_ID, speciesId);
        args.putString(NAME, name);

        SpeciesNameFragment fragment = new SpeciesNameFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public static SpeciesNameFragment newInstance(String name, Species species) {
        return SpeciesNameFragment.newInstance(name, species._id);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Species species = SQLite.select().from(Species.class).where(Species_Table._id.eq(getArguments().getLong(SPECIES_ID))).querySingle();

        View view = inflater.inflate(R.layout.fragment_species_detail_name, container, false);
        AQuery aQuery = new AQuery(view);

        setCommonNames(aQuery, species);
        setFamilyName(aQuery, species);
        setProperName(aQuery, species);
        setOtherNames(aQuery, species);

        return view;
    }

    @Override
    public String getTitle() {
        return getArguments().getString(NAME);
    }

    private void setCommonNames(AQuery aQuery, Species species) {
        if (TextUtils.isEmpty(species.commonName)) {
            aQuery.id(R.id.common_name_container).gone();
        } else {
            aQuery.id(R.id.common_name).text(species.commonName);
        }
    }

    private void setProperName(AQuery aQuery, Species species) {
        if (TextUtils.isEmpty(species.properName)) {
            aQuery.id(R.id.proper_name_container).gone();
        } else {
            aQuery.id(R.id.proper_name).text(species.properName);
        }
    }

    private void setFamilyName(AQuery aQuery, Species species) {
        if (TextUtils.isEmpty(species.familyName)) {
            aQuery.id(R.id.family_name_container).gone();
        } else {
            aQuery.id(R.id.family_name).text(species.familyName);
        }
    }

    private void setOtherNames(AQuery aQuery, Species species) {
        if (TextUtils.isEmpty(species.otherName)) {
            aQuery.id(R.id.other_name_container).gone();
        } else {
            aQuery.id(R.id.other_name).text(species.otherName);
        }
    }
}
