package gov.louisiana.ldwf.taglouisiana.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.raizlabs.android.dbflow.list.FlowQueryList;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

/**
 * Created by danielward on 10/6/16.
 */
public class CapturesListAdapter extends FlowQueryListFilterableAdapter<Capture> {

    private int resource;
    private AQuery aQuery;

    public CapturesListAdapter(Context context, FlowQueryList<Capture> items, int resource, FilterQueryConstraintRequest<Capture> filterQueryConstraintRequest) {
        super(items, filterQueryConstraintRequest);

        this.aQuery = new AQuery(context);
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = aQuery.inflate(convertView, resource, parent);
        aQuery.recycle(convertView);

        final Capture capture = getItem(position);
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();

        if (viewHolder == null) {
            viewHolder = new ViewHolder();
            viewHolder.date = aQuery.id(R.id.date).getTextView();
            viewHolder.species = aQuery.id(R.id.species).getTextView();
            viewHolder.tagNumber = aQuery.id(R.id.tag_number).getTextView();
            viewHolder.recaptured = aQuery.id(R.id.recaptured).getTextView();
            viewHolder.unverified = aQuery.id(R.id.unverified).getTextView();
            viewHolder.imageView = aQuery.id(R.id.has_images).getView();
            viewHolder.statusContainer = (RelativeLayout) aQuery.id(R.id.status_container).getView();

            convertView.setTag(viewHolder);
        }

        aQuery.id(viewHolder.species).text(capture.getSpecies().commonName);
        aQuery.id(viewHolder.date).text(Constants.US_DATE_FORMATTER.format(capture.date));
        aQuery.id(viewHolder.tagNumber).text(capture.tagNumber);

        aQuery.id(viewHolder.recaptured).visibility(capture.hasRecaptures() ? View.VISIBLE : View.GONE);
        aQuery.id(viewHolder.unverified).visibility(capture.isVerified ? View.GONE : View.VISIBLE);
        aQuery.id(viewHolder.imageView).visibility(capture.getImages().size() > 0 ? View.VISIBLE : View.GONE);

        return convertView;
    }

    private static class ViewHolder {
        TextView date;
        TextView species;
        TextView tagNumber;
        TextView recaptured;
        TextView unverified;
        View imageView;

        RelativeLayout statusContainer;
    }
}
