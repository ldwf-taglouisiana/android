package gov.louisiana.ldwf.taglouisiana.ui.species.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 9/18/14
 */
public class SpeciesTextFragment extends BaseFragment {

    private static final String TEXT_DATA = "text";
    private static final String NAME = "name";

    public static SpeciesTextFragment newInstance(String name, String text) {
        Bundle args = new Bundle();
        args.putString(TEXT_DATA, text);
        args.putString(NAME, name);

        SpeciesTextFragment fragment = new SpeciesTextFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_species_detail_text, container, false);

        ((TextView) view.findViewById(R.id.text_view)).setText(Html.fromHtml(getArguments().getString(TEXT_DATA)));

        return view;
    }

    @Override
    public String getTitle() {
        return getArguments().getString(NAME);
    }
}
