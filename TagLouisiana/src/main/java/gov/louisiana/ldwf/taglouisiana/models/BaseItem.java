package gov.louisiana.ldwf.taglouisiana.models;

import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by danielward on 10/27/16.
 */

public abstract class BaseItem extends BaseModel {
    public abstract long id();
}
