package gov.louisiana.ldwf.taglouisiana.ui.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;

import gov.louisiana.ldwf.taglouisiana.R;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 10/3/14
 */
public class MaterialDialogBuilder {

    private Context context;

    private String title;
    private String content;
    private View viewContent;
    private int layoutId;

    private String negativeButtonText;
    private String positiveButtonText;

    private OnMaterialDialogItemClickListener negativeButtonAction;
    private OnMaterialDialogItemClickListener positiveButtonAction;

    public interface OnMaterialDialogItemClickListener {
        public void onClick(Dialog dialog);
    }


    public MaterialDialogBuilder(Context context) {
        this.context = context;
    }

    public Context getContext(){
        return this.context;
    }

    public MaterialDialogBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public MaterialDialogBuilder setContent(String content) {
        this.content = content;
        return this;
    }

    public MaterialDialogBuilder setContent(int layoutId) {
        this.layoutId = layoutId;
        return this;
    }

    public MaterialDialogBuilder setContent(View content) {
        this.viewContent = content;
        return this;
    }

    public MaterialDialogBuilder setNegativeButton(@NonNull String title, @NonNull OnMaterialDialogItemClickListener listener) {
        this.negativeButtonText = title;
        this.negativeButtonAction = listener;
        return this;
    }

    public MaterialDialogBuilder setPositiveButton(@NonNull String title, @NonNull OnMaterialDialogItemClickListener listener) {
        this.positiveButtonText = title;
        this.positiveButtonAction = listener;
        return this;
    }

    public Dialog create() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        View view = createView();

        builder.setView(view);

        final Dialog dialog = builder.create();

        if (positiveButtonText != null) {
            TextView positiveButton = (TextView) view.findViewById(R.id.positive_button);
            positiveButton.setText(this.positiveButtonText);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    positiveButtonAction.onClick(dialog);
                }
            });
        } else {
            (view.findViewById(R.id.positive_button)).setVisibility(View.GONE);
        }

        if (negativeButtonText != null) {
            TextView negativeButton = (TextView) view.findViewById(R.id.negative_button);
            negativeButton.setText(this.negativeButtonText);
            negativeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    negativeButtonAction.onClick(dialog);
                }
            });
        } else {
            (view.findViewById(R.id.negative_button)).setVisibility(View.GONE);
        }
        return dialog;
    }

    public View createView() {
        View view = LayoutInflater.from(context).inflate(R.layout.generic_dialog, null, false);

        final AQuery dialogQuery = new AQuery(view);

        if (this.layoutId != 0) {
            this.viewContent = LayoutInflater.from(context).inflate(this.layoutId, (ViewGroup) view, false);
            LinearLayout contentContainer = (LinearLayout) dialogQuery.id(R.id.content_container).getView();
            contentContainer.addView(this.viewContent);
            dialogQuery.id(R.id.content).gone();
        } else if (this.viewContent != null) {
            LinearLayout contentContainer = (LinearLayout) dialogQuery.id(R.id.content_container).getView();
            contentContainer.addView(this.viewContent);
        }

        dialogQuery.id(R.id.title).text(this.title);
        dialogQuery.id(R.id.content).text(this.content);

        return view;
    }
}
