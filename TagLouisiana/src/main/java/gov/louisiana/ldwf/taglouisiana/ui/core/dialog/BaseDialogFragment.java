package gov.louisiana.ldwf.taglouisiana.ui.core.dialog;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;

import java.util.List;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseActivity;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

/**
 * Created by danielward on 8/2/13.
 *
 */
public abstract class BaseDialogFragment<T> extends AppCompatDialogFragment {

    private BaseActivity baseActivity;

    public static final int RESULT_SELECTED = Utils.getFreshInt();
    public static final int RESULT_DESELECTED = Utils.getFreshInt();

    @Override
    public void onAttach(Context context) {
        if(context instanceof BaseActivity) {
            this.baseActivity = (BaseActivity) context;
        } else{
            throw new IllegalArgumentException("Provided context must be instance of BaseActivity");
        }
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        this.baseActivity = null;
        super.onDetach();
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View contentView = getContentView();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if (contentView != null) {
            builder.setView(contentView);
            builder.setTitle(getTitle());

            if (getPositiveButtonAction() != null) {
                builder.setPositiveButton("SELECT", getPositiveButtonAction());
            }

            builder.setNegativeButton(getString(R.string.dialog_dismiss), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, null);
                    dismiss();
                }
            });


        } else if (getDisplayList() != null) {
            final List<String> displayedItems = getDisplayList();
            final List<T> selectableItems = getItemList();

            builder.setTitle(getTitle());

            if (this.isMultipleChoice()) {
                builder.setMultiChoiceItems(
                        displayedItems.toArray(new String[displayedItems.size()]),
                        checkedItems(),
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which, boolean selected) {
                                int result_code = selected ? RESULT_SELECTED : RESULT_DESELECTED;
                                getTargetFragment().onActivityResult(getTargetRequestCode(), result_code, createResultIntent(selectableItems.get(which)));
                            }
                        }
                );
            } else {
                builder.setItems(displayedItems.toArray(new String[displayedItems.size()]), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, createResultIntent(selectableItems.get(which)));
                    }
                });
            }
        }
        return builder.create();

    }

    public abstract String getTitle();

    public DialogInterface.OnClickListener getPositiveButtonAction() {
        return null;
    }

    public boolean isMultipleChoice() {
        return false;
    }

    public boolean[] checkedItems() {
        return new boolean[getItemList().size()];
    }

    public Intent createResultIntent(T item) {
        return null;
    }

    public View getContentView() {
        return null;
    }

    public List<String> getDisplayList() {
        return null;
    }

    public List<T> getItemList(){
        return null;
    }

    public BaseActivity getBaseActivity() {
        return baseActivity;
    }
}
