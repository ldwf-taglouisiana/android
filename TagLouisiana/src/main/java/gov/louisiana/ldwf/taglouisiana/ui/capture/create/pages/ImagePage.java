package gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.EnterCapturePage;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.adapters.ImageAdapter;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.util.TagImage;
import gov.louisiana.ldwf.taglouisiana.ui.dialog.TagImageDialogFragment;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Page;
import gov.louisiana.ldwf.taglouisiana.utils.EnterCapturePreferencesWrapper;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

/**
 * Created by danielward on 11/8/16.
 */

public class ImagePage extends EnterCapturePage {

    public static final int CAPTURE_IMAGE_REQUEST_CODE = Utils.getFreshInt();


    private SortedSet<TagImage> images;
    private GridView gridView;
    private String currentFilePath;

    private EnterCapturePreferencesWrapper preferencesWrapper;
    private String selectedTag;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        images = new TreeSet<>();
        preferencesWrapper = new EnterCapturePreferencesWrapper(getSharedPrefs(), getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.enter_capture_page_add_image, container, false);

        gridView = (GridView) myFragmentView.findViewById(R.id.image_grid_view);
        gridView.setAdapter(new ImageAdapter(getContext(), images));

        myFragmentView.findViewById(R.id.add_image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getChildFragmentManager();
                TagImageDialogFragment addFriendDialog = TagImageDialogFragment.newInstance(ImagePage.this, preferencesWrapper.tagNumbers());
                addFriendDialog.show(fm, "select_tag_dialog");

            }
        });

        return myFragmentView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Don't forget to check requestCode before continuing your job
        if (requestCode == CAPTURE_IMAGE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            images.add(new TagImage(selectedTag, null, currentFilePath));
            commitValues();
            updateView();
        }

        if (requestCode == TagImageDialogFragment.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                selectedTag = data.getStringExtra(TagImageDialogFragment.INTENT_TAG_NUMBER);
                startImageCaptureActivity();
            }
        }
    }

    @Override
    public void onReset() {
        super.onReset();
        this.images.clear();
        commitValues();
    }

    @Override
    public void updateView() {
        ((ImageAdapter)gridView.getAdapter()).updateData(images);
    }

    @Override
    public void restoreValues() {
        images.clear();
        images.addAll(new EnterCapturePreferencesWrapper(getSharedPrefs(), getContext()).tagImages());
    }

    @Override
    public void commitValues() {
        new EnterCapturePreferencesWrapper(getSharedPrefs(), getContext()).commitTagImages(images);
    }

    public static class Builder extends Page.Builder {

        @Override
        public Page create() {
            Page result = new ImagePage();
            result.setSharedPrefs(this.getPrefs());

            return result;
        }
    }

    private void startImageCaptureActivity() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getBaseActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Utils.logError(ex);
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        getString(R.string.common_file_authority),
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAPTURE_IMAGE_REQUEST_CODE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentFilePath = image.getAbsolutePath();
        return image;
    }

}
