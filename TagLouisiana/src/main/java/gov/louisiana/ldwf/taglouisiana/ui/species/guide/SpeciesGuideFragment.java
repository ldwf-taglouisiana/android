package gov.louisiana.ldwf.taglouisiana.ui.species.guide;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;

/**
 * Created by daniel on 5/6/14.
 */
public class SpeciesGuideFragment extends BaseFragment {

    public SpeciesGuideFragment() {
        setHasTabs();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_species_guide, container, false);


        ArrayList<BaseFragment> fragments = new ArrayList<>(2);
        fragments.add(SpeciesListFragment.getTagetSpeciesInstance());
        fragments.add(SpeciesListFragment.getAllSpeciesInstance());

        SpeciesPagerAdapter adapter = new SpeciesPagerAdapter(this.getChildFragmentManager(), fragments);

        ViewPager viewPager = (ViewPager) view.findViewById(R.id.pager);
        viewPager.setAdapter(adapter);

        getBaseActivity().setTabViewPager(viewPager);

        return view;
    }

    @Override
    public String getTitle() {
        return "Species Guide";
    }

    private class SpeciesPagerAdapter extends FragmentStatePagerAdapter {

        private List<BaseFragment> fragments;

        public SpeciesPagerAdapter(FragmentManager fm, List<BaseFragment> fragments) {
            super(fm);

            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = fragments.get(i);
            return fragment;
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragments.get(position).getTitle();
        }
    }
}
