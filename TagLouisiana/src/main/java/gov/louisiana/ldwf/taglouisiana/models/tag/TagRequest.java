package gov.louisiana.ldwf.taglouisiana.models.tag;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;

import java.util.UUID;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.database.UUIDConverter;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/28/14
 */
@Table(database = AppDatabase.class)
public class TagRequest extends BaseModel {

    public static final String INSHORE_TAG_TYPE = "Redfish/Trout Tags";
    public static final String OFFSHORE_TAG_TYPE = "Tuna Tags";

    @PrimaryKey
    @Column(typeConverter = UUIDConverter.class)
    @Expose
    @SerializedName("uuid")
    public UUID uuid;

    @Expose
    @SerializedName("number_of_tags")
    @Column
    public long numberOfTags;

    @Expose
    @SerializedName("tag_type")
    @Column
    public String tagType;

    @Override
    public void save() {
        setDefaults();
        super.save();
    }

    @Override
    public void save(DatabaseWrapper databaseWrapper) {
        setDefaults();
        super.save(databaseWrapper);
    }

    private void setDefaults() {
        // if the UUID isn't set yet, then create a new rrandom value
        if (this.uuid == null || "".equals(this.uuid.toString())) {
            this.uuid = UUID.randomUUID();
        }
    }
}
