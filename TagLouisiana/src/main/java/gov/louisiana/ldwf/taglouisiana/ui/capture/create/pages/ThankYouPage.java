package gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.EnterCapturePage;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Page;

/**
 * Created by daniel on 5/14/14.
 */
public class ThankYouPage extends EnterCapturePage {

  private final RestartListener listener;

  public ThankYouPage() {
    this.listener = new RestartListener() {
      @Override
      public void onSelected() {
        // left blank
      }
    };
  }

  @SuppressLint("ValidFragment")
  public ThankYouPage(RestartListener listener) {
    this.listener = listener;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View myFragmentView = inflater.inflate(R.layout.enter_capture_thank_you, container, false);

    myFragmentView.findViewById(R.id.start_over_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        listener.onSelected();
      }
    });

    return myFragmentView;
  }

  @Override
  public void updateView() {
    // left blank
  }

  @Override
  public void restoreValues() {
    // left blank
  }

  @Override
  public void commitValues() {
    // left blank
  }

  public interface RestartListener {
    public void onSelected();
  }

  public static class Builder extends Page.Builder {

    RestartListener listener;

    public void setRestartListener(RestartListener listener) {
      this.listener = listener;
    }

    public RestartListener getRestartListener() {
      return listener;
    }

    @Override
    public Page create() {
      Page result = new ThankYouPage(this.getRestartListener());
      result.setSharedPrefs(this.getPrefs());

      return result;
    }
  }
}
