package gov.louisiana.ldwf.taglouisiana.ui.dialog;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.FilterQueryProvider;
import android.widget.SimpleCursorAdapter;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import gov.louisiana.ldwf.taglouisiana.models.species.Species;
import gov.louisiana.ldwf.taglouisiana.models.species.Species_Table;
import gov.louisiana.ldwf.taglouisiana.ui.core.dialog.BaseCursorListDialogFragment;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

public class SpeciesDialogFragment extends BaseCursorListDialogFragment {

    public static final int REQUEST_CODE = Utils.getFreshInt();

    public static final String INTENT_SPECIES_ID = "SpeciesDialogFragment.intent.species.id";

    public static SpeciesDialogFragment newInstance(Fragment targetFragment) {

        Bundle args = new Bundle();

        SpeciesDialogFragment fragment = new SpeciesDialogFragment();
        fragment.setTargetFragment(targetFragment, REQUEST_CODE);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public Intent createResultIntent(Long item) {
        Intent intent = new Intent();
        intent.putExtra(INTENT_SPECIES_ID, item);

        return intent;
    }

    @Override
    public String getTitle() {
        return "Choose Species";
    }

    @Override
    public Cursor getItemListCursor() {
        return SQLite.select()
                .from(Species.class)
            .where(Species_Table.isPublished.is(true))
            .orderBy(Species_Table.isTarget, false)
            .orderBy(Species_Table.position, true)
            .cursorList().cursor();
    }

    @Override
    public FilterQueryProvider getCursorFilterQueryProvider() {
        return new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                String query = "%" + constraint.toString() + "%";
                return SQLite.select()
                        .from(Species.class)
                        .where(Species_Table.isPublished.is(true))
                        .and(Species_Table.commonName.like(query))
                        .orderBy(Species_Table.isTarget, false)
                        .orderBy(Species_Table.position, true)
                        .cursorList().cursor();
            }
        };
    }

    @Override
    public SimpleCursorAdapter getCursorAdapter() {
        return getCursorAdapter(new String[]{ "commonName" }, new int[]{ android.R.id.text1 });
    }
}