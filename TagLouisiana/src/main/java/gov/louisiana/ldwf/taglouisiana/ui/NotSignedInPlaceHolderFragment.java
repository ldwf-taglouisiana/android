package gov.louisiana.ldwf.taglouisiana.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import gov.louisiana.ldwf.taglouisiana.MainActivity;
import gov.louisiana.ldwf.taglouisiana.MainApplication;
import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;

/**
 * Created with IntelliJ IDEA.
 * User: danielward
 * Date: 3/16/13
 * Time: 9:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class NotSignedInPlaceHolderFragment extends BaseFragment {

    private Menu menu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View myFragmentView = inflater.inflate(R.layout.placeholder_not_signed_in, container, false);
        AQuery aq = new AQuery(myFragmentView);

        aq.id(R.id.placeholder_sign_in_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                BaseDialogFragment newFragment = new SignInDialogFragment(new SignInDialogFragment.SignInDialogListener() {
//
//                    @Override
//                    public void signInSuccessful() {
//                        //hide sign in item
//                        MenuItem menuItem = menu.findItem(R.id.menu_sign_in);
//                        menuItem.setVisible(false);
//
//                        //show sign out item
//                        menuItem = menu.findItem(R.id.menu_sign_out);
//                        menuItem.setVisible(true);
//                    }
//
//                });
//                newFragment.show(NotSignedInPlaceHolderFragment.this.getActivity().getSupportFragmentManager(), "sign_in_dialog");

                MainActivity activity = (MainActivity) NotSignedInPlaceHolderFragment.this.getActivity();
                activity.signIn();


                //---------------------
            }
        });

        aq.id(R.id.placeholder_register_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(MainApplication.getAppContext().getResources().getString(R.string.api_server) + "/sign_up/new"));
                startActivity(browserIntent);
            }
        });


        return myFragmentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        super.onCreateOptionsMenu(menu, inflater);
    }

}
