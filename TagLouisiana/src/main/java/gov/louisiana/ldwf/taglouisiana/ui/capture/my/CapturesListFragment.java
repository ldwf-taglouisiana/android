package gov.louisiana.ldwf.taglouisiana.ui.capture.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filterable;
import android.widget.ListView;

import com.raizlabs.android.dbflow.list.FlowQueryList;
import com.raizlabs.android.dbflow.sql.language.From;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.raizlabs.android.dbflow.sql.queriable.ModelQueriable;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.adapters.CapturesListAdapter;
import gov.louisiana.ldwf.taglouisiana.adapters.FilterQueryConstraintRequest;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture_Table;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseRefreshableQueryListFragment;

public class CapturesListFragment extends BaseRefreshableQueryListFragment<Capture> {

    public CapturesListFragment() {
        // left blank
    }


    @Override
    protected View setContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View myFragmentView = inflater.inflate(R.layout.my_captures_list, container, false);
        ListView gridView = (ListView) myFragmentView.findViewById(android.R.id.list);

        setAdapterView(gridView);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), CaptureDetailActivity.class);
                final long captureId = adapterView.getAdapter().getItemId(position);
                intent.putExtra(CaptureDetailFragment.CAPTURE_DETAIL_ARG, captureId);
                startActivity(intent);
            }
        });

        setHasOptionsMenu(true);

        return myFragmentView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_capture_list, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ((Filterable) getAdapter()).getFilter().filter(newText);
                return true;
            }
        });
    }

    //------------------------------------------------------------------------------------------------
    // Overrides for BaseRefreshableQueryListFragment
    //------------------------------------------------------------------------------------------------


    @Override
    protected BaseAdapter getNewAdapter() {
        return new CapturesListAdapter(getActivity(), queryList, R.layout.capture_list_card, new FilterQueryConstraintRequest<Capture>() {
            @Override
            public FlowQueryList<Capture> applyConstraint(String constraint) {
                return buildQuery(constraint).flowQueryList();
            }
        });
    }

    @Override
    protected Class<Capture> getQueryableClass() {
        return Capture.class;
    }

    @Override
    protected ModelQueriable<Capture> buildQuery(@Nullable String queryString) {
        ModelQueriable<Capture> query = SQLite.select()
                .from(Capture.class);

        if (queryString != null) {
            // cast should be safe, since above we ended with a "Where" statement
            query = ((From<Capture>)query)
                    .where(Capture_Table.tagNumber.like("%" + queryString + "%"));
        }

        if (query instanceof From<?>) {
            query = ((From<Capture>) query)
                    .orderBy(Capture_Table.date, false)
                    .orderBy(Capture_Table.tagNumber, false);
        }

        if (query instanceof Where<?>) {
            query = ((Where<Capture>) query)
                    .orderBy(Capture_Table.date, false)
                    .orderBy(Capture_Table.tagNumber, false);
        }

        return query;
    }


    //------------------------------------------------------------------------------------------------
    // Overrides from BaseFragment
    //------------------------------------------------------------------------------------------------

    @Override
    public String getTitle() {
        return "My Captures";
    }
}
