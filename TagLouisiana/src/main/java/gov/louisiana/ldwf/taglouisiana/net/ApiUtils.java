package gov.louisiana.ldwf.taglouisiana.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Pair;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import gov.louisiana.ldwf.taglouisiana.MainApplication;
import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.gson.GsonUtil;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by danielward on 8/28/14.
 */
public class ApiUtils {

  private static String TOKEN_PREFS_KEY = "api-token-key";

  public static Retrofit getRetrofit() {
    final String BASE_URL = MainApplication.getAppContext().getResources().getString(R.string.api_server);

    return new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonUtil.gson()))
            .build();
  }

  public static Uri urlWithHost(String path) {
    final String BASE_URL = MainApplication.getAppContext().getResources().getString(R.string.api_server);

    return Uri.withAppendedPath(Uri.parse(BASE_URL), path);
  }

  public static String getToken(Context context) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

    if (prefs.getString(TOKEN_PREFS_KEY, null) != null) {
      return prefs.getString(TOKEN_PREFS_KEY, null);
    } else {
      return getApiAccessToken(context);
    }
  }

  public static String getApiAccessToken(@NonNull Context context) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

    String token = "";
      try {
        token = SHA256(context.getResources().getString(R.string.client_id).toString(), context.getResources().getString(R.string.client_secret).toString());
        prefs.edit().putString(TOKEN_PREFS_KEY, token).apply();
      } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
        e.printStackTrace();
      }

      return token;
  }

  public static String SHA256(String appId, String appSecret) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    MessageDigest md = MessageDigest.getInstance("SHA-256");
    md.update(appId.getBytes());
    md.update(appSecret.getBytes());
    byte[] sha1hash = md.digest();
    return convertToHex(sha1hash);
  }

  private static String convertToHex(byte[] data) {
    StringBuilder buf = new StringBuilder();
    for (byte b : data) {
      int halfbyte = (b >>> 4) & 0x0F;
      int two_halfs = 0;
      do {
        buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
        halfbyte = b & 0x0F;
      } while (two_halfs++ < 1);
    }
    return buf.toString();
  }

  // QUERY PARAMS BUILDING

  public static Map<String, String> convertMapToQueryMap(@NonNull Map<String, Object> map) {
    return convertMapToQueryMap(map, "");
  }

  public static Map<String, String> convertMapToQueryMap(@NonNull Map<String, Object> map, @NonNull String parentKey) {
    return getPathMap(generateQueryParameterValues(map, parentKey));
  }

  /**
   * Adopted from  http://stackoverflow.com/a/28822411
   * @param map
   * @param parentKey
   * @return
     */
  private static List<Pair<String, String>> generateQueryParameterValues(@NonNull Map<String, Object> map, @NonNull String parentKey) {
    List<Pair<String, String>> results = new LinkedList<>();

    for (Map.Entry<String, Object> entry : map.entrySet()) {
      if (entry.getValue() instanceof  Map<?,?>) {
        results.addAll(generateQueryParameterValues((Map<String, Object>) entry.getValue(), entry.getKey()));

      }
      else if (entry.getValue() instanceof Collection<?> || entry.getValue().getClass().isArray() || entry.getValue() instanceof Iterable<?>) {
        Collection<Object> objects = Lists.newArrayList((Iterable<? extends Object>) entry.getValue());
        Collection<String> entries = Collections2.transform(objects, new Function<Object, String>() {
          @Nullable
          @Override
          public String apply(@Nullable Object input) {
            return encodeItem(String.valueOf(input));
          }
        });

        results.add(new Pair<>(buildKey(entry.getKey(), parentKey), Joiner.on(",").join(entries)));

        // TODO the above does not handle nested collcitons in arrays
//        for (Object item : (Iterable<? extends Object>) entry.getValue()) {
//          if (item instanceof Map<?,?>) {
//            results.addAll(generateQueryParameterValues((Map<String, Object>) item, entry.getKey()));
//          } else {
//            results.add(new Pair<>(buildKey(entry.getKey(), parentKey) + "[]", encodeItem(item)));
//          }
//        }

      }
      else {
        results.add(new Pair<>(buildKey(entry.getKey(), parentKey), encodeItem(String.valueOf(entry.getValue()))));

      }
    }

    return results;
  }

  public static Map<String, String> getPathMap(@NonNull List<Pair<String, String>> params) {
    Map<String, String> paramMap = new HashMap<>();
    String paramValue;

    for (Pair<String, String> paramPair : params) {
      if (!TextUtils.isEmpty(paramPair.first)) {
          if (paramMap.containsKey(paramPair.first)) {
            // Add the duplicate key and new value onto the previous value
            // so (key, value) will now look like (key, value&key=value2)
            // which is a hack to work with Retrofit's QueryMap
            paramValue = paramMap.get(paramPair.first);
            paramValue += "&" + paramPair.first + "=" + paramPair.second;
          } else {
            // This is the first value, so directly map it
            paramValue = paramPair.second;
          }
          paramMap.put(paramPair.first, paramValue);
        }
      }

    return paramMap;
  }

  private static String buildKey(@NonNull String key, @NonNull String parentKey) {
    if (parentKey.isEmpty()) {
      return  encodeItem(key);
    } else {
      return parentKey + "[" + encodeItem(key) + "]";
    }
  }

  private static String encodeItem(@NonNull Object value) {
    final String ENCODING = "UTF-8";
   String result = "";

    try {
      result = URLEncoder.encode(String.valueOf(value),ENCODING);
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }

    return result;
  }
}
