package gov.louisiana.ldwf.taglouisiana.ui.registration;


import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Page;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ContactInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactInfoFragment extends Page {
  // TODO: Rename parameter arguments, choose names that match
  // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";

  // TODO: Rename and change types of parameters
  private String mParam1;
  private String mParam2;


  public ContactInfoFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   * <p/>
   * //@param param1 Parameter 1.
   * //@param param2 Parameter 2.
   *
   * @return A new instance of fragment ContactInfoFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static ContactInfoFragment newInstance() {
    ContactInfoFragment fragment = new ContactInfoFragment();
    return fragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_contact_info, container, false);
  }

  @Override
  public void updateView() {

  }

  @Override
  public void restoreValues() {

  }

  @Override
  public void commitValues() {
    EditText firstName = (EditText) getView().findViewById(R.id.first_name);
    EditText lastName = (EditText) getView().findViewById(R.id.last_name);
    EditText phone = (EditText) getView().findViewById(R.id.phone);

    @SuppressLint("WrongViewCast")
    EditText email = (EditText) getView().findViewById(R.id.email);

    SharedPreferences.Editor editor = getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE).edit();

    editor.putString(Constants.REGISTER_FIRST_NAME, firstName.getText().toString());
    editor.putString(Constants.REGISTER_LAST_NAME, lastName.getText().toString());
    editor.putString(Constants.REGISTER_EMAIL, email.getText().toString());
    editor.putString(Constants.REGISTER_PHONE_NUMBER, phone.getText().toString());

    editor.commit();
  }

  public static class Builder extends Page.Builder {

    @Override
    public Page create() {
      Page result = new ContactInfoFragment();
      result.setSharedPrefs(this.getPrefs());

      return result;
    }
  }
}
