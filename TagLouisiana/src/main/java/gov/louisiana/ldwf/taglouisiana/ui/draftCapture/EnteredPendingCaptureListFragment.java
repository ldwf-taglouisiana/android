package gov.louisiana.ldwf.taglouisiana.ui.draftCapture;

import android.app.Fragment;

import gov.louisiana.ldwf.taglouisiana.models.fishEvents.EnteredPendingCapture;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EnteredPendingCaptureListFragment#newInstance} factory method to
 */
public class EnteredPendingCaptureListFragment extends PendingCaptureBaseListFragment<EnteredPendingCapture> {

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DraftCaptureListFragment.
     */
    public static EnteredPendingCaptureListFragment newInstance() {
        return new EnteredPendingCaptureListFragment();
    }

    public EnteredPendingCaptureListFragment() {
        // Required empty public constructor
    }

    //----------------------------------------------------------------------------------------------
    // Overrides from BaseRefreshableQueryListFragment
    //----------------------------------------------------------------------------------------------


    @Override
    protected Class<EnteredPendingCapture> getQueryableClass() {
        return EnteredPendingCapture.class;
    }

    //----------------------------------------------------------------------------------------------
    // Overrides from BaseFragment
    //----------------------------------------------------------------------------------------------

    @Override
    public String getTitle() {
        return "Syncing Items";
    }

}
