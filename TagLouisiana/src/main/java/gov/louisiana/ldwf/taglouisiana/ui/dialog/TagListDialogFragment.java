package gov.louisiana.ldwf.taglouisiana.ui.dialog;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.FilterQueryProvider;
import android.widget.SimpleCursorAdapter;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.primitives.Longs;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import gov.louisiana.ldwf.taglouisiana.models.tag.Tag;
import gov.louisiana.ldwf.taglouisiana.models.tag.Tag_Table;
import gov.louisiana.ldwf.taglouisiana.ui.core.dialog.BaseCursorListDialogFragment;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

public class TagListDialogFragment extends BaseCursorListDialogFragment {

    public static final int REQUEST_CODE = Utils.getFreshInt();
    public static final String INTENT_TAG_NUMBERS = "TagListDialogFragment.intent.tag.numbers";

    // key for the fragment arguments
    private static final String KEY_TAG_LIST_ARG = "tag_list";


    private Set<Long> selectedTagIds;

    //----------------------------------------------------------------------------------------------
    // FACTORIES

    public static TagListDialogFragment newInstance(Fragment targetFragment, Set<String> tags){
        Bundle args = new Bundle();
        args.putStringArray(KEY_TAG_LIST_ARG, tags.toArray(new String[tags.size()]));

        TagListDialogFragment fragment = new TagListDialogFragment();
        fragment.setTargetFragment(targetFragment, REQUEST_CODE);
        fragment.setArguments(args);

        return fragment;
    }

    //----------------------------------------------------------------------------------------------
    // DialogFragment Overrides

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectedTagIds = getPreviouslySelectedTagIds();
    }

    //----------------------------------------------------------------------------------------------
    // BaseDialogFragment Overrides

    @Override
    public String getTitle() {
        return "Choose Tags";
    }

    @Override
    public boolean isMultipleChoice() {
        return true;
    }

    @Override
    public Intent createMultipleResultIntent(Set<Long> ids) {
        Intent intent = new Intent();
        intent.putExtra(INTENT_TAG_NUMBERS, Longs.toArray(ids));

        return intent;
    }

    @Override
    public Cursor getItemListCursor() {
        return Tag.getAvailableTagsQuery().flowQueryList().cursor();
    }

    @Override
    public FilterQueryProvider getCursorFilterQueryProvider() {
        return new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                return Tag.getAvailableTagsQuery(constraint.toString().toUpperCase()).flowQueryList().cursor();
            }
        };
    }

    @Override
    public Set<Long> getCheckedItemIds() {
        return selectedTagIds;
    }

    @Override
    public SimpleCursorAdapter getCursorAdapter() {
        return getCursorAdapter(new String[]{ "number" });
    }

    //----------------------------------------------------------------------------------------------
    // Helpers

    private Set<Long> getPreviouslySelectedTagIds() {
        Set<Long> result = new LinkedHashSet<>();

        Bundle args = getArguments();

        if (args != null) {
            String []tagNumbers = args.getStringArray(KEY_TAG_LIST_ARG);
            if (tagNumbers != null) {
                List<Tag> tags = SQLite.select().from(Tag.class).where(Tag_Table.number.in(Arrays.asList(tagNumbers))).queryList();
                Collection<Long> ids = Collections2.transform(tags, new Function<Tag, Long>() {
                    @Nullable
                    @Override
                    public Long apply(@Nullable Tag input) {
                        return input._id;
                    }
                });
                result.addAll(ids);
            }
        }

        return result;
    }
}