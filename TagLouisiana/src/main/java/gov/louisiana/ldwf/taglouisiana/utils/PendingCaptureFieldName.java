package gov.louisiana.ldwf.taglouisiana.utils;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 9/29/14
 */
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target({java.lang.annotation.ElementType.FIELD})
public @interface PendingCaptureFieldName {
    java.lang.String value();
}
