package gov.louisiana.ldwf.taglouisiana.models.fishEvents;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.Index;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.Date;

import gov.louisiana.ldwf.taglouisiana.models.RemoteItem;
import gov.louisiana.ldwf.taglouisiana.models.options.FishConditionOption;
import gov.louisiana.ldwf.taglouisiana.models.options.FishConditionOption_Table;
import gov.louisiana.ldwf.taglouisiana.models.options.TimeOfDayOption;
import gov.louisiana.ldwf.taglouisiana.models.options.TimeOfDayOption_Table;
import gov.louisiana.ldwf.taglouisiana.models.species.Species;
import gov.louisiana.ldwf.taglouisiana.models.species.SpeciesLength;
import gov.louisiana.ldwf.taglouisiana.models.species.SpeciesLength_Table;
import gov.louisiana.ldwf.taglouisiana.models.species.Species_Table;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

/**
 * Created by danielward on 8/17/16.
 */
public abstract class AbstractFishEvent extends RemoteItem {
    public static final String NO_GPS_VALUE = "NOGPS";

    @Expose
    @SerializedName("comments")
    @Column
    public String comments;

    @Expose
    @SerializedName("capture_date_epoch")
    @Column
    public Date date;

    @Expose
    @SerializedName("entered_gps_type")
    @Column
    public String gpsType;

    @Expose
    @SerializedName("length")
    @Column
    public Float length;

    @Expose
    @SerializedName("latitude")
    @Column
    public double latitude;

    @Expose
    @SerializedName("latitude_formatted")
    @Column
    public String latitudeFormatted;

    @Expose
    @SerializedName("longitude")
    @Column
    public double longitude;

    @Expose
    @SerializedName("longitude_formatted")
    @Column
    public String longitudeFormatted;

    @Index
    @Expose
    @SerializedName("tag_number")
    @Column
    public String tagNumber;

    @Expose
    @SerializedName("location_description")
    @Column
    public String locationDescription;

    @Expose
    @SerializedName("verified")
    @Column
    public boolean isVerified;

    @Expose
    @SerializedName("species_id")
    @ForeignKey(tableClass = Species.class)
    public long speciesId;

    @Expose
    @SerializedName("species_length_id")
    @ForeignKey(tableClass = SpeciesLength.class)
    public long speciesLengthId;

    @Expose
    @SerializedName(value = "fish_condition_id", alternate = {"fish_condition_option_id"})
    @ForeignKey(tableClass = FishConditionOption.class)
    public long fishConditionOptionId;

    @Expose
    @SerializedName(value = "time_of_day_id", alternate = {"time_of_day_option_id"})
    @ForeignKey(tableClass = TimeOfDayOption.class)
    public long timeOfDayOptionId;

    public Species species;
    public SpeciesLength speciesLength;
    public FishConditionOption fishConditionOption;
    public TimeOfDayOption timeOfDayOption;


    public String getLengthValue() {
        if (this.getSpeciesLength() != null) {
            return this.getSpeciesLength().description;
        } else if (this.length != null && this.length > 0f) {
            return Constants.DECIMAL_FORMATTER.format(this.length) + " inches";
        } else {
            return "N?A";
        }
    }

    public boolean hasValidLocation() {
        return (!gpsType.equals(NO_GPS_VALUE)) && (latitude > 0d) && (longitude < 0d);
    }

    public boolean hasShowableLocation() {
        return this.hasValidLocation();
    }

    public Species getSpecies() {
        if (species == null) {
            species = SQLite.select().from(Species.class).where(Species_Table._id.eq(speciesId)).querySingle();
        }

        return species;
    }

    public SpeciesLength getSpeciesLength() {
        if (speciesLength == null) {
            speciesLength = SQLite.select().from(SpeciesLength.class).where(SpeciesLength_Table._id.eq(speciesLengthId)).querySingle();
        }

        return speciesLength;
    }

    public FishConditionOption getFishConditionOption() {
        if (fishConditionOption == null) {
            fishConditionOption = SQLite.select().from(FishConditionOption.class).where(FishConditionOption_Table._id.eq(fishConditionOptionId)).querySingle();
        }

        return fishConditionOption;
    }



    public TimeOfDayOption getTimeOfDayOption() {
        if (timeOfDayOption == null) {
            timeOfDayOption = SQLite.select().from(TimeOfDayOption.class).where(TimeOfDayOption_Table._id.eq(timeOfDayOptionId)).querySingle();
        }

        return timeOfDayOption;
    }

    public LatLng getLatLng() {
        return new LatLng(latitude, longitude);
    }
}
