package gov.louisiana.ldwf.taglouisiana.ui.capture.create;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.sync.DataSyncAdapter;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseActivity;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

public class EnterCaptureActivity extends BaseActivity {

    public static final String SIGNED_IN_TAG = "EnterCaptureActivity.signed_in";
    public static final String REPORTING_RECAPTURE = "EnterCaptureActivity.reporting_recapture";
    public static final String ENTER_CAPTURE_ACTIVITY_TAG = "EnterCaptureActivityTag";

    private String toolbarTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null ) {
            resetPreferences();

            fragmentManager.beginTransaction()
                    .replace(R.id.activity_content_container, EnterCaptureWizard.newInstance(isReportingRecapture()), ENTER_CAPTURE_ACTIVITY_TAG)
                    .commit();
        }
        else {
            EnterCaptureWizard retrievedWizard = (EnterCaptureWizard) fragmentManager.findFragmentByTag(ENTER_CAPTURE_ACTIVITY_TAG);
            fragmentManager.beginTransaction()
                    .replace(R.id.activity_content_container, retrievedWizard)
                    .commit();
        }
        fragmentManager.executePendingTransactions();
        enableUpButton();
    }

    @Override
    protected void onResume() {
        super.onResume();

        toolbarTitle = isReportingRecapture() ? "Report Recapture" : "Enter Capture";
        setToolbarTitle(toolbarTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.activity_enter_capture, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void signInFinished() {
        super.signInFinished();

        Bundle bundle = new Bundle();
        bundle.putBoolean(DataSyncAdapter.TAG_DATA, true);
        requestSync(bundle);
    }

    private boolean isReportingRecapture() {
        return getIntent().getBooleanExtra(REPORTING_RECAPTURE, false);
    }

    private void resetPreferences() {
         /*
     * Clear the current page value when the activity is destroyed.
     * have to do it here, since the fragment's onDestroy method
     * was sometimes being invoked when we really did not want it to be.
     */
        this.getSharedPreferences("capture_pager_state", Context.MODE_PRIVATE)
                .edit()
                .clear()
                .apply();

    /*
     * Clear the saved values for entering a capture when this
     * activity is destroyed.
     */
        this.getSharedPreferences(Constants.SHARED_PREFS_TAG_LOUISIANA_ENTER_CAPTURE, MODE_PRIVATE)
                .edit()
                .clear()
                .apply();

    }
}
