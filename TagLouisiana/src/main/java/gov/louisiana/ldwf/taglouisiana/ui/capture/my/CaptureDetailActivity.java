package gov.louisiana.ldwf.taglouisiana.ui.capture.my;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseActivity;

/**
 * Created with IntelliJ IDEA.
 * User: danielward
 * Date: 2/12/13
 * Time: 9:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class CaptureDetailActivity extends BaseActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fragment f = CaptureDetailFragment.newInstance(this.getIntent().getLongExtra(CaptureDetailFragment.CAPTURE_DETAIL_ARG, -1));
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.activity_content_container, f);
        ft.commit();

        enableUpButton();
    }
}