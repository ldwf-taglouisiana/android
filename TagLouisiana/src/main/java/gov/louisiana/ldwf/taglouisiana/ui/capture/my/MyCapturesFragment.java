package gov.louisiana.ldwf.taglouisiana.ui.capture.my;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;

/**
 * Some code used from https://gist.github.com/3405429
 *
 * @author daniel
 */
public class MyCapturesFragment extends BaseFragment {

    private Fragment capturesListFragment;

    public MyCapturesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View myFragmentView = inflater.inflate(R.layout.my_capture_main, container, false);
        capturesListFragment = getFragmentManager().findFragmentById(R.id.captures_list_fragment);

        if (SQLite.select().from(Capture.class).count() > 0) {
            initHasCaptures();
        } else {
            initNoCaptures();
        }

        return myFragmentView;
    }

    private void initNoCaptures() {
        AQuery aq = new AQuery(this.getView());
        aq.id(R.id.captures_list_fragment).gone();
        aq.id(R.id.no_captures_placeholder).visible();
    }

    private void initHasCaptures() {
        AQuery aq = new AQuery(this.getView());
        aq.id(R.id.captures_list_fragment).visible();
        aq.id(R.id.no_captures_placeholder).gone();
    }

    @Override
    public void onPause() {
        super.onPause();
        getFragmentManager().beginTransaction().detach(capturesListFragment).commit();
    }
}
