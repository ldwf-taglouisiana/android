package gov.louisiana.ldwf.taglouisiana.accounts;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.google.gson.stream.MalformedJsonException;

import java.util.Calendar;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.net.ApiServiceGenerator;
import gov.louisiana.ldwf.taglouisiana.net.LoginService;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;
import retrofit2.Call;

public class ServerAuthenticate {

    static Bundle userSignIn(Context context, @NonNull  final String username, @NonNull final String password) throws Exception {

        LoginService loginService = ApiServiceGenerator.createService(LoginService.class);
        Call<OAuthToken> call = loginService.getAccessToken(
                username,
                password,
                "password", // The grant type
                context.getResources().getString(R.string.client_id),
                context.getResources().getString(R.string.client_secret)
        );

        OAuthToken accessToken;

        try {
            accessToken = call.execute().body();
        } catch(MalformedJsonException e){
            throw new Exception("Invalid email or password");
        }

        Bundle results = new Bundle();
        results.putString(Constants.OAUTH_ACCESS_TOKEN_KEY, accessToken.accessToken);
        results.putString(Constants.OAUTH_REFRESH_TOKEN_KEY, accessToken.refreshToken);
        results.putLong(Constants.OAUTH_TOKEN_EXPIRES_ON_KEY, nowPlusSecondToEpoch(accessToken.expiresIn));

        if (TextUtils.isEmpty(results.getString(Constants.OAUTH_ACCESS_TOKEN_KEY, "").trim())) {
            throw new Exception("Invalid email or password");
        }

        return results;
    }

    private static Long nowPlusSecondToEpoch(int seconds) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.SECOND, seconds);

        return c.getTime().getTime();
    }
}
