package gov.louisiana.ldwf.taglouisiana.models.tag;

import com.raizlabs.android.dbflow.annotation.Table;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/28/14
 */
@Table(database = AppDatabase.class)
public class EnteredTagRequest extends TagRequest {
}
