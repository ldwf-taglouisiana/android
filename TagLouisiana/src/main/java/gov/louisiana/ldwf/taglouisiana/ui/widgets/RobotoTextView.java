package gov.louisiana.ldwf.taglouisiana.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import gov.louisiana.ldwf.taglouisiana.R;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 10/4/14
 */
public class RobotoTextView extends TextView {

    public RobotoTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public RobotoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }

    public RobotoTextView(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {

            Typeface myTypeface;

            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.RobotoTextView);
            String fontName = a.getString(R.styleable.RobotoTextView_fontName);

            if (fontName != null) {

                switch (fontName) {
                    case "regular":
                        myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_regular.ttf");
                        break;
                    case "medium":
                        myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_medium.ttf");
                        break;
                    case "bold":
                        myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_bold.ttf");
                        break;
                    default:
                        myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_regular.ttf");
                        break;
                }

            } else {
                myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_regular.ttf");
            }


            setTypeface(myTypeface);

            a.recycle();
        }
    }
}
