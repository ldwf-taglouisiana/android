package gov.louisiana.ldwf.taglouisiana.accounts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by danielward on 8/25/16.
 */
public class OAuthToken {
    @Expose
    @SerializedName("access_token")
    public String accessToken;

    @Expose
    @SerializedName("refresh_token")
    public String refreshToken;

    @Expose
    @SerializedName("expires_in")
    public int expiresIn;
}
