package gov.louisiana.ldwf.taglouisiana.models.species;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.RemoteItem;
//import gov.louisiana.ldwf.taglouisiana.modelsRequery.fishEvents.Capture_Table;

/**
 * Created by danielward on 8/18/16.
 */
@Table(database = AppDatabase.class, primaryKeyConflict = ConflictAction.REPLACE)
public class Species extends RemoteItem {

    @Expose
    @SerializedName("common_name")
    @Column
    public String commonName;

    @Expose
    @SerializedName("other_name")
    @Column
    public String otherName;

    @Expose
    @SerializedName("proper_name")
    @Column
    public String properName;

    @Expose
    @SerializedName("family")
    @Column
    public String familyName;

    @Expose
    @SerializedName("description")
    @Column
    public String generalDescription;

    @Expose
    @SerializedName("size")
    @Column
    public String sizeDescription;

    @Expose
    @SerializedName("habitat")
    @Column
    public String habitatDescription;

    @Expose
    @SerializedName("food_value")
    @Column
    public String foodValueDescription;

    @Expose
    @SerializedName("position")
    @Column
    public long position;

    @Expose
    @SerializedName("target")
    @Column
    public boolean isTarget;

    @Expose
    @SerializedName("publish")
    @Column
    public boolean isPublished;

    @Expose
    @SerializedName("image_url_path")
    @Column
    public String imageUrl;

    List<SpeciesLength> speciesLengths;

    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "speciesLengths")
    public List<SpeciesLength> getSpeciesLengths() {
        if (speciesLengths == null || speciesLengths.isEmpty()) {
            speciesLengths = SQLite.select()
                        .from(SpeciesLength.class)
                        .where(SpeciesLength_Table.speciesId__id.eq(_id))
                        .queryList();
        }
        return speciesLengths;
    }
}
