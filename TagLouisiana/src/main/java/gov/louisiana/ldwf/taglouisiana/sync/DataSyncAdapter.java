package gov.louisiana.ldwf.taglouisiana.sync;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.database.sqlite.SQLiteDoneException;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.common.collect.ImmutableMap;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.runtime.FlowContentObserver;
import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.NameAlias;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.gson.responses.BaseCombinedResponse;
import gov.louisiana.ldwf.taglouisiana.gson.responses.BaseResponse;
import gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi.BasePostDataResponse;
import gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi.PostCombinedDataResponse;
import gov.louisiana.ldwf.taglouisiana.gson.responses.publicApi.CombinedResponse;
import gov.louisiana.ldwf.taglouisiana.models.RemoteItem;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.AbstractFishEvent;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.EnteredPendingCapture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.PendingCapture;
import gov.louisiana.ldwf.taglouisiana.models.tag.EnteredTagRequest;
import gov.louisiana.ldwf.taglouisiana.models.timeEntry.EnteredTimeOnWaterEntry;
import gov.louisiana.ldwf.taglouisiana.net.ApiServiceGenerator;
import gov.louisiana.ldwf.taglouisiana.net.ApiUtils;
import gov.louisiana.ldwf.taglouisiana.net.ProtectedApiService;
import gov.louisiana.ldwf.taglouisiana.net.PublicApiService;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class DataSyncAdapter extends BaseSyncAdapter {

    /*
        PUBLIC CONSTANTS
     */

    // keys for private data
    public static final String CAPTURE_DATA         = "captures_data";
    public static final String DRAFT_CAPTURE_DATA   = "draft_captures_data";
    public static final String TAG_DATA             = "tags_data";
    public static final String TAG_REQUESTS_DATA    = "tag_requests_data"; // TODO enable this at a later time
    public static final String TIME_LOGS_DATA       = "time_logs_data"; // TODO enable this at a later time
    public static final String PROFILE_DATA         = "me_data";

    public static final String[] ALL_PRIVATE_DATA = {
        CAPTURE_DATA,
        DRAFT_CAPTURE_DATA,
        TAG_DATA,
        PROFILE_DATA
    };

    public static final String[] ALL_CAPTURE_DATA = {
            CAPTURE_DATA,
            DRAFT_CAPTURE_DATA,
    };

    // keys for public data
    public static final String SPECIES_DATA         = "species_data";
    public static final String FISH_CONDITIONS_DATA = "fish_conditions_data";
    public static final String DISPOSITIONS_DATA    = "dispositions_data";
    public static final String TIME_OF_DAYS_DATA    = "time_of_days_data";
    public static final String SHIRT_SIZES_DATA     = "shirt_sizes_data";

    public static final String[] ALL_PUBLIC_DATA = {
            SPECIES_DATA,
            FISH_CONDITIONS_DATA,
            DISPOSITIONS_DATA,
            TIME_OF_DAYS_DATA,
            SHIRT_SIZES_DATA
    };


    // ---------------------------------------------------------------------------------------------


    public DataSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        if (extras == null) {
            extras = new Bundle();
        }

        /*
            New workflow.

            1. Fetch all the public data, as indicated.
            2. Fetch all the private data, as indicated.
            3. Post any private data, as indicated.
            4. Update the captures and draft_captures in the local database
         */

        // fetch any public data that may need to be processed
        fetchPublicData(extras, syncResult);

        /*
            If the user is signed in, then we can fetch any
            private information as well as, post any data that
            needs to be uploaded.
         */
        if (isSignedIn(account)) {
            // do a fetch first
            try {
                fetchPrivateData(extras, syncResult);
                // do a post to upload any information
                postPrivateData(syncResult);
                // fetch any changes in the private data set
                fetchPrivateData(extras, syncResult);

//                update the local database to reflect the changes in server
                updateCaptureData(extras, syncResult);

            }
            catch (AuthenticatorException e) {
                syncResult.stats.numAuthExceptions++;
                Utils.logError("Exception is DataSyncAdapter :" + e.getLocalizedMessage(), e);
            } catch (OperationCanceledException e) {
                Utils.logError("Exception is DataSyncAdapter :" + e.getLocalizedMessage(), e);
            } catch (IOException e) {
                syncResult.stats.numIoExceptions++;
                Utils.logError("Exception is DataSyncAdapter :" + e.getLocalizedMessage(), e);
            }

        }
    }

    /**
     *
     * @param bundle
     * @param syncResult
     */
    private void fetchPublicData(@NonNull Bundle bundle, @NonNull SyncResult syncResult) {
        Collection<String> includes = prepareIncludesParams(bundle, ALL_PUBLIC_DATA);

        // Add the includes param, will only receive the items requested
        Map<String, Object> queryParams = new LinkedHashMap<>();
        queryParams.put("includes", includes);

        // add the param keys and the model classes to build the request params
        Map<String, Class<? extends RemoteItem>> updatedAfterParams = new LinkedHashMap<>();
        updatedAfterParams.put("species", gov.louisiana.ldwf.taglouisiana.models.species.Species.class);
        updatedAfterParams.put("fish_conditions", gov.louisiana.ldwf.taglouisiana.models.options.FishConditionOption.class);
        updatedAfterParams.put("shirt_sizes", gov.louisiana.ldwf.taglouisiana.models.options.ShirtSizeOption.class);
        updatedAfterParams.put("dispositions", gov.louisiana.ldwf.taglouisiana.models.options.FishConditionOption.class);
        updatedAfterParams.put("time_of_days", gov.louisiana.ldwf.taglouisiana.models.options.TimeOfDayOption.class);

        // add all the params with the max updated_at
        queryParams.putAll(buildUpdatedAfterParams(updatedAfterParams));

        // prepare the API call
        PublicApiService apiService = ApiServiceGenerator.createService(PublicApiService.class);
        Call<CombinedResponse> call = apiService.getCombined(ApiUtils.convertMapToQueryMap(queryParams));

        // execute the call and do any database updates as needed
        processCall(call, syncResult);
    }

    /**
     *
     * @param bundle
     * @param syncResult
     * @throws AuthenticatorException
     * @throws OperationCanceledException
     * @throws IOException
     */
    private void fetchPrivateData(@NonNull Bundle bundle, @NonNull SyncResult syncResult) throws AuthenticatorException, OperationCanceledException, IOException {
        Collection<String> includes = prepareIncludesParams(bundle, ALL_PRIVATE_DATA);

        // Add the includes param, will only receive the items requested
        Map<String, Object> queryParams = new LinkedHashMap<>();
        queryParams.put("includes", includes);

        // add the param keys and the model classes to build the request params
        Map<String, Class<? extends RemoteItem>> updatedAfterParams = new LinkedHashMap<>();
        updatedAfterParams.put("captures", gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture.class);
        updatedAfterParams.put("draft_captures", gov.louisiana.ldwf.taglouisiana.models.fishEvents.PendingCapture.class);
        updatedAfterParams.put("tags", gov.louisiana.ldwf.taglouisiana.models.tag.Tag.class);

        // add all the params with the max updated_at
        queryParams.putAll(buildUpdatedAfterParams(updatedAfterParams));

        // prepare the API call
        ProtectedApiService apiService = ApiServiceGenerator.createService(ProtectedApiService.class);
        Call<gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi.CombinedResponse> call = apiService.getCombined(ApiUtils.convertMapToQueryMap(queryParams));

        // execute the call and do any database updates as needed
        processCall(call, syncResult);
    }

    /**
     *
     * @param syncResult
     */
    private void postPrivateData(@NonNull final SyncResult syncResult) {
        final Map<String, Object> body = new LinkedHashMap<>();

        final Map<String, Class<? extends BaseModel>> payloadItems = new HashMap<>();
        payloadItems.put("draft_captures", EnteredPendingCapture.class);
        payloadItems.put("tag_requests", EnteredTagRequest.class);
        payloadItems.put("time_logs", EnteredTimeOnWaterEntry.class);

        ITransaction transaction = new ITransaction() {
            @Override
            public void execute(DatabaseWrapper databaseWrapper) {
                for(Map.Entry<String, Class<? extends BaseModel>> entry : payloadItems.entrySet()) {
                    /*
                        This would throw a SQLiteDoneException randomly. This seems to be due to having
                        queried and returned no rows, which we can assume the count would then be 0.
                     */
                    try {
                        if (SQLite.selectCountOf().from(entry.getValue()).count(databaseWrapper) > 0) {
                            body.put(entry.getKey(), ImmutableMap.of("items", SQLite.select().from(entry.getValue()).queryList(databaseWrapper)));
                        }
                    } catch (SQLiteDoneException e) {
                        Utils.logError(e);
                    }
                }
            }
        };
        DatabaseDefinition database = FlowManager.getDatabase(AppDatabase.class);
        database.executeTransaction(transaction);

        /*
            If the body is not empty then we need to make the call to post the data to the API
         */
        if (!body.isEmpty()) {
            ProtectedApiService apiService = ApiServiceGenerator.createService(ProtectedApiService.class);
            Call<PostCombinedDataResponse> call = apiService.postCombined(body);
            processPostDataCall(call, syncResult);
        }
    }



    /**
     *
     * @param bundle
     * @param syncResult
     * @throws AuthenticatorException
     * @throws OperationCanceledException
     * @throws IOException
     */
    private void updateCaptureData(@NonNull Bundle bundle, @NonNull SyncResult syncResult) throws AuthenticatorException, OperationCanceledException, IOException {
        Collection<String> includes = prepareIncludesParams(bundle, ALL_CAPTURE_DATA);

        // Add the includes param, will only receive the items requested
        Map<String, Object> queryParams = new LinkedHashMap<>();
        queryParams.put("includes", includes);

        // add the param keys and the model classes to build the request params
        Map<String, Class<? extends RemoteItem>> updatedAfterParams = new LinkedHashMap<>();
        updatedAfterParams.put("captures", gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture.class);
        updatedAfterParams.put("draft_captures", gov.louisiana.ldwf.taglouisiana.models.fishEvents.PendingCapture.class);
        // add all the params with the max updated_at
        queryParams.putAll(buildOnlyIdListParams(updatedAfterParams));

        // prepare the API call
        ProtectedApiService apiService = ApiServiceGenerator.createService(ProtectedApiService.class);
        Call<ResponseBody> call = apiService.getCombinedGeneric(ApiUtils.convertMapToQueryMap(queryParams));

        // execute the call and do any database updates as needed
        processUpdateCall(call, syncResult);
    }




    // ---------------------------------------------------------------------------------------------
    // HELPERS
    // ---------------------------------------------------------------------------------------------

    /**
     *
     * @param call
     * @param syncResult
     * @param <U>
     */
    private  <U extends BaseResponse & BaseCombinedResponse> void processCall(Call<U> call, SyncResult syncResult) {
        try {
            // get the response result form the API request
            retrofit2.Response<U> callResponse = call.execute();
            final U combinedResponse = callResponse.body();

            if (combinedResponse == null) {
                throw new IllegalStateException("The response body was null from: " + call.request().url().toString());
            } else {
                FlowContentObserver observer = new FlowContentObserver();
                observer.beginTransaction();

                // for each received response, we need to insert the fetched items
                for(BaseResponse response : combinedResponse.getResponses()) {
                    insertItems(response.getItems(), syncResult);
                }


                observer.endTransactionAndNotify();
            }

        } catch (IOException e) {
            Utils.logError(e);
            syncResult.stats.numIoExceptions++;
        }
    }

    private void processPostDataCall(Call<PostCombinedDataResponse> call, final SyncResult syncResult) {
        try {
            final Response<PostCombinedDataResponse> response = call.execute();
            /*
                If the server replied with a 200, then we can continue processing the response
             */

            if (response.code() == 200) {
                // create a transaction to clean up the local data set.
                ITransaction transaction = new ITransaction() {
                    @Override
                    public void execute(DatabaseWrapper databaseWrapper) {
                        // iterate through all the responses and remove all the processed items by UUID
                        for (Map.Entry<Class<? extends BaseModel>, BasePostDataResponse> entry : response.body().getReponseItems().entrySet()) {

                            // If the API replied back with any UUIDs, then we need to purge them from our data set.
                            if (!entry.getValue().processedUUIDS.isEmpty()) {
                                SQLite.delete()
                                        .from(entry.getKey())
                                        .where(
                                                Condition.column(
                                                        NameAlias.builder("uuid").build()
                                                ).in(entry.getValue().getUUIDStringList())
                                        ).execute(databaseWrapper);
                            }

                            // increment the numDeleted stats counter
                            syncResult.stats.numDeletes += entry.getValue().processedUUIDS.size();
                        }
                    }
                };

                FlowContentObserver observer = new FlowContentObserver();
                observer.beginTransaction();

                FlowManager.getDatabase(AppDatabase.class).executeTransaction(transaction);

                observer.endTransactionAndNotify();

            } else {
                Utils.logError("There was an error executing the call for: " + response.raw().request().url() + " with error: "  + response.errorBody().string());
            }

        } catch (IOException e) {
            Utils.logError(e);
            syncResult.stats.numIoExceptions++;
        }
    }


    /**
     *
     * @param call
     * @param syncResult
     */
    private  void processUpdateCall(Call<ResponseBody> call, SyncResult syncResult) {
        try {
            // get the response result form the API request
            Response<ResponseBody> callResponse = call.execute();

            if (callResponse.body() == null) {
                throw new IllegalStateException("The response body was null from: " + call.request().url().toString());
            } else {
                final String responseBody = callResponse.body().string();
                JSONObject response = new JSONObject(responseBody);

                for (Iterator<String> keys = response.keys(); keys.hasNext(); ) {
                    String key = keys.next();

                    if(key.equals("captures") || key.equals("draft_captures")) {
                        JSONArray jsonArray = response.getJSONObject(key).getJSONArray("ids");

                        if (key.equals("captures")) {
                            updateLocalDB(jsonArray, Capture.class, syncResult);
                        } else if (key.equals("draft_captures")) {
                            updateLocalDB(jsonArray, PendingCapture.class, syncResult);
                        }
                    }
                }
            }
        } catch (IOException | JSONException e) {
            Utils.logError(e);
            syncResult.stats.numIoExceptions++;
        }
    }

    private <T extends AbstractFishEvent> void updateLocalDB(JSONArray jsonArray, final Class table, SyncResult syncResult) throws JSONException{
        final List<Long> serverIdList = getIdsFromJSONArray(jsonArray);

        ITransaction transaction = new ITransaction() {
            @Override
            public void execute(DatabaseWrapper databaseWrapper) {
                // If the id is not in the list sent by the server then we should delete it

                SQLite.delete()
                        .from(table)
                        .where(
                                Condition.column(
                                        NameAlias.builder("_id").build()
                                ).notIn(serverIdList)
                        ).execute(databaseWrapper);
            }

        };
        syncResult.stats.numDeletes++;

        FlowContentObserver observer = new FlowContentObserver();
        observer.beginTransaction();

        FlowManager.getDatabase(AppDatabase.class).executeTransaction(transaction);

        observer.endTransactionAndNotify();

    }

    private List<Long> getIdsFromJSONArray(JSONArray jsonArray) throws JSONException{
        List<Long> list = new ArrayList<>();
        for(int i=0; i< jsonArray.length(); i++){
            list.add(jsonArray.getLong(i));
        }
        return list;
    }

}