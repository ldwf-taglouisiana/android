package gov.louisiana.ldwf.taglouisiana.ui.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import gov.louisiana.ldwf.taglouisiana.ui.core.dialog.BaseDialogFragment;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

public class TagImageDialogFragment extends BaseDialogFragment<String> {

    public static final int REQUEST_CODE = Utils.getFreshInt();

    public static final String INTENT_TAG_NUMBER = "TagImageDialogFragment.intent.tag_number";

    private static final String TAG_NUMBERS = "TagImageDialogFragment.tag_numbers";

    public static TagImageDialogFragment newInstance(Fragment targetFragment, Set<String> tagNumbers) {
        Bundle args = new Bundle();
        args.putStringArray(TAG_NUMBERS, tagNumbers.toArray(new String[tagNumbers.size()]));

        TagImageDialogFragment fragment = new TagImageDialogFragment();
        fragment.setTargetFragment(targetFragment, REQUEST_CODE);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public Intent createResultIntent(String item) {
        Intent intent = new Intent();
        intent.putExtra(INTENT_TAG_NUMBER, item);
        return intent;
    }

    @Override
    public List<String> getDisplayList() {
        return getItemList();
    }

    @Override
    public List<String> getItemList() {
        return Arrays.asList(getArguments().getStringArray(TAG_NUMBERS));
    }

    @Override
    public String getTitle() {
        return "Select Tag Number for Image";
    }
}