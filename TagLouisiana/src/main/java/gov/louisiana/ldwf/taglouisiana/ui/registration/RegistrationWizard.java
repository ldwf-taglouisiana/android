package gov.louisiana.ldwf.taglouisiana.ui.registration;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.TreeMap;

import gov.louisiana.ldwf.taglouisiana.ui.wizard.Page;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Wizard;
import gov.louisiana.ldwf.taglouisiana.utils.Common;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

/**
 * Created by daniel on 5/16/14.
 */
public class RegistrationWizard extends Wizard {

    private static final String PREFS = Constants.SHARED_PREFS_TAG_LOUISIANA_REGISTRATION;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initPages();

        return view;
    }

    private void initPages() {
        this.addPages(
                buildContactFragment(),
                buildAddressFragment(),
                buildPasswordFragment()
        );
    }

    private Page buildContactFragment() {
        ContactInfoFragment.Builder builder = new ContactInfoFragment.Builder();
        builder.setPrefs(PREFS);

        return builder.create();
    }

    private Page buildAddressFragment() {
        AddressInfoFragment.Builder builder = new AddressInfoFragment.Builder();
        builder.setPrefs(PREFS);

        return builder.create();
    }

    private Page buildPasswordFragment() {
        PasswordInfoFragment.Builder builder = new PasswordInfoFragment.Builder();
        builder.setPrefs(PREFS);

        return builder.create();
    }

    @Override
    public void onSubmissionSelected() {
        SharedPreferences prefs = getActivity().getSharedPreferences(Constants.SHARED_PREFS_TAG_LOUISIANA_REGISTRATION, Context.MODE_PRIVATE);

        Map<String, Object> result = new TreeMap<>();

        Map<String, String> angler = new TreeMap<>();
        Map<String, String> user = new TreeMap<>();

        angler.put(Constants.REGISTER_FIRST_NAME, prefs.getString(Constants.REGISTER_FIRST_NAME, ""));
        angler.put(Constants.REGISTER_LAST_NAME, prefs.getString(Constants.REGISTER_LAST_NAME, ""));
        angler.put(Constants.REGISTER_STREET, prefs.getString(Constants.REGISTER_STREET, ""));
        angler.put(Constants.REGISTER_SUITE, prefs.getString(Constants.REGISTER_SUITE, ""));
        angler.put(Constants.REGISTER_CITY, prefs.getString(Constants.REGISTER_CITY, ""));
        angler.put(Constants.REGISTER_STATE, prefs.getString(Constants.REGISTER_STATE, ""));
        angler.put(Constants.REGISTER_ZIP_CODE, prefs.getString(Constants.REGISTER_ZIP_CODE, ""));
        angler.put(Constants.REGISTER_PHONE_NUMBER, prefs.getString(Constants.REGISTER_PHONE_NUMBER, ""));
        angler.put(Constants.REGISTER_SHIRT_SIZE, prefs.getString(Constants.REGISTER_SHIRT_SIZE, ""));

        user.put(Constants.REGISTER_EMAIL, prefs.getString(Constants.REGISTER_EMAIL, ""));
        user.put(Constants.REGISTER_PASSWORD, prefs.getString(Constants.REGISTER_PASSWORD, ""));
        user.put(Constants.REGISTER_PASSWORD_CONFIRMATION, prefs.getString(Constants.REGISTER_PASSWORD_CONFIRMATION, ""));

        result.put("angler", angler);
        result.put("user", user);

        try {
            result.put("token", Common.getOauthHash());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        new PostRegistrationTask().execute(result);

    }

    protected void registrationSuccessful() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Registered!!");
        alertDialogBuilder.create().show();

        getActivity().getSharedPreferences(Constants.SHARED_PREFS_TAG_LOUISIANA_REGISTRATION, Context.MODE_PRIVATE)
                .edit()
                .clear()
                .apply();
    }

    protected void registrationFailed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("There was an error");
        alertDialogBuilder.create().show();
    }

    public class PostRegistrationTask extends AsyncTask<Map<String, Object>, Object, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //TODO show a progress indicator
        }

        @Override
        protected String doInBackground(Map<String, Object>... params) {

            String result = null;

            // TODO change this to use Retrofit
//            try {
//                result = ApiWrapper.POST(RegistrationWizard.this.getBaseActivity(), Api.REGISTRATION_URL, params[0], new HashMap<String, String>());
//            } catch (NetworkErrorException | IOException e) {
//                e.printStackTrace();
//            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result != null) {
                registrationSuccessful();
            } else {
                registrationFailed();
            }
        }
    }
}
