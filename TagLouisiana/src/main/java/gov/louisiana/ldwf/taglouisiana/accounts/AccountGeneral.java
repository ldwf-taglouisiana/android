package gov.louisiana.ldwf.taglouisiana.accounts;

import gov.louisiana.ldwf.taglouisiana.BuildConfig;

public class AccountGeneral {

  /**
   * Account type id
   */
  public static final String ACCOUNT_TYPE = buildAuthority();

  /**
   * Account name
   */
  public static final String ACCOUNT_NAME = "Tag Louisiana";

  /**
   * Auth token types
   */
  public static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";
  public static final String AUTHTOKEN_TYPE_DEFAULT = "DEFAULT_ACCOUNT_TOKEN_TYPE";
  public static final String AUTHTOKEN_TYPE_FULL_ACCESS_LABEL = "Full access to an Tag Louisiana account";

  public static final String DEFAULT_TOKEN = "some_really_crappy_token";

  private static String buildAuthority() {
    String authority = "gov.ldwf.taglouisiana.accounts";

    if (!BuildConfig.BUILD_TYPE.equals("release")) {
      authority += "." + BuildConfig.BUILD_TYPE;
    }

    return authority;
  }
}