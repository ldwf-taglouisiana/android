package gov.louisiana.ldwf.taglouisiana.gson.responses;

import java.util.Collection;

/**
 * Created by danielward on 8/31/16.
 */
public interface BaseCombinedResponse {
    public Collection<BaseResponse> getResponses();
}
