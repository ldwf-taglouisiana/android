package gov.louisiana.ldwf.taglouisiana.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.geojson.GeoJsonFeature;
import com.google.maps.android.geojson.GeoJsonGeometry;
import com.google.maps.android.geojson.GeoJsonLayer;
import com.google.maps.android.geojson.GeoJsonPoint;
import com.google.maps.android.geojson.GeoJsonPolygon;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by danielward on 10/26/16.
 */

public class GeoUtil {

    public static boolean hasGeometry(GeoJsonLayer layer) {
        boolean result = false;

        for (GeoJsonFeature feature : layer.getFeatures()) {
            result = result || feature.hasGeometry();
        }

        return result;
    }
    /*
        http://stackoverflow.com/a/37497144/2614663
     */
    public static LatLngBounds geoJsonLatLngBounds(GeoJsonLayer layer) {
        // Get the bounding box builder.
        LatLngBounds.Builder builder = LatLngBounds.builder();

        // Get the coordinates
        for (GeoJsonFeature feature : layer.getFeatures()) {
            if (feature.hasGeometry()) {
                GeoJsonGeometry geometry = feature.getGeometry();

                if (geometry instanceof GeoJsonPolygon) {
                    List<? extends List<LatLng>> lists = ((GeoJsonPolygon) geometry).getCoordinates();
                    // Feed the coordinates to the builder.
                    for (List<LatLng> list : lists) {
                        for (LatLng latLng : list) {
                            builder.include(latLng);
                        }
                    }

                } else if (geometry instanceof GeoJsonPoint) {
                    builder.include(((GeoJsonPoint) geometry).getCoordinates());
                }


            }
        }

        // Assign the builder's return value to a bounding box.
        return builder.build();
    }

    public static String formatEnteredLocationString(EnteredLocation enteredLocation) {
        DecimalFormat df = new DecimalFormat("###.##");

        String lat_string = "";
        String lon_string = "";

        if (enteredLocation.getGPSType().equals(EnteredLocation.DECIMAL_FORMAT)) {
            df = new DecimalFormat("###.####");
            lat_string += df.format(enteredLocation.getLatitude()) + "°";
            lon_string += df.format(enteredLocation.getLongitude()) + "°";
        }

        if (enteredLocation.getGPSType().equals(EnteredLocation.DEGREE_MINUTE_FORMAT)) {
            GeoCoord coords = new GeoCoord(enteredLocation.getLatitude(), enteredLocation.getLongitude());
            String[] s = coords.getDM();
            lat_string = (int) Math.floor(Float.parseFloat(s[0])) + "° " + df.format(Float.parseFloat(s[1])) + "'";
            lon_string = (int) Math.floor(Float.parseFloat(s[2])) + "° " + df.format(Float.parseFloat(s[3])) + "'";
        }

        if (enteredLocation.getGPSType().equals(EnteredLocation.DEGREE_MINUTE_SECOND_FORMAT)) {
            GeoCoord coords = new GeoCoord(enteredLocation.getLatitude(), enteredLocation.getLongitude());
            String[] s = coords.getDMS();

            lat_string = (int) Math.floor(Float.parseFloat(s[0])) + "° " + (int) Math.floor(Float.parseFloat(s[1])) + "' " + df.format(Float.parseFloat(s[2])) + "\"";
            lon_string = (int) Math.floor(Float.parseFloat(s[3])) + "° " + (int) Math.floor(Float.parseFloat(s[4])) + "' " + df.format(Float.parseFloat(s[5])) + "\"";
        }

        return lat_string + ", " + lon_string;
    }
}
