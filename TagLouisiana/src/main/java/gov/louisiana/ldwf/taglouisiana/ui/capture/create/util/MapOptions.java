package gov.louisiana.ldwf.taglouisiana.ui.capture.create.util;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 9/22/14
 */
public class MapOptions {

    boolean markerOnUserLocation;
    boolean addMarkerOnTouch;
    int markerLimit;

    public MapOptions() {
        this.markerOnUserLocation = true;
        this.addMarkerOnTouch = true;
        this.markerLimit = 1;
    }

    public boolean hasMarkerLimit() {
        return this.markerLimit != 0;
    }

    public boolean isMarkerOnUserLocation() {
        return markerOnUserLocation;
    }

    public MapOptions setMarkerOnUserLocation(boolean markerOnUserLocation) {
        this.markerOnUserLocation = markerOnUserLocation;

        return this;
    }

    public boolean isAddMarkerOnTouch() {
        return addMarkerOnTouch;
    }

    public MapOptions setAddMarkerOnTouch(boolean addMarkerOnTouch) {
        this.addMarkerOnTouch = addMarkerOnTouch;

        return this;
    }

    public int getMarkerLimit() {
        return markerLimit;
    }

    public MapOptions setMarkerLimit(int markerLimit) {
        this.markerLimit = markerLimit;

        return this;
    }

    public MapOptions setHasMarkerLimit(boolean hasLimit) {
        if (!hasLimit) {
            this.markerLimit = 0;
        }

        return this;
    }
}
