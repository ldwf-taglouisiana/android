package gov.louisiana.ldwf.taglouisiana.gson.typeAdapters;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

import gov.louisiana.ldwf.taglouisiana.models.fishEvents.EnteredPendingCapture;

/**
 * Created by danielward on 10/31/16.
 */

public class EnteredPendingCaptureTypeAdapterFactory extends BeforeAfterTypeAdapterFactory<EnteredPendingCapture> {
    public EnteredPendingCaptureTypeAdapterFactory() {
        super(EnteredPendingCapture.class);
    }

    @Override
    protected void beforeWrite(EnteredPendingCapture source, JsonElement toSerialize) {
        if (source.getPendingCapture() == null) {
            toSerialize.getAsJsonObject().add("id", new JsonPrimitive(-1));
        }
    }
}
