package gov.louisiana.ldwf.taglouisiana.ui.capture.my;


import android.accounts.NetworkErrorException;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.geojson.GeoJsonLayer;
import com.google.maps.android.geojson.GeoJsonPointStyle;
import com.google.maps.android.geojson.GeoJsonPolygonStyle;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi.CaptureHistoryResponse;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.AbstractFishEvent;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture_Table;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Recapture;
import gov.louisiana.ldwf.taglouisiana.net.ApiServiceGenerator;
import gov.louisiana.ldwf.taglouisiana.net.ProtectedApiService;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created with IntelliJ IDEA.
 * User: danielward
 * Date: 2/12/13
 * Time: 9:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class CaptureDetailFragment extends BaseFragment implements OnMapReadyCallback {

    public static final String CAPTURE_DETAIL_ARG = "CaptureDetailFragment.capture_id";
    public static final String CAPTURE_DETAIL_RECAPTURE_JSON_ARG = "CaptureDetailFragment.recapture_json";

    private GoogleMap googleMap;
    private Handler handler;

    //----------------------------------------------------------------------------------------------
    // Instance Builders

    public static CaptureDetailFragment newInstance(Long captureId) {
        Bundle args = new Bundle();
        args.putLong(CAPTURE_DETAIL_ARG, captureId);

        CaptureDetailFragment fragment = new CaptureDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static CaptureDetailFragment newInstance(Capture capture) {
        return CaptureDetailFragment.newInstance(capture._id);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        if (capture.hasValidLocation()) {
            googleMap.addMarker(generateMarkerOptions(this.capture));
            centerMapOnCapture(this.capture);
        }

        if (fragments.size() <= 1) {
            getHistory();
        }
    }

    //----------------------------------------------------------------------------------------------
    // Instance Variables

    private Capture capture;
    private List<AbstractFishEvent> captures;
    private List<CapturesDetailInfoFragment> fragments;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.captures = new ArrayList<>(2);
        this.capture = getRequestedCapture();
        this.fragments = new ArrayList<>(2);

        this.captures.add(this.capture);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.capture_detail_fragment, container, false);
        handler = new Handler(Looper.getMainLooper());

        this.fragments.add(CapturesDetailInfoFragment.newInstance(capture));
        setUpViewPager(new PagerAdapter(this.getFragmentManager(), this.fragments), view);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        this.getBaseActivity().setToolbarTitle(this.capture.tagNumber);
    }

    private void setUpViewPager(PagerAdapter pagerAdapter, View view) {
        if (view == null) view = getView();

        ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);

        PagerSlidingTabStrip strip = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
        strip.setViewPager(pager);
        strip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
                // left blank
            }

            @Override
            public void onPageSelected(int i) {
                centerMapOnCapture(captures.get(i));
            }

            @Override
            public void onPageScrollStateChanged(int i) {
                // left blank
            }
        });
    }

    private Capture getRequestedCapture() {
        return  SQLite.select().from(Capture.class).where(Capture_Table._id.eq(getArguments().getLong(CAPTURE_DETAIL_ARG))).querySingle();
    }

    private void centerMapOnCapture(AbstractFishEvent fishEvent) {
        if (this.googleMap != null) {

            if (fishEvent instanceof Capture) {
                this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(fishEvent.getLatLng(), 8.0f));

            } else if (fishEvent instanceof Recapture) {
                final Recapture recapture = (Recapture) fishEvent;
                if (recapture.hasShowableLocation()) {
                    this.googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(recapture.getGeoJsonBounds(), 20));
                }
            }

        }
    }

    private GeoJsonLayer addEventToMap(Recapture recapture) {
        if (this.googleMap != null) {
            try {
                final JSONObject geoJson = new JSONObject(recapture.geoJson.toString());
                GeoJsonLayer layer = new GeoJsonLayer(googleMap, geoJson);

                int[] strokes = getContext().getResources().getIntArray(R.array.strokes);
                int[] fills = getContext().getResources().getIntArray(R.array.fills);

                if (strokes.length >= recapture.recaptureNumber) {
                    float[] hsv = new float[3];
                    String hexColor = String.format("#%06X", (0xFFFFFF & strokes[recapture.recaptureNumber - 1]));
                    Color.colorToHSV(Color.parseColor(hexColor), hsv);

                    // Get the icon for the feature
                    BitmapDescriptor pointIcon = BitmapDescriptorFactory.defaultMarker(hsv[0]);

                    GeoJsonPointStyle pointStyle = layer.getDefaultPointStyle();
                    pointStyle.setIcon(pointIcon);
                    pointStyle.setTitle(recapture.tagNumber);
                    pointStyle.setSnippet(Constants.US_DATE_FORMATTER.format(recapture.date));



                    GeoJsonPolygonStyle polygonStyle = layer.getDefaultPolygonStyle();
                    polygonStyle.setStrokeColor(strokes[recapture.recaptureNumber - 1]);
                    polygonStyle.setFillColor(fills[recapture.recaptureNumber - 1]);
                }

                layer.addLayerToMap();

                return layer;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private MarkerOptions generateMarkerOptions(AbstractFishEvent fishEvent) {
        String snippet = Constants.US_DATE_FORMATTER.format(fishEvent.date);
        MarkerOptions result = new MarkerOptions().position(fishEvent.getLatLng()).title(fishEvent.tagNumber).snippet(snippet);

        if (fishEvent instanceof Recapture) {
            result.icon(BitmapDescriptorFactory.fromResource(R.drawable.recapture));
        } else {
            result.icon(BitmapDescriptorFactory.fromResource(R.drawable.captured_marker));
        }

        return result;
    }

    //----------------------------------------------------------------------------------------------
    // PAGER ADAPTER

    private class PagerAdapter extends FragmentStatePagerAdapter {

        private List<CapturesDetailInfoFragment> fragments;

        public PagerAdapter(FragmentManager fm, List<CapturesDetailInfoFragment> fragments) {
            super(fm);

            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragments.get(position).getTitle();
        }
    } // end pager adapter

    private void getHistory() {

        ProtectedApiService apiService = ApiServiceGenerator.createService(ProtectedApiService.class);
        Call<CaptureHistoryResponse> call = apiService.fishEventHistory(this.capture._id);

        final Context context = getContext();

        call.enqueue(new Callback<CaptureHistoryResponse>() {
            @Override
            public void onResponse(Call<CaptureHistoryResponse> call, Response<CaptureHistoryResponse> response) {
                try {
                    if (response.code() == 200) {

                        CaptureHistoryResponse captureHistoryResponse = response.body();

                        for (final Recapture recapture : captureHistoryResponse.getRecaptures()) {

                            // ensure the following runs on the main thread
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    GeoJsonLayer layer = addEventToMap(recapture);
                                    recapture.setGeoJsonLayer(layer);
                                    captures.add(recapture);

                                    fragments.add(CapturesDetailInfoFragment.newInstance(recapture));
                                    setUpViewPager(new PagerAdapter(CaptureDetailFragment.this.getFragmentManager(), fragments), null);
                                }
                            });
                        }

                    } else {
                        throw new NetworkErrorException("Received a " + response.code() + " from the request: " + call.request().url().toString());
                    }

                } catch (NetworkErrorException e) {
                    Utils.logError(call, e);
                }
            }

            @Override
            public void onFailure(final Call<CaptureHistoryResponse> call, final Throwable t) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context,R.string.stats_fetched_error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    } // end getHistory()

}