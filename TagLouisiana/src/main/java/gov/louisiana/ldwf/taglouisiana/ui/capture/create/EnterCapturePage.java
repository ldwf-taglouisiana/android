package gov.louisiana.ldwf.taglouisiana.ui.capture.create;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.NameAlias;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.transaction.QueryTransaction;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.models.RemoteItem;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Page;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

/**
 * Created by daniel on 5/23/14.
 */
public abstract class EnterCapturePage extends Page {

  protected  <T extends RemoteItem> void getModelByIdAsync(Class<T> model, final long id, QueryTransaction.QueryResultSingleCallback<T> callback) {
    SQLite.select()
            .from(model)
            .where(Condition.column(NameAlias.builder("_id").build()).eq(id))
            .async()
            .querySingleResultCallback(callback)
            .execute();
  }

  protected void removeValidationBorder(View view) {
    Crouton.cancelAllCroutons();
    RelativeLayout rl = (RelativeLayout) view.getParent();
    rl.setBackgroundResource(R.drawable.transparent);
  }

  protected void addValidationBorder(View view) {
    RelativeLayout rl = (RelativeLayout) view.getParent();
    rl.setBackgroundResource(R.drawable.red_border);
  }

  protected void initCoachMarks(String prefsKey) {
    SharedPreferences prefs = getActivity().getSharedPreferences(Constants.SHARED_PREFS_TAG_LOUISIANA, Context.MODE_PRIVATE);

    // if the marks have been shown before then hide the overlay
    if (prefs.getBoolean(prefsKey, false)) {
      ViewGroup group = (ViewGroup) this.getView().findViewById(R.id.overlay);
      group.setVisibility(View.GONE);
      if (group != null) {
        group.setVisibility(View.GONE);
        for (int i = 0; i < group.getChildCount(); i++) {
          ImageView imageView = (ImageView) group.getChildAt(i);
          Drawable drawable = imageView.getDrawable();
          if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            bitmap.recycle();
          }
        }
      }

    }
    // if the marks have not been shown before then add the click listener
    // and write to prefs that we have shown the marks
    else {
      this.getView().findViewById(R.id.overlay).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          EnterCapturePage.this.getView().findViewById(R.id.overlay).setVisibility(View.GONE);
          ViewGroup group = (ViewGroup) EnterCapturePage.this.getView().findViewById(R.id.overlay);
          if (group != null) {
            group.setVisibility(View.GONE);
            for (int i = 0; i < group.getChildCount(); i++) {
              ImageView imageView = (ImageView) group.getChildAt(i);
              Drawable drawable = imageView.getDrawable();
              if (drawable instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                Bitmap bitmap = bitmapDrawable.getBitmap();
                bitmap.recycle();
              }
            }
          }
        }
      });

      prefs.edit().putBoolean(prefsKey, true).commit();
    }
  }

  protected void showHelp() {
    View view = this.getView().findViewById(R.id.overlay);

    if (view != null) {
      view.setVisibility(View.VISIBLE);

      view.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          v.setVisibility(View.GONE);
        }
      });
    }
  }
}
