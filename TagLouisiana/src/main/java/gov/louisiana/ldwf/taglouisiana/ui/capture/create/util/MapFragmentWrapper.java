package gov.louisiana.ldwf.taglouisiana.ui.capture.create.util;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;

import java.util.ArrayList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.MainApplication;
import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

public class MapFragmentWrapper extends SupportMapFragment implements MapFragmentAddons, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    // set default latitude and longitude when there is no user location
    public static final double DEFAULT_LATITUDE = 29.723838893362693;
    public static final double DEFAULT_LONGITUDE = -89.79330528527498;

    // a default location to initialize the map too.
    private static final LatLng DEFAULT_LOCATION = new LatLng(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);

    private final static int PLAY_SERVICES_CONNECTION_FAILURE_RESOLUTION_REQUEST = 6000;
    public static final int MAP_ANIMATION_DURATION_MS = 500;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private GoogleMap googleMap;

    private MapOptions mapOptions;

    // list of markers that are on the map
    private List<Marker> markers;

    // the listener that can be passed to the this fragment to pass along any changes in the map
    private LocationChangedListener listener;

    private static MapFragmentWrapper singletonInstance;
    private CameraPosition oldCameraPosition;

    public static MapFragmentWrapper getInstance() {
        if (singletonInstance == null) {
            singletonInstance = new MapFragmentWrapper();
        }

        return singletonInstance;
    }

    public static MapFragmentWrapper newInstance(LocationChangedListener listener) {
        return MapFragmentWrapper.newInstance(listener, new MapOptions());
    }

    public static MapFragmentWrapper newInstance(MapOptions options) {
        return MapFragmentWrapper.newInstance(new LocationChangedListener() {
            @Override
            public void onUpdate(LatLng point) {
                // left blank
            }
        }, options);
    }

    public static MapFragmentWrapper newInstance(LocationChangedListener listener, MapOptions options) {
        MapFragmentWrapper fragment = new MapFragmentWrapper();
        fragment.setLocationChangedListener(listener);
        fragment.setMapOptions(options);

        return fragment;
    }

    //-----------------------------------------------------------------------------------------------
    // Constructors

    public MapFragmentWrapper() {
        super();

        this.mapOptions = new MapOptions();
        this.markers = new ArrayList<>();
        this.listener = new LocationChangedListener() {

            @Override
            public void onUpdate(LatLng point) {

            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();

        // check that Google Play Services is available before initializing the map
        // or getting the location of the user.
        if (isServicesConnected()) {
            setUpLocationClientIfNeeded();
            mGoogleApiClient.connect();
        }

        getMapAsync(this);

        if (this.googleMap != null) {
            moveToCurrentLocation();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (this.googleMap != null) {
            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    // left blank
                }
            });

            this.oldCameraPosition = googleMap.getCameraPosition();
        }

        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    //------------------------------------------------------------------------------------------------
    // SETTERS METHODS
    //------------------------------------------------------------------------------------------------

    public void setMapOptions(MapOptions mapOptions) {
        this.mapOptions = mapOptions;
    }


    //------------------------------------------------------------------------------------------------
    // HELPER METHODS
    //------------------------------------------------------------------------------------------------

    private void setUpLocationClientIfNeeded() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(MainApplication.getAppContext())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
    }

    private void replaceMarker(LatLng point) {
        // remove all the markers on the map
        for (Marker m : markers)
            m.remove();

        // empty the list of markers
        markers.clear();

        this.addMarker((float) point.latitude, (float) point.longitude);
    }

    public void clearAllMarkers() {
        // remove all the markers on the map
        for (Marker m : markers)
            m.remove();

        // empty the list of markers
        markers.clear();
    }

    private void moveToLatLng(LatLng latLng) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 9), MAP_ANIMATION_DURATION_MS, null);
    }

    private boolean isServicesConnected() {
        boolean result = false;

        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.getActivity());

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            result = true;
        } // Google Play services was not available for some reason
        else {

            // Get the error dialog from Google Play services
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this.getActivity(), PLAY_SERVICES_CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment = new ErrorDialogFragment();
                // Set the dialog in the DialogFragment
                errorFragment.setDialog(errorDialog);
                // Show the error dialog in the DialogFragment
                errorFragment.show(this.getActivity().getSupportFragmentManager(), "Location Updates");
            }
        }

        return result;
    }

    private void moveToCurrentLocation() {
        if (this.markers.size() == 0) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_LOCATION, 9));
        }

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } else {
            googleMap.setMyLocationEnabled(true);

        }

        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                if (mapOptions.isAddMarkerOnTouch()) {
                    replaceMarker(point);

                    // move the camera to the marker
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(point), 500, null);

                    // call the update listener
                    listener.onUpdate(point);
                }
            }
        });

        googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                Location location = googleMap.getMyLocation();

                if (mapOptions.isMarkerOnUserLocation()) {
                    updatedMarkerPosition((float) location.getLatitude(), (float) location.getLongitude());
                } else {
                    moveToLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                }

                return true;
            }
        });

        if (this.oldCameraPosition != null) {
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(this.oldCameraPosition));
        }
    }

    //------------------------------------------------------------------------------------------------
    // MapFragmentAddOns METHODS
    //------------------------------------------------------------------------------------------------

    public void setLocationChangedListener(LocationChangedListener listener) {
        this.listener = listener;
    }

    public void updatedMarkerPosition(float latitude, float longitude) {
        updatedMarkerPosition(new LatLng(latitude, longitude));
    }

    public void updatedMarkerPosition(LatLng latLng) {
        replaceMarker(latLng);
        moveToLatLng(latLng);
    }

    @Override
    public void addMarker(Marker marker) {
        this.addMarker(marker, null, 0);
    }

    @Override
    public void addMarker(Marker marker, String title, int iconId) {

        if (title != null) marker.setTitle("");
        if (iconId != 0) marker.setIcon(BitmapDescriptorFactory.fromResource(iconId));

        if (this.mapOptions.hasMarkerLimit() && markers.size() == this.mapOptions.getMarkerLimit()) {
            markers.remove(markers.get(0));
        }

        markers.add(marker);
        moveToLatLng(marker.getPosition());
    }

    @Override
    public void addMarker(float latitude, float longitude) {
        addMarker(latitude, longitude, null, 0);
    }

    @Override
    public void addMarker(float latitude, float longitude, String title, int iconId) {
        if (this.googleMap == null) {
            return;
        }

        Marker marker = this.googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)));
        this.addMarker(marker, title, iconId);
    }

    @Override
    public void addPolygon(Polygon polygon, Polygon options) {
        // TODO when the API starts sending polygons
    }

    @Override
    public void fitAllMarkers() {
        if (markers.size() == 0)
            return;

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        // add each markers position to the bounds builder
        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }

        // create the LatLng bounds for the map
        LatLngBounds bounds = builder.build();

        // get the padding for the markers
        int padding = (int) getResources().getDimension(R.dimen.capture_detail_map_padding); // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

        // move the map to fit all the markers
        this.googleMap.moveCamera(cu);

        // set the minimum zoom level for the markers, if there is only one marker
        if (this.markers.size() == 1) {
            this.googleMap.moveCamera(CameraUpdateFactory.zoomTo(8));
        }
    }

    @Override
    public void moveMapTo(double latitude, double longitude) {
        this.moveToLatLng(new LatLng(latitude, longitude));
    }

    //------------------------------------------------------------------------------------------------
    // GoogleApiClient.ConnectionCallbacks
    //------------------------------------------------------------------------------------------------

    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // Update location every second

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    //------------------------------------------------------------------------------------------------
    // GoogleApiClient.OnConnectionFailedListener
    //------------------------------------------------------------------------------------------------


    @Override
    public void onConnectionSuspended(int i) {
        Log.i(Constants.LOG_TAG, "GoogleApiClient connection has been suspend");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(Constants.LOG_TAG, "GoogleApiClient connection has failed");
    }


    //------------------------------------------------------------------------------------------------
    // LOCATION LISTENER METHODS
    //------------------------------------------------------------------------------------------------

    @Override
    public void onLocationChanged(Location location) {
        if (mGoogleApiClient.isConnected()) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            listener.onUpdate(latLng);

            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    public void setMapLayer(int mapLayer) {
        this.googleMap.setMapType(mapLayer);
    }

    // ---------------------------------------------------------------------------------------------
    // OnMapReadyCallback METHODS
    // ---------------------------------------------------------------------------------------------

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        moveToCurrentLocation();
    }

    //------------------------------------------------------------------------------------------------
    // INTERNAL CLASSES
    //------------------------------------------------------------------------------------------------


    // Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog mDialog;

        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }
}