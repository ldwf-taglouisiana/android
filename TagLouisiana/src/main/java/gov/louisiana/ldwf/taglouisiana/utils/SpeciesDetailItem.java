package gov.louisiana.ldwf.taglouisiana.utils;

public class SpeciesDetailItem {

  private String header;
  private String body;

  public SpeciesDetailItem(String header, String body) {
    this.header = header;
    this.body = body;
  }

  public String getHeader() {
    return header;
  }

  public String getBody() {
    return body;
  }
}