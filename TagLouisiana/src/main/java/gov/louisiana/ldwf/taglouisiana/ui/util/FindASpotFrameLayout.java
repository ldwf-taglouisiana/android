package gov.louisiana.ldwf.taglouisiana.ui.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by danielward on 8/9/13.
 */
public class FindASpotFrameLayout extends FrameLayout {
  private Context myContext;

  public FindASpotFrameLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
    myContext = context;
  }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
//    {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec+((int)myContext.getResources().getDimension(R.dimen.quickplay_offset)));
//    }
}
