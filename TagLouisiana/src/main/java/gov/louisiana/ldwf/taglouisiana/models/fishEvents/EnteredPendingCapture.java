package gov.louisiana.ldwf.taglouisiana.models.fishEvents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;

import java.util.List;
import java.util.UUID;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.database.UUIDConverter;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.images.AttachedPhoto;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.images.AttachedPhoto_Table;
import gov.louisiana.ldwf.taglouisiana.models.options.RecaptureDispositionOption;

/**
 * Created by danielward on 8/19/16.
 */
@Table(database = AppDatabase.class)
public class EnteredPendingCapture extends AbstractFishEvent {
    @Expose
    @SerializedName("recapture")
    @Column
    public boolean isRecapture;

    @Expose
    @Column
    @SerializedName("should_save")
    public boolean shouldSave;

    @Column
    @ForeignKey(tableClass = PendingCapture.class)
    public long pendingCaptureId;

    @Expose
    @SerializedName("recapture_disposition_id")
    @Column
    @ForeignKey(tableClass = RecaptureDispositionOption.class)
    public long recaptureDispositionOptionId;

    @Expose
    @SerializedName("uuid")
    @Column(typeConverter = UUIDConverter.class)
    @Unique
    public UUID uuid;

    @Expose
    @SerializedName("images")
    List<AttachedPhoto> images;

    private PendingCapture pendingCapture;

    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "images")
    public List<AttachedPhoto> getImages() {
        if (images == null || images.isEmpty()) {
            images = SQLite.select()
                    .from(AttachedPhoto.class)
                    .where(AttachedPhoto_Table.enteredPendingCaptureUuid.eq(this.uuid))
                    .queryList();
        }
        return images;
    }

    public PendingCapture getPendingCapture() {
        if (this.pendingCapture == null) {
            this.pendingCapture = SQLite.select().from(PendingCapture.class).where(PendingCapture_Table._id.eq(this.pendingCaptureId)).querySingle();
        }

        return pendingCapture;
    }

    @Override
    public void save() {
        this.setDefaults();
        super.save();
    }

    @Override
    public void save(DatabaseWrapper databaseWrapper) {
        this.setDefaults();
        super.save(databaseWrapper);
    }

    private void setDefaults() {
        // if the UUID isn't set yet, then create a new rrandom value
        if (this.uuid == null || "".equals(this.uuid.toString())) {
            this.uuid = UUID.randomUUID();
        }

        // tell the API to NOT save a draft entry
        this.shouldSave = true;

        //        // set the uuid in the attached images if they are all blank still
//        for (AttachedPhoto photo : attachedImages) {
//            if (photo.enteredPendingCaptureUuid == null || "".equals(photo.enteredPendingCaptureUuid)) {
//                photo.enteredPendingCaptureUuid = this.uuid;
//        photo.save();
//            }
//        }
    }
}


