package gov.louisiana.ldwf.taglouisiana.ui.core;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;

import com.raizlabs.android.dbflow.list.FlowQueryList;
import com.raizlabs.android.dbflow.runtime.FlowContentObserver;
import com.raizlabs.android.dbflow.sql.queriable.ModelQueriable;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.raizlabs.android.dbflow.structure.Model;

/**
 * Created by danielward on 10/5/16.
 */

public abstract class BaseRefreshableQueryListFragment<T extends Model> extends BaseRefreshableFragment {

    protected FlowQueryList<T> queryList;
    private FlowContentObserver observer;
    private FlowContentObserver.OnTableChangedListener tableChangeListener;

    public BaseRefreshableQueryListFragment() {
        buildQueryList();
        observer = new FlowContentObserver();
    }

    protected abstract Class<T> getQueryableClass();

    protected abstract ModelQueriable<T> buildQuery(@Nullable String queryString);
    protected abstract BaseAdapter getNewAdapter();

    protected void buildQueryList() {
        buildQueryList(null);
    }

    protected void buildQueryList(@Nullable String queryString) {
        queryList = new FlowQueryList.Builder<>(getQueryableClass())
                .modelQueriable(buildQuery(queryString))
                .build();
    }

    protected int getEmptyViewId(){
        return android.R.id.empty;
    }

    protected int getListViewId(){
        return android.R.id.list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        buildQueryList();

        observer = new FlowContentObserver();

        tableChangeListener = new FlowContentObserver.OnTableChangedListener() {
            @Override
            public void onTableChanged(@Nullable Class<?> tableChanged, BaseModel.Action action) {
                if (tableChanged == getQueryableClass()) {
                    onRefresh();
                }
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();

        setAdapter(getNewAdapter());
    }

    @Override
    public void onResume() {
        super.onResume();
//        observer.addOnTableChangedListener(tableChangeListener);
//        observer.registerForContentChanges(getActivity(), getQueryableClass());
    }

    @Override
    public void onPause() {
        super.onPause();
//        observer.removeTableChangedListener(tableChangeListener);
//        observer.unregisterForContentChanges(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        try {
            View absListView = view.findViewById(getListViewId());
            View emptyView = view.findViewById(getEmptyViewId());

            if ((absListView != null && absListView instanceof AbsListView) && emptyView != null) {
                ((AbsListView) absListView).setEmptyView(emptyView);
            }
        } catch (NullPointerException e) {
            // the views could not be fetched with the current videw structure
        }

        return view;
    }

    @Override
    protected void setAdapterView(AbsListView view) {
        super.setAdapterView(view);
    }

    @Override
    protected void onRefreshStarted() {
        Bundle bundle = new Bundle();
        this.getBaseActivity().requestSync(bundle);
    }

    @Override
    protected void onRefreshFinished() {
        super.onRefreshFinished();
        buildQueryList();
        getAdapter().notifyDataSetChanged();
    }
}
