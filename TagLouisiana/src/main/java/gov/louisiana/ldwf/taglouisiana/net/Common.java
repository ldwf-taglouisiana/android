package gov.louisiana.ldwf.taglouisiana.net;


import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import gov.louisiana.ldwf.taglouisiana.gson.GsonUtil;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/29/14
 */
public class Common {
    public static final String GMT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String GMT_FORMAT_WITH_TZ = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    public static final DateFormat UTC_OUTPUT_DATE_FORMATTER = new GMTDateFormatter(GMT_FORMAT);
    public static final DateFormat UTC_DATE_FORMATTER = new GMTDateInputFormatter();

    public static GenericResponse parseJsonResponse(String json) {
        return GsonUtil.gson().fromJson(json, GenericResponse.class);
    }

    private static class GMTDateFormatter extends SimpleDateFormat {
        public GMTDateFormatter(String format) {
            super(format);
            this.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
    }

    private static class GMTDateInputFormatter extends SimpleDateFormat {
        @Override
        public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition fieldPosition) {
            return formatter.format(date, toAppendTo, fieldPosition);
        }

        static final DateFormat formatter = new GMTDateFormatter(GMT_FORMAT);

        @Override
        public Date parse(String source, ParsePosition pos) {
      /*
       * Compare the source string with the format string to determine which format to use
       * We -2 the length of the format string to remove the 2 escape ' from the format string
       */
            if (source.length() - pos.getIndex() == (GMT_FORMAT.length() - 2)) {
                return formatter.parse(source, pos);
            }

            return formatterTZ.parse(source, pos);
        }

        static final DateFormat formatterTZ = new GMTDateFormatter(GMT_FORMAT_WITH_TZ);
    }
}
