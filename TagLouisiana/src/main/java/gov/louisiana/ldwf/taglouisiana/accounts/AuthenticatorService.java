package gov.louisiana.ldwf.taglouisiana.accounts;

/**
 * Created by daniel on 8/28/13.
 */

import android.accounts.Account;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * A bound Service that instantiates the authenticator
 * when started.
 */
public class AuthenticatorService extends Service {

  public static String DEFAULT_ACCOUNT_NAME = "Tag Louisiana Data Sync";

  public AuthenticatorService() {

  }

  @Override
  public void onCreate() {
    // Create a new authenticator object
  }

  /*
   * When the system binds to this Service to make the RPC call
   * return the authenticator's IBinder.
   */
  @Override
  public IBinder onBind(Intent intent) {
    Authenticator mAuthenticator = new Authenticator(this);
    return mAuthenticator.getIBinder();
  }

  /**
   * Obtain a handle to the {@link android.accounts.Account} used for sync in this application.
   *
   * @return Handle to application's account (not guaranteed to resolve unless CreateSyncAccount()
   * has been called)
   */
  public static Account GetAccount() {
    // Note: Normally the account name is set to the user's identity (username or email
    // address). However, since we aren't actually using any user accounts, it makes more sense
    // to use a generic string in this case.
    //
    // This string should *not* be localized. If the user switches locale, we would not be
    // able to locate the old account, and may erroneously register multiple accounts.
    final String accountName = DEFAULT_ACCOUNT_NAME;
    return new Account(accountName, AccountGeneral.ACCOUNT_TYPE);
  }
}
