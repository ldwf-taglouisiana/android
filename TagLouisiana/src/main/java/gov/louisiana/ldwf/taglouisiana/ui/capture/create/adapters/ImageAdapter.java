package gov.louisiana.ldwf.taglouisiana.ui.capture.create.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;

import java.util.SortedSet;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.util.TagImage;

/**
 * Created by danielward on 11/8/16.
 */
public class ImageAdapter extends BaseAdapter {
    private Context context;
    private AQuery aQuery;

    private SortedSet<TagImage> imageSet;

    public ImageAdapter(Context context, SortedSet<TagImage> imageSet) {
        this.context = context;
        this.aQuery = new AQuery(this.context);
        this.imageSet = imageSet;
    }

    public void updateData(SortedSet<TagImage> imageSet) {
        this.imageSet = imageSet;
        notifyDataSetChanged();
    }

    public int getCount() {
        return imageSet.size();
    }

    public TagImage getItem(int position) {
        return nthElement(imageSet, position);
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = aQuery.inflate(convertView, R.layout.enter_capture_page_add_image_item, parent);
        aQuery.recycle(convertView);

        final TagImage item = getItem(position);

        aQuery.id(R.id.tag_number).text(item.getTagNumber());
        aQuery.id(R.id.tag_image).image(item.getImage());

        return convertView;
    }

    // http://stackoverflow.com/a/24467730/2614663
    final TagImage nthElement(Iterable<TagImage> data, int n) {
        int index = 0;
        for (TagImage element : data) {
            if (index == n) {
                return element;
            }
            index++;
        }
        return null;
    }
}
