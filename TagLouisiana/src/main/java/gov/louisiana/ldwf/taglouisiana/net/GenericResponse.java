package gov.louisiana.ldwf.taglouisiana.net;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/29/14
 */
public class GenericResponse implements Parcelable {

    private ArrayList<String> errors;
    private ArrayList<String> messages;

    public GenericResponse() {
        errors = new ArrayList<String>();
        messages = new ArrayList<String>();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeStringList(errors);
        parcel.writeStringList(messages);
    }

    public static final Parcelable.Creator<GenericResponse> CREATOR
            = new Parcelable.Creator<GenericResponse>() {
        public GenericResponse createFromParcel(Parcel in) {
            return new GenericResponse(in);
        }

        public GenericResponse[] newArray(int size) {
            return new GenericResponse[size];
        }
    };

    private GenericResponse(Parcel in) {
        in.readStringList(errors);
        in.readStringList(messages);
    }
}
