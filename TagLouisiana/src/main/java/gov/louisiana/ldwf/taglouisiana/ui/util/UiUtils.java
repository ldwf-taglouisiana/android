package gov.louisiana.ldwf.taglouisiana.ui.util;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.joanzapata.iconify.Icon;
import com.joanzapata.iconify.IconDrawable;

import gov.louisiana.ldwf.taglouisiana.R;

/**
 * Created by danielward on 11/1/16.
 */

public class UiUtils {
    public static Drawable FontAwesomeItemDrawable(Context context, Icon iconValue) {
        return FontAwesomeItemDrawable(context, iconValue, R.color.white, 15);
    }

    public static Drawable FontAwesomeItemDrawable(Context context, Icon iconValue, int colorRes, int dpSize) {
        return new IconDrawable(context, iconValue).colorRes(colorRes).sizeDp(dpSize);
    }
}
