package gov.louisiana.ldwf.taglouisiana.models.species;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.Index;
import com.raizlabs.android.dbflow.annotation.Table;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.RemoteItem;

/**
 * Created by danielward on 8/19/16.
 */
@Table(database = AppDatabase.class, primaryKeyConflict = ConflictAction.REPLACE)
public class SpeciesLength extends RemoteItem {
    @Expose
    @SerializedName("description")
    @Column
    public String description;

    @Expose
    @SerializedName("position")
    @Column
    public long position;

    @Index
    @Expose
    @SerializedName("species_id")
    @ForeignKey(tableClass = Species.class)
    public long speciesId;


    @ForeignKey(stubbedRelationship = true)
    public Species species;

}
