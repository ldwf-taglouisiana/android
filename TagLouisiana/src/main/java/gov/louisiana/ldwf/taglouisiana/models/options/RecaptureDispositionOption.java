package gov.louisiana.ldwf.taglouisiana.models.options;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Table;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.RemoteItem;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/28/14
 */
@Table(database = AppDatabase.class)
public class RecaptureDispositionOption extends RemoteItem {

    @Expose
    @SerializedName("recapture_disposition")
    @Column
    public String description;

}
