package gov.louisiana.ldwf.taglouisiana.utils;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: danielward
 * Date: 3/14/13
 * Time: 3:17 PM
 * To change this template use File | Settings | File Templates.
 */

public class GeoCoord {
  double lat, lon;  // decimal degrees latitude and longitude
  double latd, latm, lond, lonm; // latitude degrees and minutes, longitude degrees and minutes
  double lats, lons;  // latitude and longitude seconds.

  public GeoCoord() {
    lat = 0;
    lon = 0;
    latd = 0;
    lond = 0;
    latm = 0;
    lonm = 0;
    lats = 0;
    lons = 0;
  }

  // construct from decimal degree input lat, lon
  public GeoCoord(double x, double y) {
    lat = x;
    lon = y;
    DecToDMS();
  }

  // construct from degrees minutes sec input (lat, lon)
  public GeoCoord(int a, int b, double c, int d, int e, double f) {
    latd = a;
    latm = b;
    lats = c;
    lond = d;
    lonm = e;
    lons = f;
    DMSToDec();
  }

  // construct from degrees minutes sec input (lat, lon)
  public GeoCoord(int a, double b, int c, double d) {
    latd = a;
    latm = (int) b;
    lats = (b - latm) * 60.0;
    lond = c;
    lonm = (int) d;
    lons = (d - lonm) * 60.0;
    DMSToDec();
  }

  // construct from degrees minutes sec input (lat, lon)
  public GeoCoord(double a, double b, double c, double d, double e, double f) {
    latd = a;
    latm = b;
    lats = c;
    lond = d;
    lonm = e;
    lons = f;
    DMSToDec();
  }

  // set coordinates in Decimal deg
  public void SetValue(double x, double y) {
    lat = x;
    lon = y;
    DecToDMS();
  }

  // set coordinates in DMS
  public void SetValue(int a, int b, double c, int d, int e, double f) {
    latd = a;
    latm = b;
    lats = c;
    lond = d;
    lonm = e;
    lons = f;
    DMSToDec();
  }

  public double lat() {
    return lat;
  }

  public double lon() {
    return lon;
  }

  public double latd() {
    return latd;
  }

  public double latm() {
    return latm;
  }

  public double lats() {
    return lats;
  }

  public double lond() {
    return lond;
  }

  public double lonm() {
    return lonm;
  }

  public double lons() {
    return lons;
  }


  void DecToDMS() {

    // change the decimal degrees to degrees minutes seconds
    double tempf;
    // first do the latitude
    tempf = java.lang.Math.abs(lat);
    latd = (int) tempf;
    tempf = (tempf - latd) * 60.0;
    latm = (int) tempf;
    lats = (tempf - latm) * 60.0;
    if (lat < 0) {    // put the sign back on the degrees
      latd = -latd;
    }

    // now do the longitude
    tempf = java.lang.Math.abs(lon);
    lond = (int) tempf;
    tempf = (tempf - lond) * 60.0;
    lonm = (int) tempf;
    lons = (tempf - lonm) * 60.0;
    if (lon < 0) {
      lond = -lond;
    }
  }


  void DecFloatToDMS() {

    // change the decimal degrees to degrees minutes seconds
    double tempf;
    // first do the latitude
    tempf = Math.abs(lat);
    latd = Math.floor(tempf);
    tempf = (tempf - latd) * 60.0;
    latm = Math.floor(tempf);
    lats = (tempf - latm) * 60.0;
    if (lat < 0) {    // put the sign back on the degrees
      latd = -latd;
    }

    // now do the longitude
    tempf = java.lang.Math.abs(lon);
    lond = Math.floor(tempf);
    tempf = (tempf - lond) * 60.0;
    lonm = Math.floor(tempf);
    lons = (tempf - lonm) * 60.0;
    if (lon < 0) {
      lond = -lond;
    }
  }

  void DMSToDec() {
    // change from degrees minutes seconds to decimal degrees

    lat = java.lang.Math.abs(latd) + latm / 60.0 + lats / 3600.0;
    lat = latd < 0 ? -lat : lat;
    lon = java.lang.Math.abs(lond) + lonm / 60.0 + lons / 3600.0;
    lon = lond < 0 ? -lon : lon;
  }

  public String[] getDMS() {
    String[] result = new String[6];
    result[0] = latd + "";
    result[1] = latm + "";
    result[2] = lats + "";
    result[3] = lond + "";
    result[4] = lonm + "";
    result[5] = lons + "";

    return result;
  }

  public String[] getDM() {
    Map<String, Object> temp = new TreeMap<>();

    String[] result = new String[4];
    result[0] = latd + "";
    result[1] = ((float) latm + (lats / 60.0)) + "";
    result[2] = lond + "";
    result[3] = ((float) lonm + (lons / 60.0)) + "";

    return result;
  }

  public String toString() {
    return (new String(lat + " " + lon));
  }

  public String toDMSstring() {
    String dstr, mstr = null, sstr = null;
    String sstr1 = null;
    String latstr = null;
    String lonstr = null;
    dstr = new String(Math.abs(latd) + "d");
    if (latm < 10) {
      mstr = new String("0" + latm + "'");
    } else {
      mstr = new String(latm + "'");
    }
    if (lats < 10) {
      sstr = new String("0" + lats);
    } else {
      sstr = new String(lats + "");
    }
    try {
      sstr1 = new String(sstr.substring(0, 3) + "''");
    } catch (StringIndexOutOfBoundsException e) {
      sstr1 = new String(sstr + "''");
    }
    if (lat < 0) {
      latstr = new String(dstr + mstr + sstr1 + "S");
    } else {
      latstr = new String(dstr + mstr + sstr1 + "N");
    }
    dstr = new String(Math.abs(lond) + "d");
    if (lonm < 10) {
      mstr = new String("0" + lonm + "'");
    } else {
      mstr = new String(lonm + "'");
    }
    if (lons < 10) {
      sstr = new String("0" + lons);
    } else {
      sstr = new String(lons + "");
    }
    try {
      sstr1 = new String(sstr.substring(0, 3) + "''");
    } catch (StringIndexOutOfBoundsException e) {
      sstr1 = new String(sstr + "''");
    }
    if (lon < 0) {
      lonstr = new String(dstr + mstr + sstr1 + "W");
    } else {
      lonstr = new String(dstr + mstr + sstr1 + "E");
    }

    return (new String(latstr + " " + lonstr + " : " + lat + " " + lon));
  }


}