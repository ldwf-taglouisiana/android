package gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.gson.responses.BaseResponse;
import gov.louisiana.ldwf.taglouisiana.models.tag.Tag;

/**
 * Created by danielward on 8/25/16.
 */
public class TagResponse extends BaseResponse {
    @Expose
    @SerializedName("items")
    public List<Tag> items;

    @Override
    public Class getModelClass() {
        return Tag.class;
    }

    public List<Tag> getItems() {
        if (items == null) {
            items = new ArrayList<>(0);
        }

        return items;
    }
}
