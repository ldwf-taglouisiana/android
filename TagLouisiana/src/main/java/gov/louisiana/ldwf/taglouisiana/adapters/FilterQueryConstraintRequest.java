package gov.louisiana.ldwf.taglouisiana.adapters;

import com.raizlabs.android.dbflow.list.FlowQueryList;

import gov.louisiana.ldwf.taglouisiana.models.BaseItem;

/**
 * Created by danielward on 10/26/16.
 */
public interface FilterQueryConstraintRequest<T extends BaseItem> {
    public FlowQueryList<T> applyConstraint(String constraint);
}
