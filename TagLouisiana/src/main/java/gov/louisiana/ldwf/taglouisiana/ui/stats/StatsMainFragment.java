package gov.louisiana.ldwf.taglouisiana.ui.stats;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;

/**
 *
 */
public class StatsMainFragment extends BaseFragment {

    public static final String IS_SIGNED_IN_ARG = "StatsMainFragment.arg.is_signed_in";

    private PagerAdapter pagerAdapter;
    List<BaseFragment> fragments;

    public StatsMainFragment() {
        // Required empty public constructor
        this.setHasTabs();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragments = new ArrayList<>(2);
        pagerAdapter = new PagerAdapter(this.getChildFragmentManager(), fragments);

        if (getArguments() != null && getArguments().getBoolean(IS_SIGNED_IN_ARG, false)) {
            fragments.add(StatsFragment.getUserInstance());
        }

        fragments.add(StatsFragment.getProgramInstance());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stats, container, false);

        ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);

//        PagerSlidingTabStrip strip = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
//        strip.setViewPager(pager);
//        strip.setTextColorResource(R.color.white);

        getBaseActivity().setTabViewPager(pager);
        return view;
    }

    @Override
    public String getTitle() {
        return "Tag Louisiana";
    }

    //----------------------------------------------------------------------------------------------
    // PAGER ADAPTER

    private class PagerAdapter extends FragmentStatePagerAdapter {

        private List<BaseFragment> fragments;

        public PagerAdapter(FragmentManager fm, List<BaseFragment> fragments) {
            super(fm);

            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragments.get(position).getTitle();
        }
    } // end pager adapter
}
