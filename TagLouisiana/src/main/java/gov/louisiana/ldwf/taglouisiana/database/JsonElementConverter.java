package gov.louisiana.ldwf.taglouisiana.database;


import com.google.gson.JsonElement;
import com.raizlabs.android.dbflow.converter.TypeConverter;

import gov.louisiana.ldwf.taglouisiana.gson.GsonUtil;

/**
 * Created by danielward on 8/19/16.
 */
@com.raizlabs.android.dbflow.annotation.TypeConverter
public class JsonElementConverter extends TypeConverter<String, JsonElement> {
    @Override
    public String getDBValue(JsonElement model) {
        return model == null ? null : model.toString();
    }

    @Override
    public JsonElement getModelValue(String data) {
        return data == null ? null : GsonUtil.gson().fromJson(data, JsonElement.class);
    }
}
