package gov.louisiana.ldwf.taglouisiana.database;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by danielward on 8/18/16.
 */
@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
public class AppDatabase {

    public static final String NAME = "taglouisiana_android"; // we will add the .db extension

    public static final int VERSION = 1;
}

