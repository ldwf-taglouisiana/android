package gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.gson.responses.BaseResponse;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture;

/**
 * Created by danielward on 8/25/16.
 */
public class CaptureResponse extends BaseResponse {
    @Expose
    @SerializedName("items")
    public List<Capture> items;

    public List<Capture> getItems() {
        if (items == null) {
            items = new ArrayList<>(0);
        }

        return items;
    }

    @Override
    public Class getModelClass() {
        return Capture.class;
    }
}
