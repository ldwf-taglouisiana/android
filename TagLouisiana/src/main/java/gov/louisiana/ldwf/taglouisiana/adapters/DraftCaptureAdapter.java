package gov.louisiana.ldwf.taglouisiana.adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.androidquery.AQuery;

import java.util.List;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.AbstractFishEvent;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.EnteredPendingCapture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.PendingCapture;
import gov.louisiana.ldwf.taglouisiana.ui.util.MaterialDialogBuilder;
import gov.louisiana.ldwf.taglouisiana.ui.util.UiUtils;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

/**
 * Created by danielward on 10/5/16.
 *
 */
public class DraftCaptureAdapter<T extends AbstractFishEvent> extends ArrayAdapter<T> {

    private int resource;
    private AQuery aQuery;

    public DraftCaptureAdapter(Context context, List<T> users, int resource) {
        super(context, 0, users);

        this.aQuery = new AQuery(context);
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = aQuery.inflate(convertView, resource, parent);
        aQuery.recycle(convertView);

        final T draft = getItem(position);

        aQuery.id(R.id.tag_number).text(draft.tagNumber);
        if(draft.getSpecies() != null) {
            aQuery.id(R.id.species).text(draft.getSpecies().commonName); //todo need to fix the species properties
        }
        aQuery.id(R.id.date).text(Constants.US_DATE_FORMATTER.format(draft.date));


        if (draft instanceof PendingCapture) {
            final PendingCapture pendingCapture = (PendingCapture) draft;
            aQuery.id(R.id.status_container).backgroundColorId(pendingCapture.statusColorResId());
            aQuery.id(R.id.status_image).image(UiUtils.FontAwesomeItemDrawable(getContext(), pendingCapture.statusIcon()));

            aQuery.id(R.id.recaptured).text("Recapture").visibility(pendingCapture.isRecapture ? View.VISIBLE : View.GONE);
            aQuery.id(R.id.has_images).visibility(pendingCapture.getImages().size() > 0 ? View.VISIBLE : View.GONE);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String title = pendingCapture.hasErrors() ? "Errors for " + draft.tagNumber : "Status";

                    MaterialDialogBuilder builder = new MaterialDialogBuilder(view.getContext());
                    builder.setTitle(title);
                    builder.setContent(pendingCapture.statusMessage());
                    builder.setPositiveButton("DISMISS", new MaterialDialogBuilder.OnMaterialDialogItemClickListener() {
                        @Override
                        public void onClick(Dialog dialog) {
                            dialog.dismiss();
                        }
                    });

                    builder.create().show();
                }
            });

        } else if (draft instanceof EnteredPendingCapture) {
            final EnteredPendingCapture enteredPendingCapture = (EnteredPendingCapture) draft;

            aQuery.id(R.id.status_container).gone();
            aQuery.id(R.id.recaptured).text("Recapture").visibility(enteredPendingCapture.isRecapture ? View.VISIBLE : View.GONE);
            aQuery.id(R.id.has_images).visibility(enteredPendingCapture.getImages().size() > 0 ? View.VISIBLE : View.GONE);
        }

        return convertView;
    }
}
