package gov.louisiana.ldwf.taglouisiana.models.fishEvents;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.images.CapturePhoto;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.images.CapturePhoto_Table;

/**
 * Created by danielward on 8/17/16.
 */
@Table(database = AppDatabase.class)
public class Capture extends AbstractFishEvent {

    @Expose
    @Column
    @SerializedName("recapture_count")
    public long recaptureCount;

    @Expose
    @SerializedName("images")
    public List<CapturePhoto> images;

    @Expose
    @SerializedName("geo_json")
    public JsonObject geoJson;

    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "images")
    public List<CapturePhoto> getImages() {
        if (images == null || images.isEmpty()) {
            images = SQLite.select()
                    .from(CapturePhoto.class)
                    .where(CapturePhoto_Table.fishEventId.eq(this._id))
                    .queryList();
        }
        return images;
    }

    public boolean hasRecaptures() {
        return recaptureCount > 0;
    }
}
