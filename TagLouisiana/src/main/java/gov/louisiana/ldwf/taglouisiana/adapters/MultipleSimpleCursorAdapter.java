package gov.louisiana.ldwf.taglouisiana.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.util.LongSparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.SimpleCursorAdapter;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by danielward on 11/10/16.
 * http://stackoverflow.com/a/12710411/2614663
 */
public class MultipleSimpleCursorAdapter extends SimpleCursorAdapter {

    private LongSparseArray<Boolean> selectedItems;


    public MultipleSimpleCursorAdapter(Context context, Cursor cursor, String[] from, Set<Long> selectedIds) {
        this(context, cursor, from, 0, selectedIds);
    }

    public MultipleSimpleCursorAdapter(Context context, Cursor cursor, String[] from, int flags, Set<Long> selectedIds) {
        super(context, android.R.layout.simple_list_item_multiple_choice, cursor, from, new int[]{android.R.id.text1}, flags);
        selectedItems = new LongSparseArray<>();

        for (Long item : selectedIds) {
            selectedItems.put(item, true);
        }
    }

    /**
     * Reuses old views if they have not already been reset and inflates new
     * views for the rows in the list that needs a new one. It the adds a
     * listener to each checkbox that is used to store information about which
     * checkboxes have been checked or not. Finally we set the checked status of
     * the checkbox and let the super class do it's thing.
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);

        CheckedTextView checkedTextView = (CheckedTextView) view.findViewById(android.R.id.text1);
        checkedTextView.setChecked(isItemChecked(position));

        checkedTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckedTextView selectedView = (CheckedTextView) view;
                selectedView.setChecked(!selectedView.isChecked());
                selectedItems.put(getItemId(position), selectedView.isChecked());
            }
        });

        return view;
    }

    public boolean isItemChecked(int position) {
        return selectedItems.get(getItemId(position), false);
    }

    /**
     * Returns an array list with all the selected items as Track objects.
     *
     * @return the selected items
     */
    public Set<Long> getSelectedItems() {
        Set<Long> result = new HashSet<>();

        for (int i = 0; i < selectedItems.size(); i++) {
            long key = selectedItems.keyAt(i);
            if (selectedItems.get(key)) {
                result.add(key);
            }
        }
        return result;
    }
}