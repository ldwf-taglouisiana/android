package gov.louisiana.ldwf.taglouisiana.models.fishEvents.images;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Table;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;

/**
 * Created by danielward on 8/23/16.
 */
@Table(database = AppDatabase.class)
public class CapturePhoto extends AbstractFishEventPhoto {
    @Expose
    @Column
    @SerializedName("parent_id")
    public long fishEventId;
}
