package gov.louisiana.ldwf.taglouisiana.gson.typeAdapters;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import gov.louisiana.ldwf.taglouisiana.models.BaseItem;
import gov.louisiana.ldwf.taglouisiana.models.annotations.IgnoreSerializedEpoch;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.EnteredPendingCapture;

/**
 * Created by danielward on 10/28/16.
 */

public class BaseItemTypeAdapterFactory extends BeforeAfterTypeAdapterFactory<BaseItem> {

    private final static String EPOCH_SUBSTRING = "_epoch";

    public BaseItemTypeAdapterFactory() {
        super(BaseItem.class);
    }

    @Override
    protected void beforeWrite(BaseItem source, JsonElement toSerialize) {

        // if the class does NOT have the IgnoreSerializedEpoch then we need to prune
        if (source.getClass().getAnnotation(IgnoreSerializedEpoch.class) == null) {
            pruneEpochFields(toSerialize);
        }

        pruneEmptyArrayFields(toSerialize);
        pruneForeignKeyFields(toSerialize);

        adjustEnteredPendingCapture(source, toSerialize);
    }

    private void adjustEnteredPendingCapture(BaseItem item, JsonElement jsonElement) {
        if (item instanceof EnteredPendingCapture) {
            EnteredPendingCapture source = (EnteredPendingCapture) item;

            if (source.getPendingCapture() == null) {
                jsonElement.getAsJsonObject().add("id", new JsonPrimitive(-1));
            }
        }
    }

    /**
     * Iterates through all the keys in a {@link JsonElement}, recursively, and finds all keys
     * that end in <tt>{@value EPOCH_SUBSTRING}</tt> and changes modifies the key names to exclude
     * the <tt>{@value EPOCH_SUBSTRING}</tt> substring.
     *
     * @param jsonElement The {@link JsonElement} to search for the <tt>{@value EPOCH_SUBSTRING}</tt> keys and modify the keyname
     */
    private void pruneEpochFields(JsonElement jsonElement) {
        // Only work on JsonObjects
        if (jsonElement.isJsonObject()) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();

            // A temp holding set for the discovered keys
            Set<Map.Entry<String, JsonElement>> additions = new LinkedHashSet<>();

            /*
                Find all the keys that have the _epoch substring
             */
            for( Map.Entry<String, JsonElement> item :jsonObject.entrySet()) {
                if (item.getValue().isJsonObject()) {
                    pruneEpochFields(item.getValue());

                } else if (item.getKey().contains(EPOCH_SUBSTRING)) {
                    additions.add(item);

                } else if (item.getValue().isJsonArray()) {
                    for (JsonElement arrayElement: item.getValue().getAsJsonArray()) {
                        if (arrayElement.isJsonObject()) {
                            pruneEpochFields(arrayElement);
                        }
                    }
                }
            } // end for

            /*
                For all the found keys, replace the key with a modified name without the _epoch value
             */
            for (Map.Entry<String, JsonElement> item : additions) {
                jsonObject.remove(item.getKey());
                jsonObject.add(item.getKey().replace(EPOCH_SUBSTRING,""), item.getValue());
            }
        } // end if
    }

    /**
     * Iterates through all the keys in a {@link JsonElement}, recursively, and finds all keys
     * that end in <tt>{@value EPOCH_SUBSTRING}</tt> and changes modifies the key names to exclude
     * the <tt>{@value EPOCH_SUBSTRING}</tt> substring.
     *
     * @param jsonElement The {@link JsonElement} to search for keys with empty arrays
     */
    private void pruneEmptyArrayFields(JsonElement jsonElement) {
        // Only work on JsonObjects
        if (jsonElement.isJsonObject()) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();

            // A temp holding set for the discovered keys
            Set<Map.Entry<String, JsonElement>> removals = new LinkedHashSet<>();

            /*
                Find all the keys have array values and are empty
             */
            for( Map.Entry<String, JsonElement> item :jsonObject.entrySet()) {
                if (item.getValue().isJsonObject()) {
                    pruneEmptyArrayFields(item.getValue());

                } else if (item.getValue().isJsonArray() && item.getValue().getAsJsonArray().size() == 0) {
                    removals.add(item);

                } else if (item.getValue().isJsonArray()) {
                    for (JsonElement arrayElement: item.getValue().getAsJsonArray()) {
                        if (arrayElement.isJsonObject()) {
                            pruneEmptyArrayFields(arrayElement);
                        }
                    }
                }
            } // end for

            /*
                For all the found keys with empty arrays, remove the key
             */
            for (Map.Entry<String, JsonElement> item : removals) {
                jsonObject.remove(item.getKey());
            }
        } // end if
    }

    /**
     * Iterates through all the keys in a {@link JsonElement}, recursively, and finds all keys
     * that end in <tt>{@value EPOCH_SUBSTRING}</tt> and changes modifies the key names to exclude
     * the <tt>{@value EPOCH_SUBSTRING}</tt> substring.
     *
     * @param jsonElement The {@link JsonElement} to search for keys with empty arrays
     */
    private void pruneForeignKeyFields(JsonElement jsonElement) {
        // Only work on JsonObjects
        if (jsonElement.isJsonObject()) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();

            // A temp holding set for the discovered keys
            Set<Map.Entry<String, JsonElement>> removals = new LinkedHashSet<>();

            /*
                Find all the keys have array values and are empty
             */
            for( Map.Entry<String, JsonElement> item :jsonObject.entrySet()) {
                if (item.getValue().isJsonObject()) {
                    pruneForeignKeyFields(item.getValue());

                } else if (
                        item.getKey().endsWith("_id")
                        && item.getValue().isJsonPrimitive()
                        && item.getValue().getAsJsonPrimitive().isNumber()
                        && item.getValue().getAsJsonPrimitive().getAsNumber().longValue() == 0) {

                    removals.add(item);

                } else if (item.getValue().isJsonArray()) {
                    for (JsonElement arrayElement: item.getValue().getAsJsonArray()) {
                        if (arrayElement.isJsonObject()) {
                            pruneForeignKeyFields(arrayElement);
                        }
                    }
                }
            } // end for

            /*
                For all the found keys with empty arrays, remove the key
             */
            for (Map.Entry<String, JsonElement> item : removals) {
                jsonObject.remove(item.getKey());
            }
        } // end if
    }
}
