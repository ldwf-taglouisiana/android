package gov.louisiana.ldwf.taglouisiana.ui.draftCapture;

import android.app.Fragment;

import gov.louisiana.ldwf.taglouisiana.models.fishEvents.PendingCapture;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PendingCaptureListFragment#newInstance} factory method to
 */
public class PendingCaptureListFragment extends PendingCaptureBaseListFragment<PendingCapture> {

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DraftCaptureListFragment.
     */
    public static PendingCaptureListFragment newInstance() {
        return new PendingCaptureListFragment();
    }

    public PendingCaptureListFragment() {
        // Required empty public constructor
    }

    //----------------------------------------------------------------------------------------------
    // Overrides from BaseRefreshableQueryListFragment
    //----------------------------------------------------------------------------------------------


    @Override
    protected Class<PendingCapture> getQueryableClass() {
        return PendingCapture.class;
    }

    //----------------------------------------------------------------------------------------------
    // Overrides from BaseFragment
    //----------------------------------------------------------------------------------------------

    @Override
    public String getTitle() {
        return "Submitted";
    }

}
