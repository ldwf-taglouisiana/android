package gov.louisiana.ldwf.taglouisiana.gson.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;
import java.util.List;

/**
 * Created by danielward on 8/25/16.
 */
public abstract class BaseResponse {
    @Expose
    @SerializedName("updated_at_epoch")
    public Date version;

    @Expose
    @SerializedName("hash")
    public String hash;

    @Expose
    @SerializedName("id_hash")
    public String idHash;

    @Expose
    @SerializedName("count")
    public int count;

    @Expose
    @SerializedName("errors")
    public List<String> errors;

    public abstract Class getModelClass();

    public abstract <T extends BaseModel> List<T> getItems();
}
