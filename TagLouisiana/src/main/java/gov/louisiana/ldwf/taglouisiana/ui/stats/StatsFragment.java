package gov.louisiana.ldwf.taglouisiana.ui.stats;

import android.accounts.NetworkErrorException;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.androidquery.AQuery;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.net.ApiServiceGenerator;
import gov.louisiana.ldwf.taglouisiana.net.ProtectedApiService;
import gov.louisiana.ldwf.taglouisiana.net.PublicApiService;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseRefreshableFragment;
import gov.louisiana.ldwf.taglouisiana.ui.util.stats.StatsItem;
import gov.louisiana.ldwf.taglouisiana.ui.util.stats.StatsParser;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */
public class StatsFragment extends BaseRefreshableFragment {

    public static final String SHOW_PROGRAM_STATS = "show_program_stats";
    public static final String PREFS_CACHED_STATS = "StatsFragment.prefs.cahched_stats";

    private boolean showProgramStats;
    private ListView listView;

    private Handler mHandler;

    //----------------------------------------------------------------------------------------------
    // FACTORIES

    public static StatsFragment getUserInstance() {
        StatsFragment fragment = new StatsFragment();

        Bundle args = new Bundle();
        args.putBoolean(SHOW_PROGRAM_STATS, false);

        fragment.setShowProgramStats(false);
        fragment.setArguments(args);

        return fragment;
    }

    public static StatsFragment getProgramInstance() {
        StatsFragment fragment = new StatsFragment();

        Bundle args = new Bundle();
        args.putBoolean(SHOW_PROGRAM_STATS, true);

        fragment.setShowProgramStats(true);
        fragment.setArguments(args);

        return fragment;
    }

    //----------------------------------------------------------------------------------------------

    public StatsFragment() {
        // Required empty public constructor
    }

    //----------------------------------------------------------------------------------------------
    // Fragment OVERRIDES

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            showProgramStats = getArguments().getBoolean(SHOW_PROGRAM_STATS, true);
        } else {
            throw new IllegalArgumentException("No arguments given");
        }

        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    protected View setContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_program_stats, container, false);
        listView = (ListView) view.findViewById(R.id.list);
        setAdapterView(listView);
        setContentView(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        loadData();
    }

    //----------------------------------------------------------------------------------------------
    // BaseRefreshFragment OVERRIDES

    @Override
    public String getTitle() {
        return showProgramStats ? "Program Stats" : "My Stats";
    }

    @Override
    protected void onRefreshStarted() {
        loadData();
    }

    @Override
    protected void onRefreshFinished() {
        super.onRefreshFinished();
    }

    public void setShowProgramStats(boolean showProgramStats) {
        this.showProgramStats = showProgramStats;
    }

    private List<StatsItem> parseResultsJson(String json) {
        if (showProgramStats) {
            return StatsParser.parsePublic(json);
        } else {
            return StatsParser.parseProtected(json);
        }
    }

    private void loadCachedStats() {
        String statsJson = getContext().getSharedPreferences(Constants.SHARED_PREFS_TAG_LOUISIANA, Context.MODE_PRIVATE)
                .getString(PREFS_CACHED_STATS + String.valueOf(showProgramStats), null);

        Log.d("***************", "loadCachedStats: statsJson: " + statsJson);


        if (!TextUtils.isEmpty(statsJson)) {
            listView.setAdapter(new StatListItemAdapter(getContext(), R.layout.stats_list_item, parseResultsJson(statsJson)));
            onDataLoaded();
        }
    }
    private void loadData() {
        // load any cached stats first
        loadCachedStats();

        // prepare the API call
        Call<ResponseBody> call;

        // change the call depending on what mode the fragment is in
        if (showProgramStats) {
            PublicApiService apiService = ApiServiceGenerator.createService(PublicApiService.class);
            call = apiService.getStats();
        } else {
            ProtectedApiService apiService = ApiServiceGenerator.createService(ProtectedApiService.class);
            call = apiService.getStats();
        }

        final Context context = getContext();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        final String json = response.body().string();
                        final List<StatsItem> results = parseResultsJson(json);

                        if (!TextUtils.isEmpty(json)) {
                            context.getSharedPreferences(Constants.SHARED_PREFS_TAG_LOUISIANA, Context.MODE_PRIVATE)
                                    .edit()
                                    .putString(PREFS_CACHED_STATS + String.valueOf(showProgramStats), json)
                                    .apply();
                        }

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                listView.setAdapter(new StatListItemAdapter(context, R.layout.stats_list_item, results));
                                onDataLoaded();
                            }
                        });


                    } else {
                        throw new NetworkErrorException("Received a " + response.code() + " from the request: " + call.request().url().toString());
                    }

                } catch (IOException | NetworkErrorException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(final Call<ResponseBody> call, final Throwable t) {
                Utils.logError(t);
            }
        });

    }

    //----------------------------------------------------------------------------------------------
    // ADAPTER CLASS

    private class StatListItemAdapter extends ArrayAdapter<StatsItem> {

        private int resource;

        private AQuery aQuery;

        public StatListItemAdapter(Context context, int resource, List<StatsItem> objects) {
            super(context, resource, objects);
            this.aQuery = new AQuery(context);
            this.resource = resource;
        }

        @SuppressLint("ViewHolder")
        // AQuery does the inflation internally and the view is really simple
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            StatsItem item = this.getItem(position);

            convertView = this.aQuery.inflate(convertView, resource, parent);
            aQuery.recycle(convertView);

            aQuery.id(R.id.title).text(item.title);

            LinearLayout linearLayout = (LinearLayout) aQuery.id(R.id.item_list_layout).getView();
            linearLayout.removeAllViewsInLayout();

            for (Map.Entry<StatsItem.KeyTitle, String> entry : item.items.entrySet()) {
                AQuery aTemp = new AQuery(getActivity());
                StatsItem.KeyTitle key = entry.getKey();
                String value = entry.getValue();

                View itemView = aTemp.inflate(null, R.layout.stats_list_item_textview_value, linearLayout);
                aTemp.recycle(itemView);

                aTemp.id(R.id.title).text(key.getTitle());
                aTemp.id(R.id.value).text(value);

                if (key.getSubTitle() != null) {
                    aTemp.id(R.id.sub_title).text(key.getSubTitle()).visible();
                } else {
                    aTemp.id(R.id.sub_title).gone();
                }

                linearLayout.addView(itemView);
            }

            if (item.items.size() == 0) {
                aQuery.id(R.id.placeholder).visible();
            } else {
                aQuery.id(R.id.placeholder).gone();
            }

            return convertView;
        }
    }

}
