package gov.louisiana.ldwf.taglouisiana.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.gson.reflect.TypeToken;
import com.raizlabs.android.dbflow.data.Blob;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import javax.annotation.Nullable;

import gov.louisiana.ldwf.taglouisiana.gson.GsonUtil;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.EnteredPendingCapture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.images.AttachedPhoto;
import gov.louisiana.ldwf.taglouisiana.models.profile.Angler;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.util.TagImage;


/**
 * Created by daniel on 6/9/14.
 */
public class EnterCapturePreferencesWrapper {
    private SharedPreferences prefs;
    private Context context;

    public EnterCapturePreferencesWrapper(String preferencesName, Context context) {
        this.prefs = context.getSharedPreferences(preferencesName, Context.MODE_PRIVATE);
        this.context = context;
    }

    public Angler getAngler() {
        Angler angler = new Angler();

        angler.firstName = (prefs.getString(Constants.ENTER_CAPTURE_ANGLER_FIRST_NAME, ""));
        angler.lastName = (prefs.getString(Constants.ENTER_CAPTURE_ANGLER_LAST_NAME, ""));
        angler.email = (prefs.getString(Constants.ENTER_CAPTURE_ANGLER_EMAIL, ""));
        angler.mobileNumber = (prefs.getString(Constants.ENTER_CAPTURE_ANGLER_PHONE, ""));

        return angler;
    }

    public Set<EnteredPendingCapture> getDraftCaptures() {
        Set<EnteredPendingCapture> results = new LinkedHashSet<>();
        Set<String> tagNumbers = tagNumbers();
        Set<TagImage> tagImageSet = tagImages();

        for (final String tagNumber : tagNumbers) {

            final EnteredPendingCapture pendingCapture = new EnteredPendingCapture();

            Collection<TagImage> captureImages = Collections2.filter(tagImageSet, new Predicate<TagImage>() {
                @Override
                public boolean apply(@Nullable TagImage input) {
                    return input.getTagNumber().equals(tagNumber);
                }
            });

            pendingCapture.tagNumber = tagNumber;

            try {
                pendingCapture.date = Constants.DATE_FORMATTER.parse(prefs.getString(Constants.ENTER_CAPTURE_DATE, ""));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            pendingCapture.isRecapture = (prefs.getBoolean(Constants.ENTER_CAPTURE_RECAPTURE, false));

            pendingCapture.timeOfDayOptionId = prefs.getLong(Constants.ENTER_CAPTURE_TIME, 0);
            pendingCapture.speciesId = prefs.getLong(Constants.ENTER_CAPTURE_SPECIES, 0);
            pendingCapture.recaptureDispositionOptionId = prefs.getLong(Constants.ENTER_CAPTURE_DISPOSITION, 0);
            pendingCapture.speciesLengthId = prefs.getLong(Constants.ENTER_CAPTURE_LENGTH, 0);
            float length = prefs.getFloat(Constants.ENTER_CAPTURE_MANUAL_LENGTH, 0.0f);
            Float boxedLength = null;
            if(length > 0.0){
                boxedLength = length;
            }
            pendingCapture.length = boxedLength;

            pendingCapture.latitude = Double.parseDouble(prefs.getString(Constants.ENTER_CAPTURE_LATITUDE, "0"));
            pendingCapture.longitude = Double.parseDouble(prefs.getString(Constants.ENTER_CAPTURE_LONGITUDE, "0"));
            pendingCapture.gpsType = prefs.getString(Constants.ENTER_CAPTURE_GPS_TYPE, "");

            pendingCapture.comments = prefs.getString(Constants.ENTER_CAPTURE_COMMENTS, "");
            pendingCapture.locationDescription = prefs.getString(Constants.ENTER_CAPTURE_LOCATION_DESCRIPTION, "");

            if (pendingCapture.uuid == null) {
                pendingCapture.uuid = UUID.randomUUID();
            }

            for (TagImage tagImage : captureImages) {
                AttachedPhoto attachedPhoto = new AttachedPhoto();
                attachedPhoto.uuid = UUID.randomUUID();
                attachedPhoto.enteredPendingCaptureUuid = pendingCapture.uuid;
                attachedPhoto.photoData = new Blob(tagImage.asByteArray());

                // if the attached photo isn't already attached.
                if (!pendingCapture.getImages().contains(attachedPhoto)) {
                    pendingCapture.getImages().add(attachedPhoto);
                }
            }

            results.add(pendingCapture);
        }


        return results;
    }

    /*
        HELPERS TO GET THE SAVED TAGS
     */

    public Set<String> tagNumbers() {
        return prefs.getStringSet(Constants.ENTER_CAPTURE_TAG_NO, new TreeSet<String>());
    }

    public void clearTags() {
        prefs.edit().putStringSet(Constants.ENTER_CAPTURE_TAG_NO, new TreeSet<String>()).apply();
    }

    /*
        HELPERS TO GET THE TAG IMAGES
     */

    public Set<TagImage> tagImages() {
        List<TagImage> images = GsonUtil.gson().fromJson(prefs.getString(Constants.ENTER_CAPTURE_IMAGES, "[]"), new TypeToken<ArrayList<TagImage>>(){}.getType());
        return new TreeSet<>(images);
    }

    public void commitTagImages(Set<TagImage> images) {
        prefs.edit()
                .putString(Constants.ENTER_CAPTURE_IMAGES, GsonUtil.gson().toJson(images))
                .apply();
    }

    public void clearTagImages() {
        prefs.edit()
                .putString(Constants.ENTER_CAPTURE_IMAGES, "[]")
                .apply();
    }
}
