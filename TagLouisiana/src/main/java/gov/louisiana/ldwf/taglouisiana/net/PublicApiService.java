package gov.louisiana.ldwf.taglouisiana.net;

import android.support.annotation.NonNull;

import java.util.Map;

import gov.louisiana.ldwf.taglouisiana.gson.responses.publicApi.CombinedResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by danielward on 8/25/16.
 */
public interface PublicApiService {
    @GET("/api/v2/public/options/combined.json")
    public Call<CombinedResponse> getCombined(@QueryMap(encoded = true) @NonNull Map<String, String> params);

    @GET("/api/v2/public/info/dashboard.json")
    public Call<ResponseBody> getStats();

    @POST("/api/v2/public/fish_event/recapture.json")
    public Call<ResponseBody> postReportedRecapture(@Body @NonNull Map<String, Object> params);
}
