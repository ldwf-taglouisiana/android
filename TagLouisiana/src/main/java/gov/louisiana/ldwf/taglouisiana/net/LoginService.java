package gov.louisiana.ldwf.taglouisiana.net;

import android.support.annotation.NonNull;

import gov.louisiana.ldwf.taglouisiana.accounts.OAuthToken;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by danielward on 8/25/16.
 */
public interface LoginService {
    @FormUrlEncoded
    @POST("/oauth/token.json")
    public Call<OAuthToken> getAccessToken(
            @Field("username") @NonNull String username,
            @Field("password") @NonNull String password,
            @Field("grant_type") @NonNull String grantType,
            @Field("client_id") @NonNull String clientId,
            @Field("client_secret") @NonNull String clientSecret
    );
}
