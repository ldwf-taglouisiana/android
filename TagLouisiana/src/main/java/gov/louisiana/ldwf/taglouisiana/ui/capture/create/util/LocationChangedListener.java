package gov.louisiana.ldwf.taglouisiana.ui.capture.create.util;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by daniel on 5/23/14.
 */
public interface LocationChangedListener {
  public void onUpdate(LatLng point);
}
