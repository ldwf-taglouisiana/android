package gov.louisiana.ldwf.taglouisiana.net;

import android.support.annotation.NonNull;

import java.util.Map;

import gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi.CaptureHistoryResponse;
import gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi.CombinedResponse;
import gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi.MeResponse;
import gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi.PostCombinedDataResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by danielward on 8/25/16.
 */
public interface ProtectedApiService {
    @GET("/api/v2/protected/account/me.json")
    public Call<MeResponse> getme();

    @GET("/api/v2/protected/account/dashboard.json")
    public Call<ResponseBody> getStats();

    @GET("/api/v2/protected/captures/{id}/history.json")
    public Call<CaptureHistoryResponse> fishEventHistory(@Path("id") long captureId);

    @GET("/api/v2/protected/combined_data.json")
    public Call<CombinedResponse> getCombined(@QueryMap @NonNull Map<String, String> params);

    @GET("/api/v2/protected/combined_data.json")
    public Call<ResponseBody> getCombinedGeneric(@QueryMap @NonNull Map<String, String> params);

    @POST("/api/v2/protected/combined_data.json")
    public Call<PostCombinedDataResponse> postCombined(@Body @NonNull Map<String, Object> params);

    @GET("/api/v2/protected/draft_captures/status.json")
    public Call<ResponseBody> getStatus(@Query("id") long id);



}
