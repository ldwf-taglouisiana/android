package gov.louisiana.ldwf.taglouisiana.models.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.Table;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.RemoteItem;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/29/14
 */
@Table(database = AppDatabase.class, primaryKeyConflict = ConflictAction.REPLACE)
public class User extends RemoteItem {

    @Expose
    @SerializedName("first_name")
    public String firstName;

    @Expose
    @SerializedName("last_name")
    public String lastName;

    @Expose
    @SerializedName("email")
    public String email;

    public Profile profile;
}
