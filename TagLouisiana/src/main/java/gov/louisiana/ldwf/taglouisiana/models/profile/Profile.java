package gov.louisiana.ldwf.taglouisiana.models.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;

import java.net.URI;
import java.util.Date;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.BaseItem;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/29/14
 */
@Table(database = AppDatabase.class, primaryKeyConflict = ConflictAction.REPLACE)
public class Profile extends BaseItem {

    @PrimaryKey(autoincrement = true)
    public long _id;

    @Expose
    @SerializedName("angler")
    @ForeignKey(saveForeignKeyModel = true)
    public Angler angler;

    @Expose
    @SerializedName("user")
    @ForeignKey(saveForeignKeyModel = true)
    public User user;

    @Expose
    @SerializedName("updated_at_epoch")
    @Column
    public Date updatedAt;

    @Override
    public long id() {
        return _id;
    }

    public String firstName() {
        return user.firstName != null ? user.firstName : angler.firstName;
    }

    public String lastName() {
        return user.lastName != null ? user.lastName : angler.lastName;
    }


    public String email() {
        return user.email != null ? user.email : angler.email;
    }

    public URI getGravatarUri() {
        String hash = Utils.getMD5String(email());
        String gravatarUrl = "https://www.gravatar.com/avatar/" + hash + "?s=204&d=identicon";
        return URI.create(gravatarUrl);
    }
}
