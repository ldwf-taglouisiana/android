package gov.louisiana.ldwf.taglouisiana.ui.registration;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Page;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PasswordInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PasswordInfoFragment extends Page {

  public PasswordInfoFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   * <p/>
   * //@param param1 Parameter 1.
   * //@param param2 Parameter 2.
   *
   * @return A new instance of fragment PasswordInfoFragment.
   */
  public static PasswordInfoFragment newInstance() {
    PasswordInfoFragment fragment = new PasswordInfoFragment();
    return fragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_password_info, container, false);
  }

  @Override
  public void updateView() {

  }

  @Override
  public void restoreValues() {

  }

  @Override
  public void commitValues() {
    EditText password = (EditText) getView().findViewById(R.id.password);
    EditText passwordConfirmation = (EditText) getView().findViewById(R.id.password_confirmation);

    SharedPreferences.Editor editor = getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE).edit();

    editor.putString(Constants.REGISTER_PASSWORD, password.getText().toString());
    editor.putString(Constants.REGISTER_PASSWORD_CONFIRMATION, passwordConfirmation.getText().toString());

    editor.commit();
  }

  public static class Builder extends Page.Builder {

    @Override
    public Page create() {
      Page result = new PasswordInfoFragment();
      result.setSharedPrefs(this.getPrefs());

      return result;
    }
  }
}
