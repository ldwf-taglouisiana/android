package gov.louisiana.ldwf.taglouisiana.ui.species.guide;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.raizlabs.android.dbflow.sql.queriable.ModelQueriable;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.adapters.SpeciesListAdapter;
import gov.louisiana.ldwf.taglouisiana.models.species.Species;
import gov.louisiana.ldwf.taglouisiana.models.species.Species_Table;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseRefreshableQueryListFragment;
import gov.louisiana.ldwf.taglouisiana.ui.species.detail.SpeciesDetailActivity;

public class SpeciesListFragment extends BaseRefreshableQueryListFragment<Species> {

    // public constants
    public static final String SHOW_TARGET_ARG = "SPECIES_LIST_FRAGMENT.SHOW_TARGET";

    // the species select listener
    private OnSpeciesSelectedListener listener;

    //------------------------------------------------------------------------------------------------
    // LISTENER CLASSES

    public static SpeciesListFragment getTagetSpeciesInstance() {
        Bundle args = new Bundle();
        args.putBoolean(SpeciesListFragment.SHOW_TARGET_ARG, true);
        SpeciesListFragment temp = new SpeciesListFragment();
        temp.setArguments(args);

        return temp;
    }

    public static SpeciesListFragment getAllSpeciesInstance() {
        Bundle args = new Bundle();
        args.putBoolean(SpeciesListFragment.SHOW_TARGET_ARG, false);
        SpeciesListFragment temp = new SpeciesListFragment();
        temp.setArguments(args);

        return temp;
    }

    @Override
    protected View setContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View myFragmentView = inflater.inflate(R.layout.target_species_layout, container, false);

        // get the adapter view and add the click listener to each item.
        final GridView speciesListView = (GridView) myFragmentView.findViewById(R.id.target_species_list_view);
        speciesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                listener.onItemSelected((Species)adapterView.getItemAtPosition(position));
            }
        });

        setAdapterView(speciesListView);
        setContentView(myFragmentView);

        return myFragmentView;
    }

    //------------------------------------------------------------------------------------------------
    // FRAGMENT OVERRIDES
    //------------------------------------------------------------------------------------------------

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    /*
     * When the activity attaches, do a test to see if it implements the
     * listener interface. If not then create a listener that starts a new
     * SpeciesDetailActivity
     */
        final Activity parentActivity = activity;

        try {
            this.listener = (OnSpeciesSelectedListener) activity;
        } catch (ClassCastException e) {
            listener = new OnSpeciesSelectedListener() {
                @Override
                public void onItemSelected(Species species) {
                    Intent intent = new Intent(parentActivity, SpeciesDetailActivity.class);
                    intent.putExtra(SpeciesDetailActivity.SPECIES_ID_ARG, species._id);
                    startActivity(intent);
                }
            };
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getView().findViewById(R.id.progress_view).setVisibility(View.GONE);
    }

    //------------------------------------------------------------------------------------------------
    // BASE FRAGMENT OVERRIDES
    //------------------------------------------------------------------------------------------------

    @Override
    public String getTitle() {
        return isShowingTargetSpecies() ? "Target Species" : "All Species";
    }

    //------------------------------------------------------------------------------------------------
    // Overrides for BaseRefreshableQueryListFragment
    //------------------------------------------------------------------------------------------------


    @Override
    protected BaseAdapter getNewAdapter() {
        return new SpeciesListAdapter(getActivity(), queryList, R.layout.target_species_item_layout);
    }

    @Override
    protected Class<Species> getQueryableClass() {
        return Species.class;
    }

    @Override
    protected ModelQueriable<Species> buildQuery(@Nullable String queryString) {
        ModelQueriable<Species> query = SQLite.select()
                .from(Species.class)
                .where(Species_Table.isPublished.eq(true))
                .and(Species_Table.isTarget.is(isShowingTargetSpecies()));

        if (queryString != null) {
            // cast should be safe, since above we ended with a "Where" statement
            query = ((Where<Species>)query)
                    .and(Species_Table.commonName.like("%" + queryString + "%"));
        }

        return query;
    }

    //------------------------------------------------------------------------------------------------
    // HELPERS
    //------------------------------------------------------------------------------------------------

    private boolean isShowingTargetSpecies() {
        if (this.getArguments() != null) {
            return this.getArguments().getBoolean(SHOW_TARGET_ARG, false);
        } else {
            return false;
        }
    }

    //------------------------------------------------------------------------------------------------
    // Inner Classes
    //------------------------------------------------------------------------------------------------

    public interface OnSpeciesSelectedListener {
        public void onItemSelected(Species species);
    }

}
