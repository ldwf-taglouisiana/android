package gov.louisiana.ldwf.taglouisiana.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Index;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;

import java.util.Date;


/**
 * Created by danielward on 8/17/16.
 */
public abstract class RemoteItem extends BaseItem {

    @PrimaryKey(quickCheckAutoIncrement = true)
    @Expose
    @SerializedName("id")
    @Column(name="_id")
    public long _id;

    @Index
    @Expose(serialize = false)
    @SerializedName("updated_at_epoch")
    @Column
    public Date updatedAt;

    @Override
    public long id() {
        return _id;
    }
}
