package gov.louisiana.ldwf.taglouisiana.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.graphics.Palette;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;

import java.util.List;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.models.species.Species;
import gov.louisiana.ldwf.taglouisiana.net.ApiUtils;

public class SpeciesListAdapter extends ArrayAdapter<Species> {

    private int resource;
    private AQuery aQuery;


    public SpeciesListAdapter(Context context, List<Species> users, int resource) {
        super(context, 0, users);

        this.aQuery = new AQuery(context);
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = aQuery.inflate(convertView, resource, parent);
        aQuery.recycle(convertView);

        final Species species = getItem(position);
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();

        if (viewHolder == null) {
            viewHolder = new ViewHolder();
            viewHolder.image = aQuery.id(R.id.target_species_image).getImageView();
            viewHolder.speciesName = aQuery.id(R.id.target_species_text).getTextView();
            viewHolder.speciesFamily = aQuery.id(R.id.target_species_family).getTextView();
            viewHolder.textContainer = aQuery.id(R.id.text_container).getView();

            convertView.setTag(viewHolder);
        }


        aQuery.id(viewHolder.speciesName).text(species.commonName);

        if (species.familyName == null) {
            aQuery.id(viewHolder.speciesFamily).gone();
        } else {
            aQuery.id(viewHolder.speciesFamily).visible();
            aQuery.id(viewHolder.speciesFamily).text(species.familyName);
        }

        final ViewHolder finalHolder = viewHolder;
        aQuery.id(viewHolder.image).progress(R.id.species_image_progress).image(ApiUtils.urlWithHost(species.imageUrl).toString(), true, true, 0, 0, new BitmapAjaxCallback() {

            @Override
            public void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

               iv.setImageBitmap(bm);

                Palette.from(bm).generate(new Palette.PaletteAsyncListener() {
                    @Override
                    public void onGenerated(Palette palette) {
                        finalHolder.textContainer.setBackgroundColor(palette.getMutedColor(getContext().getResources().getColor(R.color.tagging_blue)));
                    }
                });
            }

        });

        return convertView;
    }

    public static class ViewHolder {
        View textContainer;
        TextView speciesName;
        TextView speciesFamily;
        ImageView image;
    }

}
