package gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.HashMap;
import java.util.Map;

import gov.louisiana.ldwf.taglouisiana.models.fishEvents.EnteredPendingCapture;
import gov.louisiana.ldwf.taglouisiana.models.tag.EnteredTagRequest;
import gov.louisiana.ldwf.taglouisiana.models.timeEntry.EnteredTimeOnWaterEntry;

/**
 * Created by danielward on 10/31/16.
 */

public class PostCombinedDataResponse {

    @Expose
    @SerializedName("draft_captures")
    public BasePostDataResponse draftCapturesResponse;

    @Expose
    @SerializedName("tag_requests")
    public BasePostDataResponse tagRequetsResponse;

    @Expose
    @SerializedName("time_logs")
    public BasePostDataResponse timeLogsResponse;

    public Map<Class<? extends BaseModel>, BasePostDataResponse> getReponseItems() {
        Map<Class<? extends BaseModel>, BasePostDataResponse> results = new HashMap<>();

        Map<Class<? extends BaseModel>, BasePostDataResponse> responses = new HashMap();
        responses.put(EnteredPendingCapture.class, draftCapturesResponse);
        responses.put(EnteredTagRequest.class, tagRequetsResponse);
        responses.put(EnteredTimeOnWaterEntry.class, timeLogsResponse);

        for (Map.Entry<Class<? extends BaseModel>, BasePostDataResponse> responseItem : responses.entrySet()) {
            if (responseItem.getValue() != null && !responseItem.getValue().processedUUIDS.isEmpty()) {
                results.put(responseItem.getKey(), responseItem.getValue());
            }
        }

        return results;
    }
}
