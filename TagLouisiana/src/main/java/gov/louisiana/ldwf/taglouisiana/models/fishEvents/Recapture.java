package gov.louisiana.ldwf.taglouisiana.models.fishEvents;

import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.geojson.GeoJsonLayer;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.annotations.IgnoreSerializedEpoch;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.images.CapturePhoto;
import gov.louisiana.ldwf.taglouisiana.models.options.RecaptureDispositionOption;
import gov.louisiana.ldwf.taglouisiana.models.options.RecaptureDispositionOption_Table;
import gov.louisiana.ldwf.taglouisiana.utils.GeoUtil;

/**
 * Created by danielward on 8/17/16.
 */
@IgnoreSerializedEpoch
@Table(database = AppDatabase.class)
public class Recapture extends AbstractFishEvent {

    @Expose
    @SerializedName("recapture_disposition_id")
    @ForeignKey(tableClass = RecaptureDispositionOption.class)
    public long recaptureDispositionId;

    @Expose
    @SerializedName("angler_id")
    public String anglerId;

    @Expose
    @SerializedName("angler_name")
    public String anglerName;

    @Expose
    @SerializedName("recapture_number")
    public int recaptureNumber;

    @Expose
    @SerializedName("distance_traveled_meters")
    public double distaneFromCaptureMeters;

    @Expose
    @SerializedName("geo_json")
    public JsonObject geoJson;

    @Expose
    @SerializedName("images")
    public List<CapturePhoto> images;

    private GeoJsonLayer geoJsonLayer;
    private LatLngBounds geoJsonBounds;

    public RecaptureDispositionOption recaptureDispositionOption;

    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "images")
    public List<CapturePhoto> getImages() {
        if (images == null || images.isEmpty()) {
//            images = SQLite.select()
//                    .from(Ant.class)
//                    .where(FishEventPhoto_Table)
//                    .queryList();
        }
        return images;
    }

    @Override
    public boolean hasValidLocation() {
        return this.distaneFromCaptureMeters > 0f;
    }

    @Override
    public boolean hasShowableLocation() {
        return this.geoJsonLayer != null && GeoUtil.hasGeometry(this.geoJsonLayer);
    }

    public RecaptureDispositionOption getRecaptureDispositionOption() {
        if (recaptureDispositionOption == null) {
            recaptureDispositionOption = SQLite.select()
                    .from(RecaptureDispositionOption.class)
                    .where(RecaptureDispositionOption_Table._id.eq(recaptureDispositionId))
                    .querySingle();
        }

        return recaptureDispositionOption;
    }

    public GeoJsonLayer getGeoJsonLayer() {
        return geoJsonLayer;
    }

    public void setGeoJsonLayer(GeoJsonLayer geoJsonLayer) {
        this.geoJsonLayer = geoJsonLayer;
        this.getGeoJsonBounds();
    }

    public LatLngBounds getGeoJsonBounds() {
        if (this.hasShowableLocation()) {
            this.geoJsonBounds = GeoUtil.geoJsonLatLngBounds(this.geoJsonLayer);
        }

        return this.geoJsonBounds;
    }
}
