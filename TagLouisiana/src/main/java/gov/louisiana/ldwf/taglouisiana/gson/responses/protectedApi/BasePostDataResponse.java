package gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by danielward on 10/31/16.
 */

public class BasePostDataResponse {
    @Expose
    @SerializedName("errors")
    public JsonElement errors;

    @Expose
    @SerializedName("status")
    public long status;

    @Expose
    @SerializedName("uuids")
    public List<UUID> processedUUIDS;

    public List<String> getUUIDStringList() {
        List<String> result = new ArrayList<>(processedUUIDS.size());

        for (UUID uuid : processedUUIDS) {
            result.add(uuid.toString());
        }

        return result;
    }
}
