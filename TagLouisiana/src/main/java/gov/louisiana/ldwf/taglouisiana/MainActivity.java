package gov.louisiana.ldwf.taglouisiana;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.runtime.FlowContentObserver;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.EnteredPendingCapture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.PendingCapture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Recapture;
import gov.louisiana.ldwf.taglouisiana.models.profile.Angler;
import gov.louisiana.ldwf.taglouisiana.models.profile.Profile;
import gov.louisiana.ldwf.taglouisiana.models.profile.User;
import gov.louisiana.ldwf.taglouisiana.models.tag.EnteredTagRequest;
import gov.louisiana.ldwf.taglouisiana.models.tag.Tag;
import gov.louisiana.ldwf.taglouisiana.models.timeEntry.EnteredTimeOnWaterEntry;
import gov.louisiana.ldwf.taglouisiana.ui.NavigationDrawerFragment;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.EnterCaptureActivity;
import gov.louisiana.ldwf.taglouisiana.ui.capture.my.CapturesListFragment;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseActivity;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;
import gov.louisiana.ldwf.taglouisiana.ui.core.dialog.BaseDialogFragment;
import gov.louisiana.ldwf.taglouisiana.ui.dialog.OrderTagsDialogFragment;
import gov.louisiana.ldwf.taglouisiana.ui.dialog.VolunteerHoursDialogFragment;
import gov.louisiana.ldwf.taglouisiana.ui.draftCapture.PendingCaptureTabFragment;
import gov.louisiana.ldwf.taglouisiana.ui.species.guide.SpeciesGuideFragment;
import gov.louisiana.ldwf.taglouisiana.ui.stats.StatsMainFragment;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

public class MainActivity extends BaseActivity {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private Fragment currentFragment;
    private String currentFragmentClass;
    private FlowContentObserver flowContentObserver;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        getSupportFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {
                    public void onBackStackChanged() {
                        BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.container);
                        if(fragment != null){
                            currentFragmentClass = fragment.getClass().getCanonicalName();
                            mTitle = fragment.getTitle();
                        } else{
                            mTitle = getString(R.string.default_app_bar_title);
                        }
                        getSupportActionBar().setTitle(mTitle);
                    }
                });

        flowContentObserver = new FlowContentObserver();
        flowContentObserver.addOnTableChangedListener(new FlowContentObserver.OnTableChangedListener() {
            @Override
            public void onTableChanged(@Nullable Class<?> tableChanged, BaseModel.Action action) {
                mNavigationDrawerFragment.updateState();
            }
        });

        // if the app is not starting for the first time then switch to the previous fragment
        if(savedInstanceState != null){
            restoreFragment(savedInstanceState.getString("SavedFragment"));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("SavedFragment", currentFragmentClass);
        super.onSaveInstanceState(outState);
    }



    @Override
    protected void onStart() {
        super.onStart();
        flowContentObserver.registerForContentChanges(this, Profile.class);
        requestSync();
    }

    @Override
    protected void onStop() {
        super.onStop();
        flowContentObserver.unregisterForContentChanges(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.activity_main, menu);
            restoreActionBar();
            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_capture:
                Intent intent = new Intent(this, EnterCaptureActivity.class);
                intent.putExtra(EnterCaptureActivity.SIGNED_IN_TAG, isSignedIn());
                this.startActivity(intent);
                return true;
        }

        return (super.onOptionsItemSelected(item));
    }

    //----------------------------------------------------------------------------------------------
    // BaseActivity Override


    @Override
    protected int contentViewLayoutId() {
        return R.layout.activity_my;
    }

    @Override
    protected void signInFinished() {
        super.signInFinished();

        if (currentFragment == null || currentFragment instanceof StatsMainFragment) {
            BaseFragment fragment = new StatsMainFragment();
            Bundle args = new Bundle();
            args.putBoolean(StatsMainFragment.IS_SIGNED_IN_ARG, this.isSignedIn());
            fragment.setArguments(args);

            currentFragment = fragment;

            switchFragment(currentFragment);
        }

        mNavigationDrawerFragment.updateState();

        findViewById(R.id.progress_view).setVisibility(View.GONE);

        requestSync();

    }

    @Override
    protected void signOutFinished() {
        super.signOutFinished();

        /*
            Removal all the data that related to the signed in account
         */
        @SuppressWarnings("unchecked")
        final Class<? extends BaseModel>[] signedInData = (Class<? extends BaseModel>[]) new Class[]{
                Capture.class,
                Recapture.class,
                PendingCapture.class,
                Tag.class,
                EnteredPendingCapture.class,
                EnteredTimeOnWaterEntry.class,
                EnteredTagRequest.class,
                Profile.class,
                Angler.class,
                User.class
        };

        ITransaction transaction = new ITransaction() {
            @Override
            public void execute(DatabaseWrapper databaseWrapper) {
                // iterate through all the responses and remove all the processed items by UUID
                for (Class<? extends BaseModel> model : signedInData) {
                    SQLite.delete().from(model).execute(databaseWrapper);
                }
            }
        };
        FlowManager.getDatabase(AppDatabase.class)
                .beginTransactionAsync(transaction)
                .error(new Transaction.Error() {
                    @Override
                    public void onError(Transaction transaction, Throwable error) {
                        Utils.logError("Removal of Signed out items", error);
                    }
                }).build().execute();


        /*
            Update the navigation drawer, and navigate to the stats fragment
         */
        mNavigationDrawerFragment.updateState();


        BaseFragment fragment = new StatsMainFragment();
        Bundle args = new Bundle();
        args.putBoolean(StatsMainFragment.IS_SIGNED_IN_ARG, this.isSignedIn());
        fragment.setArguments(args);

        currentFragment = fragment;

        switchFragment(currentFragment);

    }
    public void restoreActionBar() {
        setToolbarTitle(mTitle);
    }

    public void onNavigationDrawerItemClicked(View view) {
        mNavigationDrawerFragment.closeDrawer();
        switch (view.getId()) {
            case R.id.nav_item_home:
                BaseFragment fragment = new StatsMainFragment();
                Bundle args = new Bundle();
                args.putBoolean(StatsMainFragment.IS_SIGNED_IN_ARG, this.isSignedIn());
                fragment.setArguments(args);
                switchFragment(fragment);
                break;

            case R.id.nav_item_pending_captures:
                switchFragment(PendingCaptureTabFragment.newInstance());
                break;

            case R.id.nav_item_my_captures:
                switchFragment(new CapturesListFragment());
                break;

            case R.id.nav_item_enter_captures:
                Intent taggedFishIntent = new Intent(this, EnterCaptureActivity.class);
                startActivity(taggedFishIntent);
                break;

            case R.id.nav_item_enter_time_on_water:
                showDialog(VolunteerHoursDialogFragment.newInstance());
                break;

            case R.id.nav_item_recaptured_a_fish:
                Intent recaptureIntent = new Intent(this, EnterCaptureActivity.class);
                recaptureIntent.putExtra(EnterCaptureActivity.REPORTING_RECAPTURE, true);
                startActivity(recaptureIntent);
                break;

            case R.id.nav_item_request_tags:
                showDialog(OrderTagsDialogFragment.newInstance());
                break;

            case R.id.nav_item_species_guide:
                switchFragment(new SpeciesGuideFragment());
                break;

            case R.id.nav_item_sign_in:
                findViewById(R.id.progress_view).setVisibility(View.VISIBLE);
                signIn();
                break;

            case R.id.nav_item_sign_out:
                signOut();
                break;

            case R.id.nav_contact_us:
                Intent contactUsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:info@taglouisiana.com"));
                contactUsIntent.putExtra(Intent.EXTRA_SUBJECT, "TagLouisiana Feedback");
                startActivity(Intent.createChooser(contactUsIntent, "Send Email"));
                break;

            default:
                break;
        }
    }

    private void showDialog(BaseDialogFragment fragment) {
        fragment.show(getSupportFragmentManager(), fragment.getTitle());
    }

    private void switchFragment(final Fragment fragment) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack("main_activity")
                .commitAllowingStateLoss();

        if (fragment instanceof BaseDialogFragment) {
            mTitle = ((BaseDialogFragment) fragment).getTitle();
        } else if (fragment instanceof BaseFragment) {
            BaseFragment baseFragment = ((BaseFragment) fragment);

            if (!baseFragment.hasTabs()) {
                hideTabs();
            }

            mTitle = baseFragment.getTitle();
        }

       setToolbarTitle(mTitle);

        currentFragment = fragment;
        currentFragmentClass = fragment.getClass().getCanonicalName();

    }

    public void restoreFragment(String fragmentName){
        if(fragmentName != null || fragmentName.length() != 0){
            try {
                switchFragment((BaseFragment) Class.forName(fragmentName).getConstructor().newInstance());
            } catch(Exception e){
                Log.e("MainActivity.java", "restoreFragment: ",e);
            }
        }
    }
}
