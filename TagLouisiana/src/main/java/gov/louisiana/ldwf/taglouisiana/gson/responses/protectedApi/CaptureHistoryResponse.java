package gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.gson.responses.BaseResponse;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.AbstractFishEvent;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Recapture;

/**
 * Created by danielward on 8/25/16.
 */
public class CaptureHistoryResponse extends BaseResponse {

    @Expose
    @SerializedName("capture")
    public Capture capture;

    @Expose
    @SerializedName("recaptures")
    public List<Recapture> recaptures;

    public List<AbstractFishEvent> items;

    @Override
    public Class getModelClass() {
        return AbstractFishEvent.class;
    }

    @Override
    public List<AbstractFishEvent> getItems() {
        if (items == null) {
            items = new ArrayList<>(recaptures.size() + 1);
            items.add(capture);
            items.addAll(recaptures);
        }

        return getItems();
    }

    public List<Recapture> getRecaptures() {
        if (recaptures == null) {
            recaptures = new ArrayList<>(0);
        }

        return recaptures;
    }
}
