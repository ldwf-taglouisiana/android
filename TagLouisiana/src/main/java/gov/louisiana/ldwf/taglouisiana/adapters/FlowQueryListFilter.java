package gov.louisiana.ldwf.taglouisiana.adapters;

import android.widget.BaseAdapter;
import android.widget.Filter;

import com.raizlabs.android.dbflow.list.FlowQueryList;
import com.raizlabs.android.dbflow.structure.Model;

import gov.louisiana.ldwf.taglouisiana.models.fishEvents.Capture;

/**
 * Created by danielward on 10/27/16.
 */

public class FlowQueryListFilter<T extends Model> extends Filter {

    public interface FlowQueryListFilterListener<T> {
        void constraintEmpty();
        void resultsPublished(FlowQueryList<T> results);
    }

    private FlowQueryListFilterListener<T> listener;
    private FilterQueryConstraintRequest filterQueryConstraintRequest;
    private BaseAdapter filterableAdapter;

    public FlowQueryListFilter(BaseAdapter filterableAdapter, FlowQueryListFilterListener<T> listener, FilterQueryConstraintRequest filterQueryConstraintRequest ) {
        this.listener = listener;
        this.filterQueryConstraintRequest = filterQueryConstraintRequest;
        this.filterableAdapter = filterableAdapter;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        listener.resultsPublished((FlowQueryList<T>) results.values);
        filterableAdapter.notifyDataSetChanged();  // notifies the data with new filtered values
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values

        /********
         *
         *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
         *  else does the Filtering and returns FilteredArrList(Filtered)
         *
         ********/
        if (constraint == null || constraint.length() == 0) {
            listener.constraintEmpty();
        } else {
            constraint = constraint.toString().toLowerCase();
            FlowQueryList<Capture> constrainedItems = filterQueryConstraintRequest.applyConstraint(constraint.toString());
            // set the Filtered result to return
            results.count = constrainedItems.getCount();
            results.values = constrainedItems;
        }
        return results;
    }
}
