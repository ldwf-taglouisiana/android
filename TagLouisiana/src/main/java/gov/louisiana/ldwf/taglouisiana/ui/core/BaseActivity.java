package gov.louisiana.ldwf.taglouisiana.ui.core;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OnAccountsUpdateListener;
import android.accounts.OperationCanceledException;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.IOException;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.accounts.AccountGeneral;
import gov.louisiana.ldwf.taglouisiana.accounts.AuthenticatorActivity;
import gov.louisiana.ldwf.taglouisiana.accounts.AuthenticatorService;
import gov.louisiana.ldwf.taglouisiana.event_bus.ActivityResultBus;
import gov.louisiana.ldwf.taglouisiana.event_bus.ActivityResultEvent;
import gov.louisiana.ldwf.taglouisiana.sync.SyncUtils;
import gov.louisiana.ldwf.taglouisiana.ui.util.MaterialDialogBuilder;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: danielward
 * Date: 3/19/13
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseActivity extends AppCompatActivity {


    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS_SIGN_OUT = 12543;
    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS_SIGN_IN = 12542;
    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 12541;
    private static final int SIGN_IN_ACTIVITY_RESULT_CODE = 12544;

    private String authToken;
    private Account connectedAccount;
    private AccountManager accountManager;

    private OnAccountsUpdateListener onAccountsUpdateListener;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private View progressOverlayView;

    private String authTokenType = AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS;
    private String accountType = AccountGeneral.ACCOUNT_TYPE;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(this.contentViewLayoutId());
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (checkForPermissions(MY_PERMISSIONS_REQUEST_ACCOUNTS_SIGN_IN, Manifest.permission.GET_ACCOUNTS)) {
            getAccounts();

            try {
                accountManager.addOnAccountsUpdatedListener(onAccountsUpdateListener, null, true);
            } catch (SecurityException e) {
                Utils.logError(e);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (checkForPermissions(MY_PERMISSIONS_REQUEST_ACCOUNTS_SIGN_IN, Manifest.permission.GET_ACCOUNTS)) {
            try {
                accountManager.removeOnAccountsUpdatedListener(onAccountsUpdateListener);
            } catch (SecurityException e) {
                Utils.logError(e);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SIGN_IN_ACTIVITY_RESULT_CODE) {
            signInFinished();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
            ActivityResultBus.getInstance().postQueue(
                    new ActivityResultEvent(requestCode, resultCode, data)
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, final String permissions[], int[] grantResults) {
        MaterialDialogBuilder accoutnPermissionDeniedDialog = new MaterialDialogBuilder(this);
        accoutnPermissionDeniedDialog.setTitle("Permission Required");
        accoutnPermissionDeniedDialog.setContent("This app requires the \"Accounts\" permission to function. You cannot proceed without enabling.");
        accoutnPermissionDeniedDialog.setNegativeButton("Close App", new MaterialDialogBuilder.OnMaterialDialogItemClickListener() {
            @Override
            public void onClick(Dialog dialog) {
                dialog.dismiss();
                BaseActivity.this.finish();
            }
        });
        accoutnPermissionDeniedDialog.setPositiveButton("Enable permission", new MaterialDialogBuilder.OnMaterialDialogItemClickListener() {
            @Override
            public void onClick(Dialog dialog) {
                dialog.dismiss();
                checkForPermissions(requestCode, permissions[0]);
            }
        });

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCOUNTS_SIGN_IN:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    signIn();
                } else {
                    accoutnPermissionDeniedDialog.create().show();
                }
                break;

            case MY_PERMISSIONS_REQUEST_ACCOUNTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getAccounts();
                } else {
                    accoutnPermissionDeniedDialog.create().show();
                }
                break;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    protected int contentViewLayoutId() {
        return R.layout.activity_base;
    }

    protected void init() {
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        progressOverlayView = findViewById(R.id.progress_overlay_container);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setToolbarTitle(getString(R.string.default_app_bar_title));

        accountManager = AccountManager.get(this.getBaseContext());
        onAccountsUpdateListener = new OnAccountsUpdateListener() {
            @Override
            public void onAccountsUpdated(Account[] accounts) {
                getAccounts();

                if (connectedAccount == null) {
                    signOutFinished();
                } else {
                    signInFinished();
                }
            }
        };

    }

    /*
        Toolbar helpers
     */

    public void setToolbarTitle(String title) {
        getToolbar().setTitle(title);
    }

    public void setToolbarTitle(CharSequence title) {
        getToolbar().setTitle(title);
    }

    protected void enableUpButton() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /*
        TabLayout helpers
     */

    public void hideTabs() {
        tabLayout.setVisibility(View.GONE);
    }

    public void showTabs() {
        tabLayout.setVisibility(View.VISIBLE);
    }

    public void setTabViewPager(ViewPager viewPager){
        tabLayout.setupWithViewPager(viewPager);
        showTabs();
    }

    /*
        Progress overlay helpers
     */

    public void showProgressOverlay() {
        crossfade(progressOverlayView, findViewById(R.id.activity_content_container));
    }

    public void hideProgressOverlay() {
        crossfade(findViewById(R.id.activity_content_container), progressOverlayView);
    }

    private void crossfade(final View targetView, final View OldView) {
        int fadeDuration = getResources().getInteger(android.R.integer.config_mediumAnimTime);

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        targetView.setAlpha(0f);
        targetView.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        targetView.animate()
                .alpha(1f)
                .setDuration(fadeDuration)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        OldView.animate()
                .alpha(0f)
                .setDuration(fadeDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        OldView.setVisibility(View.GONE);
                    }
                });
    }

    protected void setContent(int layoutId) {
        ViewGroup parent = (ViewGroup) findViewById(R.id.activity_content_container);
        getLayoutInflater().inflate(layoutId, parent, true);
    }

    public void requestSync() {
        requestSync(new Bundle());
    }

    public void requestSync(Bundle bundle) {
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(connectedAccount, getResources().getString(R.string.common_content_authority), bundle);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    /*
        Account management stuff
     */

    protected Account getConnectedAccount() {
        return connectedAccount;
    }

    public boolean isSignedIn() {
        return getConnectedAccount() != null && !getConnectedAccount().name.equals(AuthenticatorService.DEFAULT_ACCOUNT_NAME);
    }

    public void updateFragment(String newTitleForActivity, Fragment newFragment) {
        // left blank
    }

    public void signIn() {
        if (checkForPermissions(MY_PERMISSIONS_REQUEST_ACCOUNTS_SIGN_IN, Manifest.permission.GET_ACCOUNTS)) {
            addNewAccount();
        }
    }

    private void addNewAccount() {
        if (connectedAccount == null || AuthenticatorService.DEFAULT_ACCOUNT_NAME.equals(connectedAccount.name)) {
            startActivityForResult(new Intent(this, AuthenticatorActivity.class), SIGN_IN_ACTIVITY_RESULT_CODE);
        }
    }

    public void signOut() {
        if (checkForPermissions(MY_PERMISSIONS_REQUEST_ACCOUNTS_SIGN_OUT, Manifest.permission.GET_ACCOUNTS) && connectedAccount != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                accountManager.removeAccount(connectedAccount, this, new AccountManagerCallback<Bundle>() {
                    @Override
                    public void run(AccountManagerFuture<Bundle> future) {
                        try {
                            if (future.getResult() != null) {
                                connectedAccount = null;
                                getAccounts();
                                signOutFinished();
                            }
                        } catch (OperationCanceledException | IOException | AuthenticatorException e) {
                            Utils.logError(e);
                            Toast.makeText(BaseActivity.this, "Could not sign you out", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, null);
            } else {
                accountManager.removeAccount(connectedAccount, new AccountManagerCallback<Boolean>() {
                    @Override
                    public void run(AccountManagerFuture<Boolean> booleanAccountManagerFuture) {
                        try {
                            if(booleanAccountManagerFuture.getResult().booleanValue()) {
                                connectedAccount = null;
                                getAccounts();
                                signOutFinished();
                            }

                        } catch (Exception e) {
                            Utils.logError(e);
                            Toast.makeText(BaseActivity.this, "Could not sign you out", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, null);
            }
        }
    }

    protected void signInFinished() {
        // left blank
    }

    protected void signOutFinished() {
        // left blank
    }

    private boolean checkForPermissions(final int permissionCode, final String permissionType) {
        if (ActivityCompat.checkSelfPermission(this, permissionType) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionType)) {
                Utils.log("User asked for rational");
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permissionType}, permissionCode);
            }

            return false;
        }

        return true;
    }

    private void getAccounts() {
        // check for the needed permissions first
        if (checkForPermissions(MY_PERMISSIONS_REQUEST_ACCOUNTS, Manifest.permission.GET_ACCOUNTS)) {
            try {
                Account[] accounts = accountManager.getAccountsByType(accountType);

                // if there is one account then use it.
                if (accounts.length == 1) {
                    connectedAccount = accounts[0];
                } else if (accounts.length > 1) { // if there is more than one account, then select
                    // TODO add account chooser dialog;
                } else {
                    SyncUtils.CreateSyncAccount(this);
                    connectedAccount = AuthenticatorService.GetAccount();
                }

                getToken();
            } catch (SecurityException e) {
                Utils.logError(e);
            }
        }
    }

    private void getToken() {
        final AccountManagerFuture<Bundle> future = accountManager.getAuthTokenByFeatures(accountType, authTokenType, null, this, null, null,
                new AccountManagerCallback<Bundle>() {
                    @Override
                    public void run(AccountManagerFuture<Bundle> future) {
                        Bundle bnd = null;
                        try {
                            bnd = future.getResult();
                            BaseActivity.this.authToken = bnd.getString(AccountManager.KEY_AUTHTOKEN);

                            if (!BaseActivity.this.authToken.equals(AccountGeneral.DEFAULT_TOKEN)) {
                                String accountName = bnd.getString(AccountManager.KEY_ACCOUNT_NAME);
                                connectedAccount = new Account(accountName, accountType);
                            }

                            signInFinished();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                , null
        );
    }

}