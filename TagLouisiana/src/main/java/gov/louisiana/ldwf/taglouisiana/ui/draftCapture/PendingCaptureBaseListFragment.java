package gov.louisiana.ldwf.taglouisiana.ui.draftCapture;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.raizlabs.android.dbflow.sql.language.From;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.property.PropertyFactory;
import com.raizlabs.android.dbflow.sql.queriable.ModelQueriable;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.adapters.DraftCaptureAdapter;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.AbstractFishEvent;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseRefreshableQueryListFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class PendingCaptureBaseListFragment<T extends AbstractFishEvent> extends BaseRefreshableQueryListFragment<T> {

    public PendingCaptureBaseListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        buildQueryList();
    }

    @Override
    protected View setContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pending_capture_list, container, false);
        ListView listView = (ListView) view.findViewById(android.R.id.list);

        setAdapterView(listView);
        setHasOptionsMenu(true);
        setContentView(view);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_capture_list, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                buildQueryList(newText);
                getAdapter().notifyDataSetChanged();
                return true;
            }
        });
    }

    //----------------------------------------------------------------------------------------------
    // Overrides from BaseRefreshableQueryListFragment
    //----------------------------------------------------------------------------------------------


    @Override
    protected BaseAdapter getNewAdapter() {
        return new DraftCaptureAdapter<T>(getActivity(), queryList, R.layout.draft_capture_list_card);
    }

    @Override
    protected ModelQueriable<T> buildQuery(@Nullable String queryString) {
        ModelQueriable<T> query = SQLite.select()
                .from(getQueryableClass());

        if (queryString != null) {
            // cast should be safe, since above we ended with a "From" statement
            query = ((From<T>)query)
                    .where(PropertyFactory.from("tagNumber").like("%" + queryString + "%"));
        }

        return query;
    }

    //----------------------------------------------------------------------------------------------
    // Overrides from BaseFragment
    //----------------------------------------------------------------------------------------------
}
