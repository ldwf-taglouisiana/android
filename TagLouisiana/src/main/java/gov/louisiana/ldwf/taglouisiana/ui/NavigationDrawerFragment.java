package gov.louisiana.ldwf.taglouisiana.ui;


import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.joanzapata.iconify.Icon;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.PendingCapture;
import gov.louisiana.ldwf.taglouisiana.models.profile.Profile;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseActivity;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;
import gov.louisiana.ldwf.taglouisiana.ui.util.UiUtils;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends BaseFragment {

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListView;
    private View mFragmentContainerView;

    private int mCurrentSelectedPosition = 0;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;

    private int[] signedInItems = {
            R.id.nav_item_enter_time_on_water,
            R.id.nav_item_my_captures,
            R.id.nav_item_enter_captures,
            R.id.nav_item_pending_captures,
            R.id.nav_item_request_tags,
            R.id.nav_item_sign_out,
            R.id.nav_header
    };

    private int[] signedOutItems = {
            R.id.nav_item_sign_in,
            R.id.nav_item_sign_out_top_border
    };
    private Handler viewHandler;

    public NavigationDrawerFragment() {

    }

    //----------------------------------------------------------------------------------------------
    // Activity Overrides

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);
        viewHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);

        // programmatically add the icons to the drawer items
        addIconsToNavItems(getView());

        // hide the pending capture menu item, if there are no pending items
        if (SQLite.selectCountOf().from(PendingCapture.class).count() == 0 || SQLite.selectCountOf().from(PendingCapture.class).count() == 0) {
            getView().findViewById(R.id.nav_item_pending_captures).setVisibility(View.GONE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        // add the icons to the bar. We are creating the icons from some fonts, which has to be done

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        updateState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    //----------------------------------------------------------------------------------------------
    // Drawer Helpers

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(mFragmentContainerView);
    }


    //----------------------------------------------------------------------------------------------
    // General helpers

    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        Toolbar toolbar = getBaseActivity().getToolbar();
        toolbar.setTitle(R.string.app_name);
    }

    public void updateState() {
        View v = getView();
        final AQuery aQuery = new AQuery(v);

        /*
            Check if we need to show the menu items that are related to being signed in
         */
        boolean isSignedIn = getActivity() != null && ((BaseActivity) getActivity()).isSignedIn();
        for (int item : signedInItems) {
            if (isSignedIn) {
                aQuery.id(item).visible();
            } else {
                aQuery.id(item).gone();
            }
        }

        for (int item : signedOutItems) {
            if (isSignedIn) {
                aQuery.id(item).gone();
            } else {
                aQuery.id(item).visible();
            }
        }

        /*
            Update the drawer header with the profile, if it exists and the user is signed in.
         */
        final Profile profile = SQLite.select().from(Profile.class).querySingle();

        if (isSignedIn && profile != null && profile.user != null) {
            viewHandler.post(new Runnable() {
                @Override
                public void run() {
                    aQuery.id(R.id.nav_header_name).text(profile.firstName() + " " + profile.lastName());
                    aQuery.id(R.id.nav_header_email).text(profile.email());

                    final View navHeader = aQuery.id(R.id.nav_header).getView();
                    navHeader.setVisibility(View.VISIBLE);

                    aQuery.id(R.id.profile_image).longClicked(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            Toast.makeText(view.getContext(), "You can change your profile icon at https://gravatar.com/", Toast.LENGTH_SHORT).show();

                            return true;
                        }
                    });

                    aQuery.id(R.id.profile_image).image(profile.getGravatarUri().toString(), true, false, 0, 0, new BitmapAjaxCallback() {

                        @Override
                        public void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                            iv.setImageBitmap(bm);
                            Palette.from(bm).generate(new Palette.PaletteAsyncListener() {
                                @Override
                                public void onGenerated(Palette palette) {
                                    Palette.Swatch vibrantSwatch = palette.getLightVibrantSwatch();
                                    Palette.Swatch darkswatch = palette.getDarkMutedSwatch();

                                    List<Palette.Swatch> swatches = palette.getSwatches();

                                    /*
                                        we prefer the swatches in this order.
                                        getLightVibrantSwatch
                                        getDarkMutedSwatch

                                        then any non null swatch
                                     */
                                    if (vibrantSwatch != null) {
                                        navHeader.setBackgroundColor(vibrantSwatch.getRgb());
                                    } else if (darkswatch != null) {
                                        navHeader.setBackgroundColor(darkswatch.getRgb());
                                    } else  {
                                        for (Palette.Swatch swatch : swatches) {
                                            if (swatch !=  null) {
                                                navHeader.setBackgroundColor(swatch.getRgb());
                                                return;
                                            }
                                        }
                                    }


                                }
                            });
                        }

                    });
                }
            });
        }

    }

    //----------------------------------------------------------------------------------------------
    // Setup Helpers


    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        mDrawerToggle = new ActionBarDrawerToggle(getBaseActivity(),drawerLayout,getBaseActivity().getToolbar(),R.string.app_name,R.string.app_name);
//
//        // ActionBarDrawerToggle ties together the the proper interactions
//        // between the navigation drawer and the action bar app icon.
//        mDrawerToggle = new ActionBarDrawerToggle(
//                getActivity(),                    /* host Activity */
//                mDrawerLayout,                    /* DrawerLayout object */
//                getBaseActivity().getToolbar(),             /* nav drawer image to replace 'Up' caret */
//                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
//                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
//        ) {
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//                if (!isAdded()) {
//                    return;
//                }
//
//                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
//            }
//
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                if (!isAdded()) {
//                    return;
//                }
//
//                if (!mUserLearnedDrawer) {
//                    // The user manually opened the drawer; store this flag to prevent auto-showing
//                    // the navigation drawer automatically in the future.
//                    mUserLearnedDrawer = true;
//                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
//                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
//                }
//
//                showGlobalContextActionBar();
//
//                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
//            }
//        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
//        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
//            mDrawerLayout.openDrawer(mFragmentContainerView);
//        }

        mDrawerLayout.addDrawerListener(mDrawerToggle);

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    private void addIconsToNavItems(View view) {
        AQuery aQuery = new AQuery(view);

        aQuery.find(R.id.nav_item_home).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawerNavSubItemDrawable(FontAwesomeIcons.fa_home), null, null, null);
        aQuery.find(R.id.nav_item_pending_captures).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawerNavSubItemDrawable(FontAwesomeIcons.fa_clock_o), null, null, null);
        aQuery.find(R.id.nav_item_my_captures).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawerNavSubItemDrawable(FontAwesomeIcons.fa_list), null, null, null);
        aQuery.find(R.id.nav_item_enter_captures).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawerNavSubItemDrawable(FontAwesomeIcons.fa_edit), null, null, null);
        aQuery.find(R.id.nav_item_enter_time_on_water).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawerNavSubItemDrawable(FontAwesomeIcons.fa_anchor), null, null, null);
        aQuery.find(R.id.nav_item_recaptured_a_fish).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawerNavSubItemDrawable(FontAwesomeIcons.fa_tag), null, null, null);
        aQuery.find(R.id.nav_item_request_tags).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawerNavSubItemDrawable(FontAwesomeIcons.fa_tags), null, null, null);
        aQuery.find(R.id.nav_item_species_guide).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawerNavSubItemDrawable(FontAwesomeIcons.fa_book), null, null, null);

        // sub items
        aQuery.find(R.id.nav_item_sign_in).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawerNavSubItemDrawable(FontAwesomeIcons.fa_sign_in), null, null, null);
        aQuery.find(R.id.nav_item_sign_out).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawerNavSubItemDrawable(FontAwesomeIcons.fa_sign_out), null, null, null);
//        aQuery.find(R.id.nav_settings).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawerNavSubItemDrawable(Iconify.IconValue.fa_cog), null, null, null);
        aQuery.find(R.id.nav_contact_us).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawerNavSubItemDrawable(FontAwesomeIcons.fa_envelope), null, null, null);
    }

    private Drawable drawerNavSubItemDrawable(Icon iconValue) {
        return UiUtils.FontAwesomeItemDrawable(getContext(), iconValue, R.color.nav_sub_title, 15);
    }
}