package gov.louisiana.ldwf.taglouisiana.models.fishEvents;

import com.google.common.base.Joiner;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.database.JsonElementConverter;
import gov.louisiana.ldwf.taglouisiana.database.UUIDConverter;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.images.PendingCapturePhoto;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.images.PendingCapturePhoto_Table;
import gov.louisiana.ldwf.taglouisiana.models.options.RecaptureDispositionOption;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

/**
 * Created by danielward on 8/19/16.
 *
 */
@Table(database = AppDatabase.class)
public class PendingCapture extends AbstractFishEvent {
    @Expose
    @SerializedName("recapture")
    @Column
    public boolean isRecapture;

    @Expose
    @SerializedName("should_save")
    @Column
    public boolean shouldSave;

    @Expose
    @SerializedName("recapture_disposition_id")
    @Column
    @ForeignKey(tableClass = RecaptureDispositionOption.class)
    public long recaptureDispositionOptionId;

    @Expose
    @SerializedName("uuid")
    @Column(typeConverter = UUIDConverter.class)
    @Unique(onUniqueConflict = ConflictAction.REPLACE)
    public UUID uuid;

    @Expose(serialize = false)
    @SerializedName("status")
    @Column(typeConverter = JsonElementConverter.class)
    public JsonElement status;

    @Expose(serialize = false)
    @SerializedName("errors")
    @Column(typeConverter = JsonElementConverter.class)
    public JsonElement errorsJson;

    @Expose(serialize = false)
    @SerializedName("expires_at_epoch")
    public Date expiresAt;

    @Expose
    @SerializedName("images")
    List<PendingCapturePhoto> images;

    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "images")
    public List<PendingCapturePhoto> getImages() {
        if (images == null || images.isEmpty()) {
            images = SQLite.select()
                    .from(PendingCapturePhoto.class)
                    .where(PendingCapturePhoto_Table.fishEventId.eq(this._id))
                    .queryList();
        }

        if (images == null) {
            return new ArrayList<>();
        }

        return images;
    }

    public FontAwesomeIcons statusIcon() {
        switch (this.status.getAsJsonObject().getAsJsonPrimitive("icon").getAsString()) {
            case "fa-warning":
                return FontAwesomeIcons.fa_warning;
            case "fa-save":
                return FontAwesomeIcons.fa_save;
            case "fa-clock-o":
                return FontAwesomeIcons.fa_clock_o;
            default:
                return FontAwesomeIcons.fa_question_circle;
        }
    }

    public int statusColorResId() {
        switch (this.status.getAsJsonObject().getAsJsonPrimitive("icon").getAsString()) {
            case "fa-warning":
                return R.color.pending_capture_status_error;
            case "fa-save":
                return R.color.pending_capture_status_save;
            case "fa-clock-o":
                return R.color.pending_capture_status_waiting;
            default:
                return R.color.black;
        }
    }

    public String statusMessage() {
        Joiner joiner = Joiner.on("\n- ");
        String message = "";

        if (this.hasErrors()) {
            // convert the JsonArray to a list of Strings
            List<String> errors = new ArrayList<>();
            for (JsonElement element : this.errorsJson.getAsJsonObject().get("errors").getAsJsonArray()) {
                errors.add(element.getAsString());
            }
            // Create the message bulleted list
            message = "- " + joiner.join(errors);
        } else {
            /*
                expiresAt is null always and causes app to crash. So adding a check, but since the
                expiresAt is always null this if condition is never executed.
                Once the server has a way to send the value for expiresAt, it can be set and then if block will get executed.

                For now the message in this.status JsonElement is shown to the user.
             */
            if(this.expiresAt != null) {
                message = "This pending capture can be edited until: " + Constants.US_DATETIME_FORMATTER.format(this.expiresAt);
            } else{
                message = this.status.getAsJsonObject().get("message").getAsString();
            }
        }

        message = message + "\n\n*You can edit this pending capture on taglouisiana.com";

        return message;
    }

    public boolean hasErrors() {
        return this.errorsJson.getAsJsonObject().get("errors").getAsJsonArray().size() > 0;
    }
}
