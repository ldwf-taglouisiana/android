package gov.louisiana.ldwf.taglouisiana.models.fishEvents.images;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;

import gov.louisiana.ldwf.taglouisiana.models.RemoteItem;

/**
 * Created by danielward on 8/23/16.
 */
public abstract class AbstractFishEventPhoto extends RemoteItem {

    @Expose
    @Column
    @SerializedName("full")
    public String fullUrl;

    @Expose
    @Column
    @SerializedName("medium")
    public String mediumUrl;

    @Expose
    @Column
    @SerializedName("thumb")
    public String thumbUrl;

}
