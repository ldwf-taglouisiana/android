package gov.louisiana.ldwf.taglouisiana.ui.capture.create;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.AbstractFishEvent;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.EnteredPendingCapture;
import gov.louisiana.ldwf.taglouisiana.net.ApiServiceGenerator;
import gov.louisiana.ldwf.taglouisiana.net.PublicApiService;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages.AnglerInformationPage;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages.CommentsPage;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages.FishInfoPage;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages.ImagePage;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages.LocationInfoPage;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages.TagInfoPage;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages.ThankYouPage;
import gov.louisiana.ldwf.taglouisiana.ui.util.MaterialDialogBuilder;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Wizard;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;
import gov.louisiana.ldwf.taglouisiana.utils.EnterCapturePreferencesWrapper;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Some code used from https://gist.github.com/3405429
 *
 * @author daniel
 */
public class EnterCaptureWizard extends Wizard {

    private static final String PREFS = Constants.SHARED_PREFS_TAG_LOUISIANA_ENTER_CAPTURE;
    private static final String REPORTING_RECAPTURE_ARG = "EnterCaptureWizard.reporting_recapture";
    private static final String CURRENT_PAGE = "EnterCaptureWizard.current_page";
    private Handler handler;

    //------------------------------------------------------------------------------------------------
    // FRAGMENT METHOD OVERRIDES
    //------------------------------------------------------------------------------------------------

    public static EnterCaptureWizard newInstance() {
        return new EnterCaptureWizard();
    }

    public static EnterCaptureWizard newInstance(boolean isRecapture) {
        Bundle args = new Bundle();
        args.putBoolean(REPORTING_RECAPTURE_ARG, isRecapture);

        EnterCaptureWizard fragment = new EnterCaptureWizard();
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        this.setHasOptionsMenu(true);

        this.initPages();

        handler = new Handler(Looper.getMainLooper());

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
//            case R.id.save:
//                saveDraftCapture();
//                return true;
            // TODO add this back in when the overlay images are reallocated
//            case R.id.help:
//                ((EnterCapturePage) getCurrentPage()).showHelp();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        if (getBaseActivity().isSignedIn()) {
            inflater.inflate(R.menu.enter_capture_wizard, menu);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    //------------------------------------------------------------------------------------------------
    // INITIALIZERS
    //------------------------------------------------------------------------------------------------

    private void initPages() {
        new android.os.Handler().post(new Runnable() {
            public void run() {
                EnterCaptureWizard.this.addPages(
                        getTagInfoFragment(),
                        getFishInfoFragment(),
                        getLocationInfoFragment(),
                        getCommentsFragment(),
                        getImagePageFragment()
                );

                // if not signed in, then show the angler info page
                if (!getBaseActivity().isSignedIn()) {
                    EnterCaptureWizard.this.addPage(getAnglerInfoFragment());
                }

                if (isSubmittingRecaptureReport()) {
                    EnterCaptureWizard.this.setOnSubmitListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            EnterCaptureWizard.this.onSubmissionSelected();
                        }
                    });
                } else {
                    EnterCaptureWizard.this.addConfirmationPage(getThankYouFragment());
                }
            }
        });

    }

    //------------------------------------------------------------------------------------------------
    // WIZARD ABSTRACT METHOD IMPLEMENTATIONS
    //------------------------------------------------------------------------------------------------

    @Override
    public void onSubmissionSelected() {
        hideKeyboard(getCurrentPage());
        getCurrentPage().commitValues();
        if(getCurrentPage() instanceof AnglerInformationPage){
            if(! (getCurrentPage().isValid())){
                return;
            }
        }


        // If the user is not signed in and submitting recapture then hit the public route to
        // submit the recapture.
        // If the user is signed in then save the capture or recapture to the database
        // and it will be sent to the server when there is a sync.
        if(!getBaseActivity().isSignedIn() && isSubmittingRecaptureReport()){
            createPublicReportedRecapture();
        } else {
            createCapture();
        }
    }

    //------------------------------------------------------------------------------------------------
    // HELPER METHODS FOR BUILDING THE PAGES
    //------------------------------------------------------------------------------------------------

    private EnterCapturePage getTagInfoFragment() {
        TagInfoPage.Builder builder = new TagInfoPage.Builder();
        builder.setPrefs(PREFS);
        builder.setRecapture(isSubmittingRecaptureReport());

        return (EnterCapturePage) builder.create();
    }

    private EnterCapturePage getFishInfoFragment() {
        FishInfoPage.Builder builder = new FishInfoPage.Builder();
        builder.setPrefs(PREFS);
        builder.setRecapture(isSubmittingRecaptureReport());

        return (EnterCapturePage) builder.create();
    }

    private EnterCapturePage getLocationInfoFragment() {
        LocationInfoPage.Builder builder = new LocationInfoPage.Builder();
        builder.setPrefs(PREFS);

        return (EnterCapturePage) builder.create();
    }

    private EnterCapturePage getCommentsFragment() {
        CommentsPage.Builder builder = new CommentsPage.Builder();
        builder.setPrefs(PREFS);

        return (EnterCapturePage) builder.create();
    }

    private EnterCapturePage getImagePageFragment() {
        ImagePage.Builder builder = new ImagePage.Builder();
        builder.setPrefs(PREFS);

        return (EnterCapturePage) builder.create();
    }

    private EnterCapturePage getAnglerInfoFragment() {
        AnglerInformationPage.Builder builder = new AnglerInformationPage.Builder();
        builder.setPrefs(PREFS);

        return (EnterCapturePage) builder.create();
    }

    private EnterCapturePage getThankYouFragment() {
        ThankYouPage.Builder builder = new ThankYouPage.Builder();
        builder.setPrefs(PREFS);
        builder.setRestartListener(new ThankYouPage.RestartListener() {
            @Override
            public void onSelected() {
                getActivity().getSharedPreferences(PREFS, Context.MODE_PRIVATE)
                        .edit()
                        .clear()
                        .apply();

                setCurrentPage(0, true);
            }
        });

        return (EnterCapturePage) builder.create();
    }

    //------------------------------------------------------------------------------------------------
    // HELPERS TO CREATE THE MODELS
    //------------------------------------------------------------------------------------------------

    private boolean isSubmittingRecaptureReport() {
        return getArguments().getBoolean(REPORTING_RECAPTURE_ARG, false);
    }

    private void createPublicReportedRecapture(){
        EnterCapturePreferencesWrapper wrapper = new EnterCapturePreferencesWrapper(Constants.SHARED_PREFS_TAG_LOUISIANA_ENTER_CAPTURE, this.getActivity());

        Map<String, Object> body = new HashMap<>();
        body.put("angler", wrapper.getAngler());
        List<EnteredPendingCapture> list = new ArrayList<>(wrapper.getDraftCaptures());

        for (EnteredPendingCapture item : list) {
            item.isRecapture = isSubmittingRecaptureReport();
        }

        body.put("recapture", list.iterator().next());


        final MaterialDialogBuilder builder = new MaterialDialogBuilder(getContext());
        builder.setContent(R.layout.dialog_submitting_recapture);

        final Dialog dialog = builder.create();
        dialog.show();

        Call<ResponseBody> profileRespose = ApiServiceGenerator.createService(PublicApiService.class, getContext()).postReportedRecapture(body);
        profileRespose.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    dialog.dismiss();
                    showSuccessDialog();

                } else {
                    dialog.dismiss();
                    try{
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONArray errorMessagesArray = jObjError.getJSONArray("errors");
                        String errorMessage = getStringFromJsonArray(errorMessagesArray);
                        showErrorDialog(errorMessage);
                    } catch (IOException | JSONException e){
                        Log.e("EnterCaptureWizard ", "onResponse: ", e);
                        showErrorDialog("There was error submitting the capture. Please try again later. If the error persists please contact Louisiana Department of Wildlife Fisheries");
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utils.logError(call, t);
                Log.e("EnterCaptureWizard: ", "onFailure: ", t);
                showErrorDialog(t.getMessage());
            }
        });
    }

    public String getStringFromJsonArray(JSONArray array){
        StringBuilder builder = new StringBuilder();
        try {
            for (int i = 0; i < array.length(); i++) {
                builder.append((array.getString(i) + "\n"));
            }
        } catch (JSONException e){
            Log.e("EnterCaptureWizard", "getStringFromJsonArray: ",e );
            builder.append("There was an error. Please try again later. If the error persists contact Louisiana Department of Wildlife Fisheries");
        }

        return builder.toString();
    }


    private void showSuccessDialog() {
        MaterialDialogBuilder builder = new MaterialDialogBuilder(getContext());
        String body = builder.getContext().getString(R.string.thank_you_header) + "\n" + builder.getContext().getString(R.string.thank_you_subtext);


        builder.setTitle("Report Submitted").setContent(body);
        builder.setPositiveButton("DISMISS", new MaterialDialogBuilder.OnMaterialDialogItemClickListener() {
            @Override
            public void onClick(Dialog dialog) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        EnterCaptureWizard.this.resetPages();
                        setCurrentPage(0, false);
                    }
                });
                dialog.dismiss();
            }
        });
        (builder.create()).show();
    }

    private void showErrorDialog(String errorMessage) {
        MaterialDialogBuilder builder = new MaterialDialogBuilder(getContext());
        builder.setTitle("Error Submitting").setContent(errorMessage);
        builder.setPositiveButton("Dismiss", new MaterialDialogBuilder.OnMaterialDialogItemClickListener(){
            @Override
            public void onClick(Dialog dialog) {
                dialog.dismiss();
            }
        });
        (builder.create()).show();
    }


    private void createCapture() {
        EnterCapturePreferencesWrapper wrapper = new EnterCapturePreferencesWrapper(Constants.SHARED_PREFS_TAG_LOUISIANA_ENTER_CAPTURE, this.getActivity());
        final Set<EnteredPendingCapture> pendingCaptures = wrapper.getDraftCaptures();

        // if we are subnmitting a recapture, then mark the entries as recaptures
        for (EnteredPendingCapture item : pendingCaptures) {
            item.isRecapture = isSubmittingRecaptureReport();
        }

        commitItems(pendingCaptures);
        getActivity().finish();

    }

    private void commitItems(final Set<? extends AbstractFishEvent> items) {
        DatabaseDefinition database = FlowManager.getDatabase(AppDatabase.class);

        Transaction transaction = database.beginTransactionAsync(new ITransaction() {
            @Override
            public void execute(DatabaseWrapper databaseWrapper) {
                for (AbstractFishEvent item : items) {
                    item.save(databaseWrapper);
                }
            }
        }).success(new Transaction.Success() {
            @Override
            public void onSuccess(Transaction transaction) {
                getBaseActivity().requestSync();
            }
        }).build();

        transaction.execute();
    }

}
