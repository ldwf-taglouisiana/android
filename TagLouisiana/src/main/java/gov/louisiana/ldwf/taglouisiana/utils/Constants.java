package gov.louisiana.ldwf.taglouisiana.utils;

import com.google.android.gms.maps.model.LatLng;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Constants {

    // intent actions
    public static final String ACTION_STATS_UPDATING = "gov.louisiana.ldwf.taglouisiana.action.stats.updating";
    public static final String ACTION_STATS_UPDATED = "gov.louisiana.ldwf.taglouisiana.action.stats.updated";
    public static final String ACTION_PROFILE_UPDATED = "gov.louisiana.ldwf.taglouisiana.intent.meinfo";
    public static final String ACTION_SIGN_IN = "gov.louisiana.ldwf.taglouisiana.intent.signin";
    public static final String ACTION_SIGN_OUT = "gov.louisiana.ldwf.taglouisiana.intent.signout";
    public static final String ACTION_SYNC_FINISHED = "gov.louisiana.ldwf.taglouisiana.intent.sync.finsihed";
    public static final String ACTION_RECAPTURE_STATE_CHANGED = "gov.louisiana.ldwf.taglouisiana.intent.recapture.changed";

    // shared preferences
    public static final String SHARED_PREFS_TAG_LOUISIANA = "gov.louisiana.ldwf.taglouisiana";
    public static final String SHARED_PREFS_TAG_LOUISIANA_AUTH_TOKEN = "gov.louisiana.ldwf.taglouisiana.auth_token";
    public static final String SHARED_PREFS_TAG_LOUISIANA_DEVICE_ID = "gov.louisiana.ldwf.taglouisiana.device_id";
    public static final String SHARED_PREFS_TAG_LOUISIANA_REGISTRATION = "gov.louisiana.ldwf.taglouisiana.registration";

    public static final String LOG_TAG = "taglouisiana";

    // Constants for coach marks
    public static final String TAG_INFO_MARKS_SHOWN = "tag_info_marks_shown";
    public static final String FISH_INFO_MARKS_SHOWN = "fish_info_marks_shown";
    public static final String LOCATION_INFO_MARKS_SHOWN = "locations_info_marks_shown";

    // Constants for entering a capture
    public static final String SHARED_PREFS_TAG_LOUISIANA_ENTER_CAPTURE = "gov.louisiana.ldwf.taglouisiana.new.capture";
    public static final String ENTER_CAPTURE_TAG_NO = "tag_no";
    public static final String ENTER_CAPTURE_DATE = "date";
    public static final String ENTER_CAPTURE_TIME = "time";
    public static final String ENTER_CAPTURE_SPECIES = "species";
    public static final String ENTER_CAPTURE_DISPOSITION = "disposition";
    public static final String ENTER_CAPTURE_LENGTH = "length";
    public static final String ENTER_CAPTURE_MANUAL_LENGTH = "manual_length";
    public static final String ENTER_CAPTURE_LATITUDE = "latitude";
    public static final String ENTER_CAPTURE_LONGITUDE = "longitude";
    public static final String ENTER_CAPTURE_LOCATION_DESCRIPTION = "location description";
    public static final String ENTER_CAPTURE_COMMENTS = "comments";
    public static final String ENTER_CAPTURE_GPS_TYPE = "entered_gps_type";
    public static final String ENTER_CAPTURE_RECAPTURE = "recapture";
    public static final String ENTER_CAPTURE_IMAGES = "images";
    public static final String ENTER_CAPTURE_ANGLER_FIRST_NAME = "first_name";
    public static final String ENTER_CAPTURE_ANGLER_LAST_NAME = "last_name";
    public static final String ENTER_CAPTURE_ANGLER_EMAIL = "email";
    public static final String ENTER_CAPTURE_ANGLER_PHONE = "phone";

    // Keys for registration preferences
    public static final String REGISTER_FIRST_NAME = "first_name";
    public static final String REGISTER_LAST_NAME = "last_name";
    public static final String REGISTER_STREET = "street";
    public static final String REGISTER_SUITE = "suite";
    public static final String REGISTER_CITY = "city";
    public static final String REGISTER_STATE = "state";
    public static final String REGISTER_ZIP_CODE = "zip";
    public static final String REGISTER_PHONE_NUMBER = "phone_number";
    public static final String REGISTER_SHIRT_SIZE = "shirt_size";
    public static final String REGISTER_EMAIL = "email";
    public static final String REGISTER_PASSWORD = "password";
    public static final String REGISTER_PASSWORD_CONFIRMATION = "password_confirmation";

    // Common date formatter
    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
    public static final SimpleDateFormat US_DATE_FORMATTER = new SimpleDateFormat("MMM d, yyyy", Locale.US);
    public static final SimpleDateFormat US_DATETIME_FORMATTER = new SimpleDateFormat("MMM d, yyyy h:mm a", Locale.US);

    // Decimal Formatter
    private static final String DECIMAL_FORMAT = "#,###,###.##";
    private static final String DECIMAL_FORMAT_WHOLE = "#,###,###";
    public static final DecimalFormat DECIMAL_FORMATTER = new DecimalFormat(DECIMAL_FORMAT);
    public static final DecimalFormat DECIMAL_FORMATTER_WHOLE = new DecimalFormat(DECIMAL_FORMAT_WHOLE);

    // OAuth Token keys
    public static final String OAUTH_ACCESS_TOKEN_KEY = "access_token";
    public static final String OAUTH_REFRESH_TOKEN_KEY = "refresh_token";
    public static final String OAUTH_TOKEN_EXPIRES_ON_KEY = "expires_on";
    public static final String OAUTH_TOKEN_EXPIRES_IN_KEY = "expires_in";


    // set default latitude and longitude when there is no user location
    public static final double DEFAULT_LATITUDE = 29.723838893362693d;
    public static final double DEFAULT_LONGITUDE = -89.79330528527498d;

    // a default location to initialize the map too.
    public static final LatLng DEFAULT_LAT_LNG = new LatLng(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
    public static final EnteredLocation DEFAULT_ENTERED_LOCATION = new EnteredLocation(DEFAULT_LAT_LNG);
}
