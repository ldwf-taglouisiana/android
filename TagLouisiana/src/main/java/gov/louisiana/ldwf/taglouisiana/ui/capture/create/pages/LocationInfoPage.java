package gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import com.androidquery.AQuery;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.EnterCapturePage;
import gov.louisiana.ldwf.taglouisiana.ui.dialog.LocationDialogFragment;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Page;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;
import gov.louisiana.ldwf.taglouisiana.utils.EnteredLocation;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

public class LocationInfoPage extends EnterCapturePage implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int MY_LOCATION_REQUEST_CODE = Utils.getFreshInt();
    private EnteredLocation enteredLocation;
    private AQuery aq;
    private GoogleMap googleMap;
    private Marker currentMarker;
    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;

    private boolean hasSetLocation;

    public LocationInfoPage() {
        init();
    }

    private void init() {
        enteredLocation = Constants.DEFAULT_ENTERED_LOCATION;
        hasSetLocation = false;
    }

    // ---------------------------------------------------------------------------------------------
    // Fragment
    // ---------------------------------------------------------------------------------------------

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        return inflater.inflate(R.layout.enter_capture_location_info_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        initView(view);
    }

    @Override
    public void onStart() {
        super.onStart();

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        googleApiClient.connect();
    }


    @Override
    public void onStop() {
        super.onStop();

        if (googleApiClient.isConnected()) {
            // Disconnecting the client invalidates it.
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 && android.Manifest.permission.ACCESS_FINE_LOCATION.equals(permissions[0]) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                enableMapLocation();
            }
        } else {
            // Permission was denied. Display an error message.
        }
    }

    // ---------------------------------------------------------------------------------------------
    // EnterCapturePage
    // ---------------------------------------------------------------------------------------------


    @Override
    public void updateView() {
        updateDisplayLocation(this.enteredLocation);
    }

    @Override
    public void restoreValues() {
        SharedPreferences prefs = getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE);

        double latitude = Double.parseDouble(prefs.getString(Constants.ENTER_CAPTURE_LATITUDE, String.valueOf(Constants.DEFAULT_LATITUDE)));
        double longitude = Double.parseDouble(prefs.getString(Constants.ENTER_CAPTURE_LONGITUDE, String.valueOf(Constants.DEFAULT_LONGITUDE)));
        String enteredGPSType = prefs.getString(Constants.ENTER_CAPTURE_GPS_TYPE, EnteredLocation.DECIMAL_FORMAT);

        this.enteredLocation.setLatitude(latitude);
        this.enteredLocation.setLongitude(longitude);
        this.enteredLocation.setGPSType(enteredGPSType);
        this.hasSetLocation = true;
        updateDisplayLocation(enteredLocation);
    }

    @Override
    public void commitValues() {
        SharedPreferences prefs = this.getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(Constants.ENTER_CAPTURE_LATITUDE, String.valueOf(enteredLocation.getLatitude()));
        editor.putString(Constants.ENTER_CAPTURE_LONGITUDE, String.valueOf(enteredLocation.getLongitude()));
        editor.putString(Constants.ENTER_CAPTURE_GPS_TYPE, enteredLocation.getGPSType());

        editor.apply();
    }

    @Override
    public boolean isValid() {
        boolean result = true;

        Crouton.cancelAllCroutons();

        /*
            If the location is not valid, then show the error message
         */
        if (!hasSetLocation || !enteredLocation.isValid()) {
            result = false;
            Crouton.makeText(this.getActivity(), "Please select a valid location", Style.ALERT).show();
        }
        return result;
    }

    // ---------------------------------------------------------------------------------------------
    // OnMapReadyCallback
    // ---------------------------------------------------------------------------------------------

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;

        if (this.googleMap != null) {
            this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(this.enteredLocation.getLatLng(), 7));
            this.googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    hasSetLocation = true;
                    updateDisplayLocation(latLng);
                }
            });
            checkForLocationPermission();
        }
        restoreValues();
    }

    // ---------------------------------------------------------------------------------------------
    // GoogleApiClient.ConnectionCallbacks
    // ---------------------------------------------------------------------------------------------


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        checkForLocationPermission();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    // ---------------------------------------------------------------------------------------------
    // GoogleApiClient.OnConnectionFailedListener
    // ---------------------------------------------------------------------------------------------


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // left blank
    }

    // ---------------------------------------------------------------------------------------------
    // LocationListener
    // ---------------------------------------------------------------------------------------------

    @Override
    public void onLocationChanged(Location location) {
        if (enteredLocation.isDefault() && !hasSetLocation) {
            updateEnteredLocation(location);
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Page.Builder
    // ---------------------------------------------------------------------------------------------

    public static class Builder extends Page.Builder {

        @Override
        public Page create() {
            Page result = new LocationInfoPage();
            result.setSharedPrefs(this.getPrefs());

            return result;
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Helpers
    // ---------------------------------------------------------------------------------------------

    @TargetApi(23)
    private void checkForLocationPermission() {
        if(getContext() != null) {
            if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                enableMapLocation();
            } else {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_LOCATION_REQUEST_CODE);
            }
        }
    }

    private void enableMapLocation() {
        if (googleMap != null) {
            try {
                googleMap.setMyLocationEnabled(true);
            } catch (SecurityException e) {
                Utils.logError(e);
            }
        }

        if (googleApiClient.isConnected()) {
            try {

                // Get last known recent location.
                if (enteredLocation.isDefault()) {
                    Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    updateEnteredLocation(currentLocation);
                }

                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            } catch (SecurityException e) {
                Utils.logError(e);
            }

        }
    }

    private void initView(View view) {
        aq = new AQuery(getActivity(), view);

        aq.id(R.id.location_display).text(getResources().getString(R.string.no_locaton_text));
        aq.id(R.id.location_display_container).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLocationDialog();
            }
        });
        aq.id(R.id.layer_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(getActivity(), aq.id(R.id.layer_button).getView());
                popupMenu.getMenuInflater().inflate(R.menu.map_layers, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int kind = GoogleMap.MAP_TYPE_NORMAL;

                        switch (item.getItemId()) {
                            case R.id.satellite_layer:
                                kind = GoogleMap.MAP_TYPE_SATELLITE;
                                break;
                            case R.id.standard_layer:
                                kind = GoogleMap.MAP_TYPE_NORMAL;
                                break;
                            case R.id.terrain_layer:
                                kind = GoogleMap.MAP_TYPE_TERRAIN;
                                break;
                        }

                        if (googleMap != null) {
                            googleMap.setMapType(kind);
                        }

                        return true;
                    }
                });
                popupMenu.show();
            }
        });
    }


    private void openLocationDialog() {
        LocationDialogFragment.LocationDialogListener listener = new LocationDialogFragment.LocationDialogListener() {

            @Override
            public void onLocationDialogClick(EnteredLocation location) {
                LocationInfoPage.this.enteredLocation = location;
                LocationInfoPage.this.hasSetLocation = true;

                updateDisplayLocation(location);
            }
        };

        DialogFragment newFragment = new LocationDialogFragment(enteredLocation, listener);
        newFragment.show(LocationInfoPage.this.getFragmentManager(), "location_enter_fragment");
    }


    private void updateEnteredLocation(Location location) {
        if (location != null) {
            this.enteredLocation.setLatitude(location.getLatitude());
            this.enteredLocation.setLatitude(location.getLongitude());
            updateDisplayLocation(this.enteredLocation);
        }
    }

    private void updateDisplayLocation(EnteredLocation enteredLocation) {
        updateDisplayLocation(enteredLocation.getLatitude(), enteredLocation.getLongitude(), enteredLocation.getGPSType());
    }

    private void updateDisplayLocation(LatLng latLng) {
        updateDisplayLocation(latLng.latitude, latLng.longitude, enteredLocation.getGPSType());
    }

    private void updateDisplayLocation(LatLng latLng, String gpsType) {
        updateDisplayLocation(latLng.latitude, latLng.longitude, gpsType);
    }

    private void updateDisplayLocation(double latitude, double longitude) {
        updateDisplayLocation(latitude, longitude, enteredLocation.getGPSType());
    }

    private void updateDisplayLocation(double latitude, double longitude, String gpsType) {
        if (googleMap == null) {
            Utils.logError("Cannot set the location if the map has not been loaded yet.");
            return;
        }

        if (currentMarker != null) {
            currentMarker.remove();
        }

        LatLng latLng = new LatLng(latitude, longitude);

        currentMarker = googleMap.addMarker(new MarkerOptions().position(latLng));
        enteredLocation.setLatLng(latLng);
        enteredLocation.setGPSType(gpsType);

        aq.id(R.id.location_display).text(hasSetLocation ? enteredLocation.formattedToString() : "No Location Set");

        commitValues();
    }

}
