package gov.louisiana.ldwf.taglouisiana.accounts;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.runtime.FlowContentObserver;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.Nullable;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi.MeResponse;
import gov.louisiana.ldwf.taglouisiana.net.ApiServiceGenerator;
import gov.louisiana.ldwf.taglouisiana.net.ProtectedApiService;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;
import retrofit2.Call;

/**
 * Created by daniel on 8/29/13.
 *
 */
public class AuthenticatorActivity extends AccountAuthenticatorActivity {
    public final static String ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE";
    public final static String ARG_AUTH_TYPE = "AUTH_TYPE";
    public final static String ARG_AUTH_TOKEN_TYPE = "AUTH_TOKEN_TYPE";
    public final static String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";
    public final static String ARG_IS_ADDING_NEW_ACCOUNT = "IS_ADDING_ACCOUNT";
    public static final String KEY_ERROR_MESSAGE = "ERR_MSG";
    public final static String PARAM_USER_PASS = "USER_PASS";
    private static final String USER_DATA_BUNDLE = "USER_DATA";

    private final int REQ_SIGNUP = 1;

    private final String TAG = this.getClass().getSimpleName();

    private AccountManager mAccountManager;
    private String mAuthTokenType;

    public static void TriggerRefresh(Context context, Account account) {
        Bundle b = new Bundle();
        // Disable sync backoff and ignore sync preferences. In other words...perform sync NOW!
        b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(
                account,      // Sync account
                context.getResources().getString(R.string.common_content_authority), // Content authority
                b);                                      // Extras
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAccountManager = AccountManager.get(getBaseContext());

        mAuthTokenType = getIntent().getStringExtra(ARG_AUTH_TYPE);

        if (mAuthTokenType == null)
            mAuthTokenType = AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS;

        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Sign In");

        // set the edit text to a password type.
        ((EditText) findViewById(R.id.password)).setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }

    public void submit() {

        final String userName = ((EditText) findViewById(R.id.email)).getText().toString();
        final String userPass = ((EditText) findViewById(R.id.password)).getText().toString();

        final String accountType = getIntent().getStringExtra(ARG_ACCOUNT_TYPE) == null ? AccountGeneral.ACCOUNT_TYPE : getIntent().getStringExtra(ARG_ACCOUNT_TYPE);

        new AsyncTask<String, Void, Bundle>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                findViewById(R.id.progress_view).setVisibility(View.VISIBLE);
            }

            @Override
            protected Bundle doInBackground(String... params) {
                String authtoken;
                Bundle data = new Bundle();
                try {

                    Bundle result = ServerAuthenticate.userSignIn(AuthenticatorActivity.this, userName, userPass);
                    authtoken = result.getString(Constants.OAUTH_ACCESS_TOKEN_KEY);

                    data.putString(AccountManager.KEY_ACCOUNT_NAME, userName);
                    data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
                    data.putString(AccountManager.KEY_AUTHTOKEN, authtoken);
                    data.putString(PARAM_USER_PASS, userPass);

                    data.putBoolean(ARG_IS_ADDING_NEW_ACCOUNT, true);

                    data.putBundle(USER_DATA_BUNDLE, result);

                    finishLogin(data);

                    fetchProfileInformation();

                } catch (Exception e) {
                    Utils.logError("Login Error: ", e);
                    data.putString(KEY_ERROR_MESSAGE, e.getMessage());
                }

                return data;
            }

            @Override
            protected void onPostExecute(Bundle data) {
                String error = data.getString(KEY_ERROR_MESSAGE, "");

                if (!TextUtils.isEmpty(error)) {
                    Toast.makeText(getBaseContext(), error, Toast.LENGTH_SHORT).show();
                    findViewById(R.id.progress_view).setVisibility(View.GONE);
                } else {
                    finishAndReturn(data);
                }

            }

        }.execute();
    }

    private void finishAndReturn(Bundle data) {
        Intent intent = new Intent();
        intent.putExtras(data);
        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK, intent);
        finish();
    }

    private void fetchProfileInformation() throws IOException {
        Call<MeResponse> profileRespose = ApiServiceGenerator.createService(ProtectedApiService.class, this).getme();
        final MeResponse response = profileRespose.execute().body();

        // create a transaction to clean up the local data set.
        ITransaction transaction = new ITransaction() {
            @Override
            public void execute(DatabaseWrapper databaseWrapper) {
                // iterate through all the responses and remove all the processed items by UUID
                for(BaseModel item : response.getItems()) {
                    item.save(databaseWrapper);
                }
            }
        };

        FlowContentObserver observer = new FlowContentObserver();
        observer.beginTransaction();
        FlowManager.getDatabase(AppDatabase.class).executeTransaction(transaction);
        observer.endTransactionAndNotify();
    }

    private void finishLogin(final Bundle data) {

        final String accountName = data.getString(AccountManager.KEY_ACCOUNT_NAME);
        final String accountPassword = data.getString(PARAM_USER_PASS);

        final String authToken = data.getString(AccountManager.KEY_AUTHTOKEN);
        final Account account = new Account(accountName, data.getString(AccountManager.KEY_ACCOUNT_TYPE));

        final Bundle userBundle = data.getBundle(USER_DATA_BUNDLE);

        // we are using a try, since the app most likely has the permission we need
        try {

            /*
                Get the accounts, which match the default sync account
             */
            Collection<Account> defaultAccounts = Collections2.filter(
                    Arrays.asList(mAccountManager.getAccountsByType(data.getString(AccountManager.KEY_ACCOUNT_TYPE))),
                    new Predicate<Account>() {
                        @Override
                        public boolean apply(@Nullable Account input) {
                            return AuthenticatorService.DEFAULT_ACCOUNT_NAME.equals(input.name);
                        }
                    });

            /*
                There should only ever be one "default" account.
             */
            if (defaultAccounts.size() == 1) {
                Account defaultAccount = defaultAccounts.iterator().next();

                /*
                    Remove the default account, then add the new account.
                 */
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    mAccountManager.removeAccountExplicitly(defaultAccount);
                    addNewAccount(account, accountPassword, authToken, userBundle, data);
                } else {
                    mAccountManager.removeAccount(defaultAccount, new AccountManagerCallback<Boolean>() {
                        @Override
                        public void run(AccountManagerFuture<Boolean> future) {
                            addNewAccount(account, accountPassword, authToken, userBundle, data);
                        }
                    }, null);
                }

            } else {
                addNewAccount(account, accountPassword, authToken, userBundle, data);
            }

        } catch (SecurityException e) {
            Utils.logError(e);
        }
    }

    private void addNewAccount(final Account account, final String password, final String authToken, final Bundle userBundle, final Bundle data) {
        // Creating the account on the device and setting the auth token we got
        // (Not setting the auth token will cause another call to the server to authenticate the user)
        mAccountManager.addAccountExplicitly(account, password, userBundle);
        mAccountManager.setAuthToken(account, mAuthTokenType, authToken);

        // Inform the system that this account supports sync, the 1 denotes that it is syncable
        ContentResolver.setIsSyncable(account, getResources().getString(R.string.common_content_authority), 1);

        // Inform the system that this account is eligible for auto sync when the network is up
        ContentResolver.setSyncAutomatically(account, getResources().getString(R.string.common_content_authority), true);

        TriggerRefresh(this, account);
    }
}
