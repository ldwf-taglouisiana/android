package gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.raizlabs.android.dbflow.list.FlowQueryList;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.transaction.QueryTransaction;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.models.options.TimeOfDayOption;
import gov.louisiana.ldwf.taglouisiana.models.tag.Tag;
import gov.louisiana.ldwf.taglouisiana.models.tag.Tag_Table;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.EnterCapturePage;
import gov.louisiana.ldwf.taglouisiana.ui.dialog.TagListDialogFragment;
import gov.louisiana.ldwf.taglouisiana.ui.dialog.TimeOfDayDialogFragment;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Page;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

public class TagInfoPage extends EnterCapturePage{

    // constants
    public  static final String REPORTING_RECAPTURE_ARG = "TagInfoPage.reporting_recapture";
    private static final String FRAG_TAG_DATE_PICKER = "TagInfoPage.fragment.date_picker_tag";

    // loader management
    private List<Integer> doneLoaders;

    // form items
    private Calendar calendar;
    private Set<String> tagNumbers;
    private TimeOfDayOption timeOfDayOption;
    private boolean isRecapture;

    // the view helper instance
    private AQuery aq;

    // dialogs
    private FlowQueryList<Tag> taglist;

    // -----------------------------------------------------------------------------------------------
    // Initializers

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.isRecapture = false;
        this.tagNumbers = new LinkedHashSet<>();
        this.calendar = Calendar.getInstance();
    }


    // -----------------------------------------------------------------------------------------------
    // Overrides from Fragment

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.enter_capture_tag_info_fragment, container, false);
        aq = new AQuery(getActivity(), myFragmentView);

        if (getArguments() != null) {
            this.isRecapture = getArguments().getBoolean(REPORTING_RECAPTURE_ARG, false);
        }

        // Init the fields
        updateDate();

        // setup the click listeners
        aq.id(R.id.date_button).clicked(this, "dateButtonClicked");
        aq.id(R.id.time_button).clicked(this, "timeButtonClicked");
        aq.id(R.id.fish_tag_number).longClicked(this, "tagNumberLongClicked");

        // force the tagNumbers to be in all upper case
        aq.id(R.id.fish_tag_number).getEditText().setFilters(new InputFilter[] {new InputFilter.AllCaps()});

        this.setHasOptionsMenu(true);

        return myFragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
//        initCoachMarks(Constants.TAG_INFO_MARKS_SHOWN);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        /*
         * Had to put this in an if guard due to adding
         * the menu option everytime the screen gets unlocked.
         */
        if (menu.findItem(R.id.tag_list_menu_option) == null && getBaseActivity().isSignedIn()) {
            inflater.inflate(R.menu.enter_capture_taginfo, menu);

            MenuItem temp = menu.findItem(R.id.tag_list_menu_option);
            temp.setIcon(
                    new IconDrawable(this.getActivity(), FontAwesomeIcons.fa_tags)
                            .colorRes(R.color.white)
                            .actionBarSize()
            );

        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TimeOfDayDialogFragment.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                getModelByIdAsync(TimeOfDayOption.class, data.getLongExtra(TimeOfDayDialogFragment.INTENT_TIME_OF_DAY_ID, -1), timeOfDayOptionQueryResultSingleCallback);
            }
        }

        if (requestCode == TagListDialogFragment.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                long[] ids = data.getLongArrayExtra(TagListDialogFragment.INTENT_TAG_NUMBERS);

                SQLite.select()
                        .from(Tag.class)
                        .where(Tag_Table._id.in(-1, ids))
                        .async()
                        .queryListResultCallback(new QueryTransaction.QueryResultListCallback<Tag>() {
                            @Override
                            public void onListQueryResult(QueryTransaction transaction, @Nullable List<Tag> tags) {
                                tagNumbers.addAll(
                                        Collections2.transform(tags, new Function<Tag, String>() {
                                            @javax.annotation.Nullable
                                            @Override
                                            public String apply(@javax.annotation.Nullable Tag input) {
                                                return input.number;
                                            }
                                        })
                                );

                                updateView();
                            }
                        })
                        .execute();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.tag_list_menu_option) {
            openTagListDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // ===============================================================================================
    // Click callbacks

    // Callback for long pressing the number
    public void tagNumberLongClicked(View v) {
        if (getBaseActivity().isSignedIn()) {
            openTagListDialog();
        }
    }

    // Callback for the time button
    public void timeButtonClicked(View v) {
        openTimeOfDayDialog();
    }

    // Callback for the time button
    public void dateButtonClicked(View v) {
        CalendarDatePickerDialogFragment datePickerFragment = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(new CalendarDatePickerDialogFragment.OnDateSetListener() {
                    @Override
                    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
                        // update the calendar instance
                        calendar.set(year, monthOfYear, dayOfMonth, 12, 0, 0);
                        // update the displayed date
                        updateDate();
                    }
                })
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setPreselectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .setDateRange(null, new MonthAdapter.CalendarDay(Calendar.getInstance()))
                .setDoneText("SELECT")
                .setCancelText("CANCEL");

        datePickerFragment.show(getChildFragmentManager(), FRAG_TAG_DATE_PICKER);
    }

    // -----------------------------------------------------------------------------------------------
    // Helpers

    private void updateTagNumbers() {
        // if only one tag was selected
        if (tagNumbers.size() == 1) {
            String tag = tagNumbers.iterator().next();

            aq.id(R.id.fish_tag_number).visible();
            aq.id(R.id.multi_tag_selected_view).gone();
            aq.id(R.id.fish_tag_number).getEditText().setText(tag);

        }// end if
        else if (tagNumbers.size() > 1) {
            aq.id(R.id.fish_tag_number).gone();
            aq.id(R.id.multi_tag_selected_view).visible();
        }

    }

    private void updateTimeOfDay() {
        if (this.timeOfDayOption == null) {
            this.timeOfDayOption = TimeOfDayOption.getCurrentTime();
        }

        aq.id(R.id.time_button).text(this.timeOfDayOption.description.replaceAll("\\s\\(.*$", ""));
    }

    private void updateDate() {
        aq.id(R.id.date_button).text(Constants.US_DATE_FORMATTER.format(calendar.getTime()));
    }

    private void openTagListDialog() {
        FragmentManager fm = getChildFragmentManager();
        DialogFragment dialogFragment = TagListDialogFragment.newInstance(this, tagNumbers);
        dialogFragment.show(fm, "TagInfoPage.dialog.tags");
    }

    private void openTimeOfDayDialog() {
        FragmentManager fm = getChildFragmentManager();
        DialogFragment dialogFragment = TimeOfDayDialogFragment.newInstance(this);
        dialogFragment.show(fm, "TagInfoPage.dialog.time_of_day");
    }

    // -----------------------------------------------------------------------------------------------
    // Overrides from Page


    @Override
    public void onReset() {
        super.onReset();
        this.tagNumbers.clear();
        aq.id(R.id.fish_tag_number).text(null);
    }

    @Override
    public void updateView() {
        updateTagNumbers();
        updateDate();
        updateTimeOfDay();
    }

    @Override
    public void restoreValues() {
        SharedPreferences prefs = this.getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE);

        // get the items that are stored natively
        this.isRecapture = prefs.getBoolean(Constants.ENTER_CAPTURE_RECAPTURE, false);

        this.tagNumbers.clear();
        this.tagNumbers.addAll(prefs.getStringSet(Constants.ENTER_CAPTURE_TAG_NO, new TreeSet<String>()));

        // get the items that need to converted
        String dateString = prefs.getString(Constants.ENTER_CAPTURE_DATE, Constants.DATE_FORMATTER.format(this.calendar.getTime()));
        long timeOptionId = prefs.getLong(Constants.ENTER_CAPTURE_TIME, -1);

        // convert the stored items and set the variables to them
        getModelByIdAsync(TimeOfDayOption.class, timeOptionId, timeOfDayOptionQueryResultSingleCallback);

        try {
            this.calendar.setTime(Constants.DATE_FORMATTER.parse(dateString));
        } catch (ParseException e) {
            Utils.logError(e);
        }
    }

    @Override
    public void commitValues() {

        // if the suer typed in the tag number then add it to the set.
        if (!TextUtils.isEmpty(aq.id(R.id.fish_tag_number).getText())) {
            // clear any tags already entered
            this.tagNumbers.clear();
            // add the new entered tag nuumber
            this.tagNumbers.add(aq.id(R.id.fish_tag_number).getText().toString());
        }

        SharedPreferences prefs = this.getContext().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE);
        Editor editor = prefs.edit();

        editor.putStringSet(Constants.ENTER_CAPTURE_TAG_NO, this.tagNumbers);
        editor.putString(Constants.ENTER_CAPTURE_DATE, Constants.DATE_FORMATTER.format(this.calendar.getTime()));
        editor.putLong(Constants.ENTER_CAPTURE_TIME, this.timeOfDayOption._id);
        editor.putBoolean(Constants.ENTER_CAPTURE_RECAPTURE, this.isRecapture);

        editor.apply();
    }

    @Override
    public boolean isValid() {
        boolean result = true;

        // remove any error messages
        Crouton.cancelAllCroutons();

        // check that some tagNumbers were entered/selected
        Pattern pattern = Pattern.compile("^[A-Za-z]+[\\d\\-]{6,7}$");
        Matcher matcher = null;

        if (tagNumbers.size() == 0) {
            result = false;

            Crouton.makeText(this.getActivity(), "Please enter or select some tag numbers", Style.ALERT).show();

            aq.id(R.id.fish_tag_number).getEditText().requestFocus();
            addValidationBorder(aq.id(R.id.fish_tag_number).getEditText());
        } else{
            Iterator<String> iter = tagNumbers.iterator();
            String tagNumber = "";
            while(iter.hasNext() && result){
                tagNumber = iter.next();
                matcher = pattern.matcher(tagNumber);
                result = (result && matcher.matches());

                if(!result){
                    Crouton.makeText(this.getActivity(), "Please enter valid tag number", Style.ALERT).show();

                    aq.id(R.id.fish_tag_number).getEditText().requestFocus();
                    addValidationBorder(aq.id(R.id.fish_tag_number).getEditText());
                }
            }
        }

        return result;
    }

    private QueryTransaction.QueryResultSingleCallback<TimeOfDayOption> timeOfDayOptionQueryResultSingleCallback = new QueryTransaction.QueryResultSingleCallback<TimeOfDayOption>() {
        @Override
        public void onSingleQueryResult(QueryTransaction transaction, @Nullable TimeOfDayOption timeOfDayOption) {
            TagInfoPage.this.timeOfDayOption = timeOfDayOption;
            updateView();
        }
    };

    public static class Builder extends Page.Builder {

        private boolean isRecapture;

        @Override
        public Page create() {
            Page result = new TagInfoPage();
            result.setSharedPrefs(this.getPrefs());

            Bundle bundle = new Bundle();
            bundle.putBoolean(REPORTING_RECAPTURE_ARG, isRecapture);

            result.setArguments(bundle);

            return result;
        }

        public void setRecapture(boolean isRecapture) {
            this.isRecapture = isRecapture;
        }
    }


}
