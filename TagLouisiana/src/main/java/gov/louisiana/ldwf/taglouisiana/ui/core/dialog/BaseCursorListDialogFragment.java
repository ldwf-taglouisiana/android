package gov.louisiana.ldwf.taglouisiana.ui.core.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;

import java.util.HashSet;
import java.util.Set;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.adapters.MultipleSimpleCursorAdapter;

/**
 * Created by danielward on 11/10/16.
 */

public abstract class BaseCursorListDialogFragment extends BaseDialogFragment<Long> {

    private SimpleCursorAdapter listAdapter;
    ListView listView;

    @Override
    public View getContentView() {
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_list_search, null, false);

        listView = (ListView)view.findViewById(R.id.list);
        final SearchView searchView = (SearchView)view.findViewById(R.id.search);

        listAdapter = getCursorAdapter();
        final FilterQueryProvider filterQueryProvider = getCursorFilterQueryProvider();

        listView.setAdapter(listAdapter);

        if (this.isMultipleChoice()) {
            listView.setChoiceMode(ListView.CHOICE_MODE_NONE);

        } else {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long id) {
                    final long itemId = adapterView.getAdapter().getItemId(position);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, createResultIntent(itemId));
                    dismiss();
                }
            });
        }

        /*
            if the filterQueryProvider was implemented by a subclass then we need to configure the
            search view
         */
        if (filterQueryProvider != null) {
            listAdapter.setFilterQueryProvider(filterQueryProvider);
            searchView.setVisibility(View.VISIBLE);
            searchView.setIconified(false);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    ((SimpleCursorAdapter)listView.getAdapter()).getFilter().filter(newText);
                    return true;
                }
            });

            searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                @Override
                public boolean onClose() {
                    searchView.setIconified(false);
                    return true;
                }
            });
        }

        return view;
    }

    @Override
    public DialogInterface.OnClickListener getPositiveButtonAction() {
        if (isMultipleChoice()) {
            return new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Set<Long> ids = ((MultipleSimpleCursorAdapter)listView.getAdapter()).getSelectedItems();
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, createMultipleResultIntent(ids));
                    dialogInterface.dismiss();
                }
            };
        } else {
            return super.getPositiveButtonAction();
        }
    }

    public Intent createMultipleResultIntent(Set<Long> ids) {
        return null;
    }

    public abstract Cursor getItemListCursor();

    public FilterQueryProvider getCursorFilterQueryProvider() { return null; }

    public abstract SimpleCursorAdapter getCursorAdapter();

    public SimpleCursorAdapter getCursorAdapter(String[] projections) {
        return this.getCursorAdapter(projections, new int[] { android.R.id.text1 });
    }

    public SimpleCursorAdapter getCursorAdapter(String[] projections, int[] fieldTargets) {

        if (isMultipleChoice()) {
            return new MultipleSimpleCursorAdapter(
                    getContext(),
                    getItemListCursor(),
                    projections,
                    getCheckedItemIds()
            );
        } else {
            return new SimpleCursorAdapter(
                    getActivity(),
                    android.R.layout.simple_list_item_1,
                    getItemListCursor(),
                    projections,
                    fieldTargets,
                    0);
        }
    }

    public Set<Long> getCheckedItemIds() {
        return new HashSet<>();
    }
}
