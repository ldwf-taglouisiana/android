package gov.louisiana.ldwf.taglouisiana.ui.core;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;

import gov.louisiana.ldwf.taglouisiana.R;

/**
 * Created with IntelliJ IDEA.
 * User: danielward
 * Date: 2/21/13
 * Time: 3:16 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseRefreshableFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private BaseAdapter adapter;
    private AbsListView adapterView;

    protected abstract View setContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base_refreshable, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);

        swipeRefreshLayout.addView(setContentView(inflater, swipeRefreshLayout, savedInstanceState));

        initSwipeOptions();
        setContentView(view);

        return swipeRefreshLayout;
    }

    //----------------------------------------------------------------------------------------------
    // Adapter Views and Adapters

    protected BaseAdapter getAdapter() {
        return this.adapter;
    }

    protected void setAdapter(BaseAdapter adapter) {
        this.adapter = adapter;

        if (this.adapterView != null) {
            adapterView.setAdapter(this.adapter);
        }
    }

    protected AbsListView getAdapterView() {
        return this.adapterView;
    }

    protected void setAdapterView(AbsListView view) {
        this.adapterView = view;

        if (this.adapter != null) {
            adapterView.setAdapter(this.adapter);
        }

        adapterView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    if (isViewAtTop(adapterView)) {
                        enableSwipe();
                    } else {
                        disableSwipe();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    enableSwipe();
                } else {
                    disableSwipe();
                }
            }
        });
    }

    //----------------------------------------------------------------------------------------------
    // SwipeRefreshLayout

    private void initSwipeOptions() {
        swipeRefreshLayout.setOnRefreshListener(this);
        setAppearance();
        swipeRefreshLayout.setEnabled(true);
    }

    private void setAppearance() {
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    protected void showSwipeProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    /**
     * It shows the SwipeRefreshLayout progress
     */
    protected void hideSwipeProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Enables swipe gesture
     */
    protected void enableSwipe() {
        swipeRefreshLayout.setEnabled(true);
    }

    /**
     * Disables swipe gesture. It prevents manual gestures but keeps the option tu show
     * refreshing programatically.
     */
    protected void disableSwipe() {
        swipeRefreshLayout.setEnabled(false);
    }

    public void onStartRefresh() {
        showSwipeProgress();
    }

    public void onRefresh() {
        showSwipeProgress();
        onRefreshStarted();
    }

    protected abstract void onRefreshStarted();

    protected void onDataLoaded() {
        View progressView = getContentView().findViewById(R.id.progress_view);
        View contentView = getContentView().findViewById(R.id.content_view);

        if (progressView != null && contentView != null && progressView.getVisibility() == View.VISIBLE) {
            crossfade(contentView, progressView);
        }
    }

    protected void onRefreshFinished() {
        onDataLoaded();
        hideSwipeProgress();
    }

    //----------------------------------------------------------------------------------------------
    // Helpers


    private boolean isViewAtTop(AbsListView view) {
        if (view.getChildCount() == 0) return true;
        return view.getChildAt(0).getTop() == 0;
    }


    private void crossfade(final View targetView, final View OldView) {
        int fadeDuration = getResources().getInteger(android.R.integer.config_mediumAnimTime);

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        targetView.setAlpha(0f);
        targetView.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        targetView.animate()
                .alpha(1f)
                .setDuration(fadeDuration)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        OldView.animate()
                .alpha(0f)
                .setDuration(fadeDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        OldView.setVisibility(View.GONE);
                    }
                });
    }
}
