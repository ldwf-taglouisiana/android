package gov.louisiana.ldwf.taglouisiana.accounts;

import android.support.annotation.NonNull;

/**
 * Created by danielward on 8/25/16.
 */
public class OAuthClientInfo {
    private String clientId;
    private String clientSecret;

    public OAuthClientInfo(@NonNull  String clientId, @NonNull  String clientSecret) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }
}
