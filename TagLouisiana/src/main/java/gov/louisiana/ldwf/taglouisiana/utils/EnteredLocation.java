package gov.louisiana.ldwf.taglouisiana.utils;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public class EnteredLocation {

  public final static String DECIMAL_FORMAT = "D";
  public final static String DEGREE_MINUTE_FORMAT = "DM";
  public final static String DEGREE_MINUTE_SECOND_FORMAT = "DMS";


  private String type;
  private double latitude;
  private double longitude;

  public EnteredLocation(LatLng latLng) {
    this(latLng.latitude, latLng.longitude);
  }

  public EnteredLocation(double latitude, double longitude) {
    this.latitude = latitude;
    this.longitude = longitude;

    this.setGPSTypeD();
  }

  public boolean isDefault() {
      float[]  results = new float[1];
      Location.distanceBetween(this.latitude, this.longitude, Constants.DEFAULT_LATITUDE, Constants.DEFAULT_LONGITUDE, results);

      // if the distance is less than 20 meters
    return results[0] < 20.0f;
  }

  public boolean isValid() {
    return this.getLatitude() > 0 && this.getLongitude() < 0;
  }

  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  public LatLng getLatLng() {
    return new LatLng(this.latitude, this.longitude);
  }

  public void setLatLng(LatLng latLng) {
    setLatitude(latLng.latitude);
    setLongitude(latLng.longitude);
  }

  public String getGPSType() {
    return type;
  }

  public void setGPSType(String s) {
    this.type = s;
  }

  public void setGPSTypeD() {
    type = DECIMAL_FORMAT;
  }

  public void setGPSTypeDM() {
    type = DEGREE_MINUTE_FORMAT;
  }

  public void setGPSTypeDMS() {
    type = DEGREE_MINUTE_SECOND_FORMAT;
  }

  public EnteredLocation copy() {
    EnteredLocation result = new EnteredLocation(this.getLatitude(), this.getLongitude());
    result.setGPSType(this.getGPSType());
    return result;
  }

  public String formattedToString() {
    return GeoUtil.formatEnteredLocationString(this);
  }

  @Override
  public String toString() {
    return "EnteredLocation{" +
            "type='" + type + '\'' +
            ", latitude=" + latitude +
            ", longitude=" + longitude +
            '}';
  }
}