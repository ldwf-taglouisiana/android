package gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.EnterCapturePage;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Page;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

public class CommentsPage extends EnterCapturePage {

  private EditText comments;
  private EditText locationDescription;

  public CommentsPage() {
    // left blank
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {

    View myFragmentView = inflater.inflate(R.layout.enter_capture_comments_fragment, container, false);
    locationDescription = (EditText) myFragmentView.findViewById(R.id.enter_capture_location_description);
    comments = (EditText) myFragmentView.findViewById(R.id.enter_capture_comments);

    return myFragmentView;
  }

  @Override
  public void updateView() {
    SharedPreferences prefs = this.getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE);
    this.comments.setText(prefs.getString(Constants.ENTER_CAPTURE_COMMENTS, null));
    this.locationDescription.setText(prefs.getString(Constants.ENTER_CAPTURE_LOCATION_DESCRIPTION, null));
  }

  @Override
  public void restoreValues() {
    SharedPreferences prefs = this.getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE);

    locationDescription.setText(prefs.getString(Constants.ENTER_CAPTURE_LOCATION_DESCRIPTION, null));
    comments.setText(prefs.getString(Constants.ENTER_CAPTURE_COMMENTS, null));
  }

  @Override
  public void commitValues() {
    SharedPreferences prefs = this.getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE);
    Editor editor = prefs.edit();
    editor.putString(Constants.ENTER_CAPTURE_COMMENTS, this.comments.getText().toString());
    editor.putString(Constants.ENTER_CAPTURE_LOCATION_DESCRIPTION, this.locationDescription.getText().toString());
    editor.apply();
  }

  public static class Builder extends Page.Builder {

    @Override
    public Page create() {
      Page result = new CommentsPage();
      result.setSharedPrefs(this.getPrefs());

      return result;
    }
  }
}
