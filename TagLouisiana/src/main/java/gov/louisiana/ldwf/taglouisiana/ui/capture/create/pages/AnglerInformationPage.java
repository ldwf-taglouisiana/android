package gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.androidquery.AQuery;
import com.google.common.base.Joiner;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.models.profile.Angler;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.EnterCapturePage;
import gov.louisiana.ldwf.taglouisiana.ui.util.UsPhoneNumberFormatter;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Page;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

public class AnglerInformationPage extends EnterCapturePage {

    private AQuery aq;
    private Angler angler;


    public AnglerInformationPage() {
        // left blank
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View myFragmentView = inflater.inflate(R.layout.enter_capture_angler_info_fragment, container, false);
        aq = new AQuery(myFragmentView);

        EditText phoneEditText = aq.id(R.id.phone).getEditText();
        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(new WeakReference<>(phoneEditText));
        phoneEditText.addTextChangedListener(addLineNumberFormatter);

        angler = new Angler();

        return myFragmentView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        restoreValues();

    }

    @Override
    public boolean isValid() {
        boolean result = true;

        // clear all the errors
        aq.id(R.id.first_name).getEditText().setError(null);
        aq.id(R.id.last_name).getEditText().setError(null);
        aq.id(R.id.email).getEditText().setError(null);
        aq.id(R.id.phone).getEditText().setError(null);

        // check that the first_name is blank
        if (aq.id(R.id.first_name).getEditText().getText().toString().trim().equals("")) {
            aq.id(R.id.first_name).getEditText().setError("• Cannot be blank");
            result = false;
        }

        // check if the last_name is blank
        if (aq.id(R.id.last_name).getEditText().getText().toString().trim().equals("")) {
            aq.id(R.id.last_name).getEditText().setError("• Cannot be blank");
            result = false;
        }

        // check if the email is blank
        if (aq.id(R.id.email).getEditText().getText().toString().trim().equals("")) {
            aq.id(R.id.email).getEditText().setError("• Cannot be blank");
            result = false;
        }

        // check if the email is formatted correctly
        if (!isEmailValid(aq.id(R.id.email).getEditText().getText().toString().trim())) {
            List<String> errors;
            if (aq.id(R.id.email).getEditText().getError() != null) {
                errors = errorStringToList(aq.id(R.id.email).getEditText().getError().toString());
            } else {
                errors = new LinkedList<>();
            }
            errors.add("• Email is not valid");

            Joiner joiner = Joiner.on("\n");

            aq.id(R.id.email).getEditText().setError(joiner.skipNulls().join(errors));

            result = false;
        }

        // check if the phone is blank
        if (aq.id(R.id.phone).getEditText().getText().toString().trim().equals("")) {
            aq.id(R.id.phone).getEditText().setError("• Cannot be blank");
            result = false;
        }

        // check if the phone is too short
        if (aq.id(R.id.phone).getEditText().getText().toString().replaceAll("[^0-9]", "").length() != 11) {
            aq.id(R.id.phone).getEditText().setError("• Phone number not long enough");
            result = false;
        }


        return result;
    }

    @Override
    public void updateView() {
        aq.id(R.id.first_name).getEditText().setText(angler.firstName);
        aq.id(R.id.last_name).getEditText().setText(angler.lastName);
        aq.id(R.id.email).getEditText().setText(angler.email);
        aq.id(R.id.phone).getEditText().setText(angler.mobileNumber);

    }

    @Override
    public void restoreValues() {
        SharedPreferences prefs = this.getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE);

        angler.firstName = prefs.getString(Constants.ENTER_CAPTURE_ANGLER_FIRST_NAME, null);
        angler.lastName = prefs.getString(Constants.ENTER_CAPTURE_ANGLER_LAST_NAME, null);
        angler.email = prefs.getString(Constants.ENTER_CAPTURE_ANGLER_EMAIL, null);
        angler.mobileNumber = prefs.getString(Constants.ENTER_CAPTURE_ANGLER_PHONE, null);
    }

    /*
        Click Listeners
     */

    @Override
    public void commitValues() {
        SharedPreferences prefs = this.getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE);
        Editor editor = prefs.edit();

        editor.putString(Constants.ENTER_CAPTURE_ANGLER_FIRST_NAME, aq.id(R.id.first_name).getEditText().getText().toString());
        editor.putString(Constants.ENTER_CAPTURE_ANGLER_LAST_NAME, aq.id(R.id.last_name).getEditText().getText().toString());
        editor.putString(Constants.ENTER_CAPTURE_ANGLER_EMAIL, aq.id(R.id.email).getEditText().getText().toString());
        editor.putString(Constants.ENTER_CAPTURE_ANGLER_PHONE, aq.id(R.id.phone).getEditText().getText().toString());

        editor.apply();
    }


    private boolean isEmailValid(String email) {
        Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        return matcher.find();
    }

    private List<String> errorStringToList(String errorString) {
        List<String> errors = new LinkedList<>();

        if (errorString != null) {
            String[] parts = errorString.split("\n");

            for (String s : parts) {
                errors.add(s.trim());
            }
        }

        return errors;
    }

    public static class Builder extends Page.Builder {

        @Override
        public Page create() {
            Page result = new AnglerInformationPage();
            result.setSharedPrefs(this.getPrefs());

            return result;
        }
    }
}
