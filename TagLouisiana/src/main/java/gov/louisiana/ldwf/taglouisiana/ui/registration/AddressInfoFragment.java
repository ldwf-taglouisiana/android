package gov.louisiana.ldwf.taglouisiana.ui.registration;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Page;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddressInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddressInfoFragment extends Page {

  public AddressInfoFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   * <p/>
   * //@param param1 Parameter 1.
   * //@param param2 Parameter 2.
   *
   * @return A new instance of fragment AddressInfoFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static AddressInfoFragment newInstance() {
    AddressInfoFragment fragment = new AddressInfoFragment();
    return fragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_address_info, container, false);
  }

  @Override
  public void updateView() {

  }

  @Override
  public void restoreValues() {

  }

  @Override
  public void commitValues() {
    EditText street = (EditText) getView().findViewById(R.id.street);
    EditText suite = (EditText) getView().findViewById(R.id.suite);
    EditText zipcode = (EditText) getView().findViewById(R.id.zipcode);

    SharedPreferences.Editor editor = getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE).edit();

    editor.putString(Constants.REGISTER_STREET, street.getText().toString());
    editor.putString(Constants.REGISTER_SUITE, suite.getText().toString());
    editor.putString(Constants.REGISTER_ZIP_CODE, zipcode.getText().toString());

    editor.putString(Constants.REGISTER_SHIRT_SIZE, getShirtSizeValue());

    editor.commit();
  }

  private String getShirtSizeValue() {
    Spinner shirt = (Spinner) getView().findViewById(R.id.shirt_size);

    String[] options = getResources().getStringArray(R.array.shirt_sizes);

    return options[shirt.getSelectedItemPosition()];
  }

  public static class Builder extends Page.Builder {

    @Override
    public Page create() {
      Page result = new AddressInfoFragment();
      result.setSharedPrefs(this.getPrefs());

      return result;
    }
  }

}
