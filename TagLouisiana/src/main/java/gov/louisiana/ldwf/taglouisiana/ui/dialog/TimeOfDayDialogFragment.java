package gov.louisiana.ldwf.taglouisiana.ui.dialog;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.SimpleCursorAdapter;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import gov.louisiana.ldwf.taglouisiana.models.options.TimeOfDayOption;
import gov.louisiana.ldwf.taglouisiana.ui.core.dialog.BaseCursorListDialogFragment;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

public class TimeOfDayDialogFragment extends BaseCursorListDialogFragment {


    public static final int REQUEST_CODE = Utils.getFreshInt();
    public static final String INTENT_TIME_OF_DAY_ID = "TimeOfDayDialogFragment.intent.time_of_day.id";

    public static TimeOfDayDialogFragment newInstance(Fragment targetFragment) {
        Bundle args = new Bundle();

        TimeOfDayDialogFragment fragment = new TimeOfDayDialogFragment();
        fragment.setTargetFragment(targetFragment, REQUEST_CODE);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public String getTitle() {
        return "Choose Time Of Day";
    }

    @Override
    public Intent createResultIntent(Long item) {
        Intent intent = new Intent();
        intent.putExtra(INTENT_TIME_OF_DAY_ID, item);
        return intent;
    }

    @Override
    public Cursor getItemListCursor() {
        return SQLite.select().from(TimeOfDayOption.class).flowQueryList().cursor();
    }

    @Override
    public SimpleCursorAdapter getCursorAdapter() {
        return getCursorAdapter(new String[]{ "description" }, new int[] { android.R.id.text1 });
    }
}