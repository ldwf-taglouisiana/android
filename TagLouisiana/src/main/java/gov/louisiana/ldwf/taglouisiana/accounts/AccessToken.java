package gov.louisiana.ldwf.taglouisiana.accounts;

/**
 * Created by danielward on 8/25/16.
 */
public class AccessToken {
    private String tokenType;
    private String accesstoken;

    public AccessToken(String tokenType, String accesstoken) {
        this.tokenType = tokenType;
        this.accesstoken = accesstoken;
    }

    public AccessToken(String accesstoken) {
        this.accesstoken = accesstoken;
        this.tokenType = "Bearer";
    }

    public String getTokenType() {
        return tokenType;
    }

    public String getAccesstoken() {
        return accesstoken;
    }
}
