package gov.louisiana.ldwf.taglouisiana.ui.draftCapture;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.models.fishEvents.EnteredPendingCapture;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;

/**
 *
 */
public class PendingCaptureTabFragment extends BaseFragment {

    private PagerAdapter pagerAdapter;
    List<BaseFragment> fragments;

    public static PendingCaptureTabFragment newInstance() {
        Bundle args = new Bundle();

        PendingCaptureTabFragment fragment = new PendingCaptureTabFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public PendingCaptureTabFragment() {
        // Required empty public constructor
        this.setHasTabs();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragments = new ArrayList<>(2);
        pagerAdapter = new PagerAdapter(this.getChildFragmentManager(), fragments);
        fragments.add(PendingCaptureListFragment.newInstance());

        if(SQLite.selectCountOf().from(EnteredPendingCapture.class).count() > 0) {
            fragments.add(EnteredPendingCaptureListFragment.newInstance());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stats, container, false);

        ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);

        getBaseActivity().setTabViewPager(pager);

        if (fragments.size() == 1) {
            getBaseActivity().hideTabs();
        }
        return view;
    }

    @Override
    public String getTitle() {
        return "Pending Captures";
    }

    //----------------------------------------------------------------------------------------------
    // PAGER ADAPTER

    private class PagerAdapter extends FragmentStatePagerAdapter {

        private List<BaseFragment> fragments;

        public PagerAdapter(FragmentManager fm, List<BaseFragment> fragments) {
            super(fm);

            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragments.get(position).getTitle();
        }
    } // end pager adapter
}
