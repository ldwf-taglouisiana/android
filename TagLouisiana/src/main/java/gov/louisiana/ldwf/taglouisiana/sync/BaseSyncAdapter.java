package gov.louisiana.ldwf.taglouisiana.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.runtime.FlowContentObserver;
import com.raizlabs.android.dbflow.sql.language.OrderBy;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Nullable;

import gov.louisiana.ldwf.taglouisiana.accounts.AuthenticatorService;
import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.RemoteItem;
/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/29/14
 */
public abstract class BaseSyncAdapter extends AbstractThreadedSyncAdapter {

    protected final AccountManager mAccountManager;

    public BaseSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mAccountManager = AccountManager.get(context);
    }

    protected boolean isSignedIn(Account account) {
        return !account.name.equals(AuthenticatorService.DEFAULT_ACCOUNT_NAME);
    }

    // ---------------------------------------------------------------------------------------------
    // HELPERS
    // ---------------------------------------------------------------------------------------------

    /**
     * Used to help prepare the <tt>includes</tt> parameter for the API calls to fetch data.
     * Can be used to get all the data in a given call or a subset of the data depending
     * on the needs of the request.
     *
     * @param bundle The {@link android.os.Bundle} object that contains the syncing params. Must be from the <tt>String</tt> keys in the <tt>array</tt> param.
     * @param array The array of keys to check against the <tt>bundle</tt> param.
     * @return The {@link Collection} that will contain the <tt>includes</tt> param for the API request
     */
    protected Collection<String> prepareIncludesParams(@NonNull Bundle bundle, @NonNull String[] array) {
        Collection<String> includes = new ArrayList<>(array.length);

        /*
            go through all the public keys that are available and see if the item has
            been passed in
         */
        for (String key : array) {
            // if the specific data key is in the extras, then add the item to the includes
            if (bundle.getBoolean(key, false)) {
                includes.add(key.replace("_data", ""));
            }
        }

        /*
            If there were no specific items specified, then we will include all
            the public data.
        */
        if (includes.isEmpty()) {
            /*
                Transform all the key values so that they have the correct value needed by the API
            */
            includes = Collections2.transform(Arrays.asList(array), new Function<String, String>() {
                @Nullable
                @Override
                public String apply(@Nullable String input) {
                    return input == null ? null : input.replace("_data", "");
                }
            });
        }

        // remove any nulls that might have crept into the includes list
        includes = Collections2.filter(includes, new Predicate<String>() {
            @Override
            public boolean apply(@Nullable String input) {
                return input != null;
            }
        });

        return includes;
    }

    /**
     * Builds a params set with give models and their keys in the resulting {@link Map}.
     * Will only contain the max <tt>updated_at</tt> for the model in the result set.
     *
     * @param items
     * @return
     */
    protected Map<String, Object> buildUpdatedAfterParams(@NonNull Map<String, Class<? extends RemoteItem>> items) {
        Map<String, Object> results = new LinkedHashMap<>();

        for (Map.Entry<String, Class<? extends RemoteItem>> entry : items.entrySet()) {
            Long versionValue = getVersionParamValue(entry.getValue());

            if (versionValue != null) {
                Map<String, Object> params = new LinkedHashMap<>();
                params.put("updated_after", versionValue / 1000);
                results.put(entry.getKey(), params);
            }
        }

        return results;
    }


    /**
     * Builds a params set with given models and their keys in the resulting {@link Map}.
     * Will only contain the <tt>only_id_list</tt> for the model in the result set.
     *
     * @param items
     * @return
     */
    protected Map<String, Object> buildOnlyIdListParams(@NonNull Map<String, Class<? extends RemoteItem>> items) {
        Map<String, Object> results = new LinkedHashMap<>();

        for (Map.Entry<String, Class<? extends RemoteItem>> entry : items.entrySet()) {
            Map<String, Object> params = new LinkedHashMap<>();
            params.put("only_id_list", true);
            results.put(entry.getKey(), params);

        }

        return results;
    }



    /**
     * Finds the max <tt>updated_at</tt> for the passed in model class. Returns the values as an epoch value.
     *
     * @param model The model to get the version parameter for.
     * @return The max <tt>updated_at</tt> for the given model, in epoch format.
     */
    protected Long getVersionParamValue(@NonNull Class<? extends RemoteItem> model) {
        RemoteItem item = SQLite.select()
                .from(model)
                .orderBy(OrderBy.fromString("updatedAt DESC"))
                .limit(1)
                .querySingle();

        if (item != null && item.updatedAt != null) {
            return item.updatedAt.getTime();
        } else {
            return null;
        }
    }


    /**
     * Will insert a set of {@link RemoteItem} models into the database, as well as update the passed in {@link SyncResult} object.
     *
     * @param items The {@link Collection} of @{link RemoteItem} models to insert into the database.
     * @param syncResult The {@link SyncResult} to be passed. The <tt>stats</tt> will be updated with the number of inserts
     * @param <T> {@link RemoteItem} type that will be used for {@link Collection} parameter.
     */
    protected  <T extends BaseModel> void insertItems(@NonNull final Collection<T> items, @NonNull final SyncResult syncResult) {
        // allows for the notification when bulk items are finished, not for each item
        FlowContentObserver observer = new FlowContentObserver();
        observer.beginTransaction();

        // run a transaction synchronous easily.
        DatabaseDefinition database = FlowManager.getDatabase(AppDatabase.class);
        database.executeTransaction(new ITransaction() {
            @Override
            public void execute(DatabaseWrapper databaseWrapper) {
                for (T item : items) {
                    item.save(databaseWrapper);
                    syncResult.stats.numInserts++;
                }

            }
        });

        // notify that the transaction has ended
        observer.endTransactionAndNotify();
    }

    protected  <T extends BaseModel> void deleteItems(@NonNull final Collection<T> items, @NonNull final SyncResult syncResult) {
        // allows for the notification when bulk items are finished, not for each item
        FlowContentObserver observer = new FlowContentObserver();
        observer.beginTransaction();

        // run a transaction synchronous easily.
        DatabaseDefinition database = FlowManager.getDatabase(AppDatabase.class);
        database.executeTransaction(new ITransaction() {
            @Override
            public void execute(DatabaseWrapper databaseWrapper) {
                for (T item : items) {
                    item.delete(databaseWrapper);
                    syncResult.stats.numDeletes++;
                }

            }
        });

        // notify that the transaction has ended
        observer.endTransactionAndNotify();
    }
}
