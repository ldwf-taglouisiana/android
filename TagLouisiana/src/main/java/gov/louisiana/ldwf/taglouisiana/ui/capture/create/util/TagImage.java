package gov.louisiana.ldwf.taglouisiana.ui.capture.create.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.SystemClock;

import com.google.gson.annotations.Expose;

import java.io.ByteArrayOutputStream;

/**
 * Created by danielward on 11/8/16.
 */
public class TagImage implements Comparable<TagImage> {
    @Expose
    private String tagNumber;

    private Bitmap image;

    @Expose
    private String filePath;

    @Expose
    private Long timeStamp;

    public TagImage(String tagNumber, Bitmap image, String filePath) {
        this.tagNumber = tagNumber;
        this.image = image;
        this.filePath = filePath;
        this.timeStamp = SystemClock.currentThreadTimeMillis();
    }

    public String getTagNumber() {
        return tagNumber;
    }

    public Bitmap getImage() {
        if (image == null) {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//                bmOptions.inJustDecodeBounds = true;
            image = BitmapFactory.decodeFile(filePath, bmOptions);
        }

        return image;
    }

    public byte[] asByteArray() {
        if (getImage() != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            getImage().compress(Bitmap.CompressFormat.JPEG, 100, stream);
            return stream.toByteArray();
        } else {
            return new byte[0];
        }
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TagImage) {
            TagImage other = (TagImage) obj;
            return this.filePath.equals(other.filePath) && this.tagNumber.equals(other.tagNumber);
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(TagImage tagImage) {
        return timeStamp.compareTo(tagImage.getTimeStamp());
    }
}
