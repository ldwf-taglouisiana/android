package gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.structure.BaseModel;

import org.apache.commons.lang3.NotImplementedException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nullable;

import gov.louisiana.ldwf.taglouisiana.gson.responses.BaseCombinedResponse;
import gov.louisiana.ldwf.taglouisiana.gson.responses.BaseResponse;

/**
 * Created by danielward on 8/25/16.
 */
public class CombinedResponse extends BaseResponse implements BaseCombinedResponse {

    @Expose
    @SerializedName("captures")
    public CaptureResponse captureResponse;

    @Expose
    @SerializedName("draft_captures")
    public PendingCaptureResponse pendingCaptureResponse;

    @Expose
    @SerializedName("tags")
    public TagResponse tagResponse;

    @Expose
    @SerializedName("me")
    public MeResponse meResponse;

    @Override
    public Class getModelClass() {
        return BaseModel.class;
    }

    @Override
    public <T extends BaseModel> List<T> getItems() {
        throw new NotImplementedException("Not Implemented in the Combined Response");
    }

    public Collection<BaseResponse> getResponses() {
        ArrayList<BaseResponse> results = new ArrayList<>(4);

        results.add(captureResponse);
        results.add(pendingCaptureResponse);
        results.add(tagResponse);
        results.add(meResponse);

        // reject any lists that are empty
        return Collections2.filter(results, new Predicate<BaseResponse>() {
            @Override
            public boolean apply(@Nullable BaseResponse input) {
                return input != null && !input.getItems().isEmpty();
            }
        });
    }
}
