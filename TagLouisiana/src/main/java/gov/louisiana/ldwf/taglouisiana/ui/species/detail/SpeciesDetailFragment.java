package gov.louisiana.ldwf.taglouisiana.ui.species.detail;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.astuetz.PagerSlidingTabStrip;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.models.species.Species;
import gov.louisiana.ldwf.taglouisiana.models.species.Species_Table;
import gov.louisiana.ldwf.taglouisiana.net.ApiUtils;
import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;

/**
 * Created with IntelliJ IDEA.
 * User: daniel
 * Date: 2/20/13
 * Time: 1:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class SpeciesDetailFragment extends BaseFragment {

    private Species species;
    private PagerAdapter pagerAdapter;

    public static SpeciesDetailFragment newInstance(long speciesId) {

        Bundle args = new Bundle();
        args.putLong(SpeciesDetailActivity.SPECIES_ID_ARG, speciesId);

        SpeciesDetailFragment fragment = new SpeciesDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static SpeciesDetailFragment newInstance(Species species) {
        return SpeciesDetailFragment.newInstance(species._id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.species_detail_fragment, container, false);
        final AQuery aQuery = new AQuery(view);

        init();

        getBaseActivity().getToolbar().setTitle(species.commonName);

        List<BaseFragment> fragments = new ArrayList<>(4);

        fragments.add(SpeciesNameFragment.newInstance("Names", species._id));

        if (!TextUtils.isEmpty(species.generalDescription))
            fragments.add(SpeciesTextFragment.newInstance("Description", species.generalDescription));
        if (!TextUtils.isEmpty(species.habitatDescription))
            fragments.add(SpeciesTextFragment.newInstance("Habitat", species.habitatDescription));
        if (!TextUtils.isEmpty(species.sizeDescription))
            fragments.add(SpeciesTextFragment.newInstance("Size", species.sizeDescription));
        if (!TextUtils.isEmpty(species.foodValueDescription))
            fragments.add(SpeciesTextFragment.newInstance("Food Value", species.foodValueDescription));

        pagerAdapter = new PagerAdapter(this.getChildFragmentManager(), fragments);

        ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);

        PagerSlidingTabStrip strip = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
        strip.setViewPager(pager);

        aQuery.id(R.id.hero_image).progress(R.id.progress_view).image(ApiUtils.urlWithHost(species.imageUrl).toString(), true, true, 0, 0, new BitmapAjaxCallback() {

            @Override
            public void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                iv.setImageBitmap(bm);

                Palette.from(bm).generate(new Palette.PaletteAsyncListener() {
                    @Override
                    public void onGenerated(Palette palette) {
                        getBaseActivity().getToolbar().setBackgroundColor(palette.getMutedColor(getActivity().getResources().getColor(R.color.tagging_orange)));
                    }
                });
            }

        });

        return view;
    }

    private void init() {
        this.species = SQLite.select()
                .from(Species.class)
                .where(Species_Table._id.eq(getArguments().getLong(SpeciesDetailActivity.SPECIES_ID_ARG)))
                .querySingle();
    }

    //----------------------------------------------------------------------------------------------
    // PAGER ADAPTER

    private class PagerAdapter extends FragmentStatePagerAdapter {

        private List<BaseFragment> fragments;

        public PagerAdapter(FragmentManager fm, List<BaseFragment> fragments) {
            super(fm);

            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragments.get(position).getTitle();
        }
    } // end pager adapter
}
