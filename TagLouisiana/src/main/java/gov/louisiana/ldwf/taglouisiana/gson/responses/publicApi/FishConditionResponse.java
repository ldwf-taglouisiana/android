package gov.louisiana.ldwf.taglouisiana.gson.responses.publicApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.gson.responses.BaseResponse;
import gov.louisiana.ldwf.taglouisiana.models.options.FishConditionOption;

/**
 * Created by danielward on 8/25/16.
 */
public class FishConditionResponse extends BaseResponse {
    @Expose
    @SerializedName("items")
    public List<FishConditionOption> items;

    @Override
    public Class getModelClass() {
        return FishConditionOption.class;
    }

    public List<FishConditionOption> getItems() {
        if (items == null) {
            items = new ArrayList<>(0);
        }

        return items;
    }
}
