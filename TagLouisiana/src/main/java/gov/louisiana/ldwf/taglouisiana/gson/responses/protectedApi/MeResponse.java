package gov.louisiana.ldwf.taglouisiana.gson.responses.protectedApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.gson.responses.BaseResponse;
import gov.louisiana.ldwf.taglouisiana.models.profile.Angler;
import gov.louisiana.ldwf.taglouisiana.models.profile.Profile;
import gov.louisiana.ldwf.taglouisiana.models.profile.User;

/**
 * Created by danielward on 8/25/16.
 */
public class MeResponse extends BaseResponse {
    @Expose
    @SerializedName("user")
    public User user;

    @Expose
    @SerializedName("angler")
    public Angler angler;

    private Profile profile;

    private List<BaseModel> items;

    @Override
    public Class getModelClass() {
        return BaseModel.class;
    }

    public MeResponse() {
        profile = SQLite.select().from(Profile.class).querySingle();
    }

    public List<BaseModel> getItems() {
        if (items == null) {
            items = new ArrayList<>(0);

            if (angler != null) {
                items.add(angler);
            }

            if (user != null) {
                items.add(user);
            }

            if (profile == null) {
                profile = new Profile();
            }

            profile.angler = angler;
            profile.user = user;
            profile.updatedAt = this.version;

            items.add(profile);
        }

        return items;
    }
}
