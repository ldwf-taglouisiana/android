package gov.louisiana.ldwf.taglouisiana.ui.util.stats;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import gov.louisiana.ldwf.taglouisiana.gson.GsonUtil;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

/**
 * This is a utility class to help parsing the stats/dashboard information from the Narwhal API.
 */
public class StatsParser {
    private static final String DATE_FORMAT = "dd-MMM-yyyy";
    private static final String DECIMAL_FORMAT = "#,###,###";
    private static final DecimalFormat DECIMAL_FORMATTER = new DecimalFormat(DECIMAL_FORMAT);

    /**
     *
     * @param json
     * @return
     */
    public static List<StatsItem> parsePublic(String json) {
        List<StatsItem> result = new LinkedList<>();

        // convert the string to a navigable jSON object
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(json).getAsJsonObject();

        /*
            Get the season start and end times
         */
        Long currentSeasonStartMs     = GsonUtil.getElementFromPath(jsonObject, "season/current/start/epoch").getAsLong() * 1000;
        Long currentSeasonEndMs       = GsonUtil.getElementFromPath(jsonObject, "season/current/end/epoch").getAsLong() * 1000;
        Long previousSeasonStartMs    = GsonUtil.getElementFromPath(jsonObject, "season/previous/start/epoch").getAsLong() * 1000;
        Long previousSeasonEndMs      = GsonUtil.getElementFromPath(jsonObject, "season/previous/end/epoch").getAsLong() * 1000;

        /*
            Get the program statistics. mainly the counts
         */
        result.add(
                new StatsItem.Builder("Program Totals")
                        .setItem("Captures Overall", getLongAsFormatted(jsonObject, "captures/total"))
                        .setItem("Recaptures Overall", getLongAsFormatted(jsonObject, "recaptures/total"))
                        .setItem("Captures this season", getDateRange(currentSeasonStartMs, currentSeasonEndMs), getLongAsFormatted(jsonObject, "captures/season/current"))
                        .setItem("Recaptures this season", getDateRange(currentSeasonStartMs, currentSeasonEndMs), getLongAsFormatted(jsonObject, "recaptures/season/current"))
                        .setItem("Captures last season", getDateRange(previousSeasonStartMs, previousSeasonEndMs), getLongAsFormatted(jsonObject, "captures/season/previous"))
                        .setItem("Recaptures last season", getDateRange(previousSeasonStartMs, previousSeasonEndMs), getLongAsFormatted(jsonObject, "recaptures/season/previous"))
                        .build()
        );

        /*
            Add the species list for captures
         */
        result.add(
                parseSpeciesListItem(jsonObject, "Captures by Species (Last 30 days)", "captures/species")
        );

        /*
            Add the species list for recaptures
         */
        result.add(
                parseSpeciesListItem(jsonObject, "Recaptures by Species (Last 30 days)", "recaptures/species")
        );

        return result;
    }

    public static List<StatsItem> parseProtected(String json) {
        List<StatsItem> result = new LinkedList<>();

        // convert the string to a navigable jSON object
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(json).getAsJsonObject();

        /*
            Get the season start and end times
         */
        Long currentSeasonStartMs     = GsonUtil.getElementFromPath(jsonObject, "program/season/current/start/epoch").getAsLong() * 1000;
        Long currentSeasonEndMs       = GsonUtil.getElementFromPath(jsonObject, "program/season/current/end/epoch").getAsLong() * 1000;
        Long previousSeasonStartMs    = GsonUtil.getElementFromPath(jsonObject, "program/season/previous/start/epoch").getAsLong() * 1000;
        Long previousSeasonEndMs      = GsonUtil.getElementFromPath(jsonObject, "program/season/previous/end/epoch").getAsLong() * 1000;

        /*
            Get the angler statistics. mainly the counts
         */
        result.add(
                new StatsItem.Builder("Your Totals")
                        .setItem("Captures Overall", getLongAsFormatted(jsonObject, "angler/captures/total"))
                        .setItem("Recaptures Overall", getLongAsFormatted(jsonObject, "angler/recaptures/total"))
                        .setItem("Captures this season", getDateRange(currentSeasonStartMs, currentSeasonEndMs), getLongAsFormatted(jsonObject, "angler/captures/season/current"))
                        .setItem("Recaptures this season", getDateRange(currentSeasonStartMs, currentSeasonEndMs), getLongAsFormatted(jsonObject, "angler/recaptures/season/current"))
                        .setItem("Captures last season", getDateRange(previousSeasonStartMs, previousSeasonEndMs), getLongAsFormatted(jsonObject, "angler/captures/season/previous"))
                        .setItem("Recaptures last season", getDateRange(previousSeasonStartMs, previousSeasonEndMs), getLongAsFormatted(jsonObject, "angler/recaptures/season/previous"))
                        .build()
        );

        /*
            Add the species list for captures
         */
        result.add(
                parseSpeciesListItem(jsonObject, "Captures by Species (Last 30 days)", "angler/captures/species")
        );

        /*
            Add the species list for captures
         */
        result.add(
                parseSpeciesListItem(jsonObject, "Recaptures by Species (Last 30 days)", "angler/recaptures/species")
        );

        return result;
    }


    /**
     * Calls {@link #getDateRange(Long startTimeMs, Long endTimeMs, Calendar calendar)}, using {@link Calendar#getInstance()} as the calendar.
     *
     * @param startTimeMs see {@link #getDateRange(Long startTimeMs, Long endTimeMs, Calendar calendar)}
     * @param endTimeMs see {@link #getDateRange(Long startTimeMs, Long endTimeMs, Calendar calendar)}
     * @see #getDateRange(Long startTimeMs, Long endTimeMs, Calendar calendar)
     * @return
     */
    private static String getDateRange(Long startTimeMs, Long endTimeMs) {
        return getDateRange(startTimeMs, endTimeMs, Calendar.getInstance());
    }

    /**
     * Takes the 2 long parameters and creates a date range string <tt>({@value #DATE_FORMAT} - {@value #DATE_FORMAT})</tt>.
     * Uses the calendar instance to use the local calendar information of the device.
     *
     * @param startTimeMs The first time, in milliseconds since epoch UTC.
     * @param endTimeMs The second time, in milliseconds since epoch UTC.
     * @param calendar The calendar instance to use.
     * @return The date range string. <tt>({@value #DATE_FORMAT} - {@value #DATE_FORMAT})</tt>
     */
    private static String getDateRange(Long startTimeMs, Long endTimeMs, Calendar calendar) {
        SimpleDateFormat simpleDateFormat = Constants.US_DATE_FORMATTER;

        calendar.setTimeInMillis(startTimeMs);
        Date startDate = calendar.getTime();

        calendar.setTimeInMillis(endTimeMs);
        Date endDate = calendar.getTime();

        return "(" + simpleDateFormat.format(startDate) + " - " + simpleDateFormat.format(endDate) + ")";
    }

    /**
     * Gets a value in the given <tt>jsonObject</tt> at the given <tt>path</tt> in the decimal format of {@value #DECIMAL_FORMAT}.
     *
     * @param jsonObject The jsonObject in which the desired long item exits
     * @param path The string path in the <tt>jsonObject</tt> item. See {@link GsonUtil#getElementFromPath(JsonObject, String)} for more information.
     * @return The long at the given path in theformat of {@value #DECIMAL_FORMAT}, null if the element at the path does not exist
     */
    private static String getLongAsFormatted(JsonObject jsonObject, String path) {
        return DECIMAL_FORMATTER.format(GsonUtil.getElementFromPath(jsonObject, path).getAsLong());
    }

    private static StatsItem parseSpeciesListItem(JsonObject jsonObject, String title, String pathToSpeciesList) {
        StatsItem.Builder builder = new StatsItem.Builder(title);
        for (JsonElement speciesElement : GsonUtil.getElementFromPath(jsonObject, pathToSpeciesList).getAsJsonArray()) {
            builder.setItem(
                speciesElement.getAsJsonObject().getAsJsonPrimitive("name").getAsString(),
                getLongAsFormatted(speciesElement.getAsJsonObject(), "count")
            );
        }

        return builder.build();
    }
}
