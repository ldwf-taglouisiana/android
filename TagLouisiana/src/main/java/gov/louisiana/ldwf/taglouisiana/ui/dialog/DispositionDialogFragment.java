package gov.louisiana.ldwf.taglouisiana.ui.dialog;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.SimpleCursorAdapter;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import gov.louisiana.ldwf.taglouisiana.models.options.RecaptureDispositionOption;
import gov.louisiana.ldwf.taglouisiana.ui.core.dialog.BaseCursorListDialogFragment;
import gov.louisiana.ldwf.taglouisiana.utils.Utils;

public class DispositionDialogFragment extends BaseCursorListDialogFragment {

    public static final int REQUEST_CODE = Utils.getFreshInt();
    public static final String INTENT_DISPOSITION_ID = "DispositionDialogFragment.intent.disposition.id";

    public static DispositionDialogFragment newInstance(Fragment targetFragment) {
        Bundle args = new Bundle();

        DispositionDialogFragment fragment = new DispositionDialogFragment();
        fragment.setTargetFragment(targetFragment, REQUEST_CODE);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public String getTitle() {
        return "Recapture Dispositions";
    }

    @Override
    public Intent createResultIntent(Long item) {
        Intent intent = new Intent();
        intent.putExtra(INTENT_DISPOSITION_ID, item);

        return intent;
    }

    @Override
    public Cursor getItemListCursor() {
        return SQLite.select().from(RecaptureDispositionOption.class).flowQueryList().cursor();
    }

    @Override
    public SimpleCursorAdapter getCursorAdapter() {
        return getCursorAdapter(new String[] { "description" }, new int[] { android.R.id.text1 });
    }
}