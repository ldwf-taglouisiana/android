package gov.louisiana.ldwf.taglouisiana.ui.capture.create.util;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polygon;

/**
 * Created by daniel on 5/23/14.
 */
public interface MapFragmentAddons {

    public void setLocationChangedListener(LocationChangedListener listener);

    public void updatedMarkerPosition(float latitude, float longitude);

    public void clearAllMarkers();

    public void addMarker(Marker marker);

    public void addMarker(Marker marker, String title, int iconId);

    public void addMarker(float latitude, float longitude);

    public void addMarker(float latitude, float longitude, String title, int iconId);

    public void addPolygon(Polygon polygon, Polygon options);

    public void fitAllMarkers();

    public void moveMapTo(double latitude, double longitude);
}
