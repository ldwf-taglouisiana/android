package gov.louisiana.ldwf.taglouisiana.gson.typeAdapters;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Created by danielward on 8/24/16.
 */
public abstract class BeforeAfterTypeAdapterFactory<C> implements TypeAdapterFactory {
    private final Class<C> customizedClass;

    public BeforeAfterTypeAdapterFactory(Class<C> customizedClass) {
        this.customizedClass = customizedClass;
    }

    @SuppressWarnings("unchecked") // we use a runtime check to guarantee that 'C' and 'T' are equal
    public final <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
//        boolean m  =  type.getRawType().isInstance(customizedClass);
//        boolean n  =  customizedClass.isInstance(type.getRawType());
//        boolean k  =  customizedClass.isAssignableFrom(type.getRawType());
//        if (type.getRawType() == customizedClass || customizedClass.isInstance(type.getRawType())) {
        if (type.getRawType() == customizedClass || customizedClass.isAssignableFrom(type.getRawType())) {
            return (TypeAdapter<T>) customizeMyClassAdapter(gson, (TypeToken<C>) type);
        }
        else {
            return null;
        }
    }

    private TypeAdapter<C> customizeMyClassAdapter(Gson gson, TypeToken<C> type) {
        final TypeAdapter<C> delegate = gson.getDelegateAdapter(this, type);
        final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);
        return new TypeAdapter<C>() {
            @Override
            public void write(JsonWriter out, C value) throws IOException {
                JsonElement tree = delegate.toJsonTree(value);
                beforeWrite(value, tree);
                elementAdapter.write(out, tree);
            }

            @Override
            public C read(JsonReader in) throws IOException {
                JsonElement tree = elementAdapter.read(in);
                afterRead(tree);
                return delegate.fromJsonTree(tree);
            }
        };
    }

    /**
     * Override this to muck with {@code toSerialize} before it is written to
     * the outgoing JSON stream.
     */
    protected void beforeWrite(C source, JsonElement toSerialize) {
    }

    /**
     * Override this to muck with {@code deserialized} before it parsed into
     * the application type.
     */
    protected void afterRead(JsonElement deserialized) {
    }
}