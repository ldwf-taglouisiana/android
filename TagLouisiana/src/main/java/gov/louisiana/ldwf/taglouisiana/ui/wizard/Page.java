package gov.louisiana.ldwf.taglouisiana.ui.wizard;


import gov.louisiana.ldwf.taglouisiana.ui.core.BaseFragment;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

public abstract class Page extends BaseFragment {

    private String sharedPrefs;

    @Override
    public void onResume() {
        super.onResume();
        restoreValues();
        updateView();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getContext() != null) {
            commitValues();
        }
    }

    public String getSharedPrefs() {
        return sharedPrefs;
    }

    public void setSharedPrefs(String sharedPrefs) {
        this.sharedPrefs = sharedPrefs;
    }

    public void onReset() {
        // left blank
    }

    public abstract void restoreValues();

    public abstract void commitValues();

    public abstract void updateView();

    public boolean isValid() {
        return true; // defaults to true
    }

    public static abstract class Builder {
        String prefs;

        public Builder() {
            prefs = Constants.SHARED_PREFS_TAG_LOUISIANA;
        }

        public void setPrefs(String prefs) {
            this.prefs = prefs;
        }

        public String getPrefs() {
            return prefs;
        }

        public abstract Page create();
    }
}
