package gov.louisiana.ldwf.taglouisiana.ui.dialog;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.timeEntry.EnteredTimeOnWaterEntry;
import gov.louisiana.ldwf.taglouisiana.ui.core.dialog.BaseDialogFragment;
import gov.louisiana.ldwf.taglouisiana.ui.util.MaterialDialogBuilder;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

/**
 * Created with IntelliJ IDEA.
 * User: danielward
 * Date: 3/15/13
 * Time: 11:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class VolunteerHoursDialogFragment extends BaseDialogFragment {

    private static final String FRAG_TAG_DATE_PICKER = "VolunteerHoursDialogFragment.Fragment.date_picker";
    private static final String FRAG_TAG_TIME_PICKER = "VolunteerHoursDialogFragment.Fragment.time_picker";

    private View view;
    private boolean certified = false;

    private Calendar start = Calendar.getInstance();
    private Calendar end = Calendar.getInstance();

    private SimpleDateFormat timeFormatter;
    private SimpleDateFormat dateFormatter;

    public static BaseDialogFragment newInstance() {
        return new VolunteerHoursDialogFragment();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        this.view = getContentView();

        MaterialDialogBuilder builder = new MaterialDialogBuilder(getActivity());

        builder.setTitle("Enter Time On Water");
        builder.setContent(this.view);

        builder.setPositiveButton("SUBMIT", new MaterialDialogBuilder.OnMaterialDialogItemClickListener() {
            @Override
            public void onClick(Dialog dialog) {
                EnteredTimeOnWaterEntry log = buildLog(start, end);
                if(log.verified) {
                    AsyncSubmitVolunteerHours asyncSubmit = new AsyncSubmitVolunteerHours(log, dialog);
                    asyncSubmit.execute();
                } else{
                    Toast.makeText(getContext(), "Please certify that the information given is correct before submitting", Toast.LENGTH_LONG).show();
                }
            }
        });

        builder.setNegativeButton("DISMISS", new MaterialDialogBuilder.OnMaterialDialogItemClickListener() {
            @Override
            public void onClick(Dialog dialog) {
                dialog.dismiss();
            }
        });

        return builder.create();
    }

    @Override
    public String getTitle() {
        return "Enter Time on Water";
    }


    private EnteredTimeOnWaterEntry buildLog(Calendar start, Calendar end) {
        EnteredTimeOnWaterEntry log = new EnteredTimeOnWaterEntry();
        log.startDate = start.getTime();
        log.endDate = end.getTime();
        log.verified = this.certified;

        return log;
    }

    private void sendVolunteerHours(final EnteredTimeOnWaterEntry log) {

        DatabaseDefinition database;
        database = FlowManager.getDatabase(AppDatabase.class);
        Transaction transaction = database.beginTransactionAsync(new ITransaction() {
            @Override
            public void execute(DatabaseWrapper databaseWrapper) {
                log.save(databaseWrapper);
            }
        }).success(new Transaction.Success() {
            @Override
            public void onSuccess(Transaction transaction) {
                getBaseActivity().requestSync();
            }
        }).build();
        transaction.execute(); // execute
    }

    @Override
    public View getContentView() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.volunteer_hours_dialog_fragment, null);

        AQuery aq = new AQuery(view);

        initDateFormatter();
        initDates();

        aq.id(R.id.date).text(Constants.US_DATE_FORMATTER.format(start.getTime()));
        aq.id(R.id.start_time).text(timeFormatter.format(start.getTime()));
        aq.id(R.id.end_time).text(timeFormatter.format(end.getTime()));

        aq.id(R.id.certified_checkbox).clicked(CertifyButtonListener);
        aq.id(R.id.date_container).clicked(DateButtonListener);
        aq.id(R.id.start_container).clicked(TimeButtonListener);
        aq.id(R.id.end_container).clicked(TimeButtonListener);
        aq.id(R.id.certified_checkbox).text("Yes");

        return view;
    }

    private void initDateFormatter() {
        if (DateFormat.is24HourFormat(getActivity())) {
            timeFormatter = new SimpleDateFormat("H:mm");
        } else {
            timeFormatter = new SimpleDateFormat("h:mm a");
        }

        dateFormatter = Constants.US_DATE_FORMATTER;
    }

    private void initDates() {
        int minutes = start.get(Calendar.MINUTE);
        minutes = (minutes / 30) < 1 ? 0 : 30;
        start.set(Calendar.MINUTE, minutes);
        end.set(Calendar.MINUTE, minutes);
        end.add(Calendar.HOUR, 1);
    }

    // ---------------------------------------------------------------------------------------------

    private View.OnClickListener CertifyButtonListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            certified = !certified;
        }
    };

    private View.OnClickListener DateButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            MonthAdapter.CalendarDay minDate = new MonthAdapter.CalendarDay(getPreviousYears(2));
            MonthAdapter.CalendarDay maxDate = new MonthAdapter.CalendarDay(Calendar.getInstance());

            CalendarDatePickerDialogFragment datePickerFragment = new CalendarDatePickerDialogFragment()
                    .setOnDateSetListener(new CalendarDatePickerDialogFragment.OnDateSetListener() {
                        @Override
                        public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
                            Calendar[] calendars = new Calendar[]{start, end};

                            for (Calendar calendar : calendars) {
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            }

                            AQuery aQuery = new AQuery(VolunteerHoursDialogFragment.this.view);
                            aQuery.id(R.id.date).text(dateFormatter.format(start.getTime()));
                        }
                    })
                    .setFirstDayOfWeek(Calendar.SUNDAY)
                    .setPreselectedDate(start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH))
                    .setDateRange(minDate, maxDate)
                    .setDoneText("SELECT")
                    .setCancelText("CANCEL");

            datePickerFragment.show(getChildFragmentManager(), FRAG_TAG_DATE_PICKER);

        }
    };
    private View.OnClickListener TimeButtonListener = new View.OnClickListener() {

        @Override
        public void onClick(final View view) {
            final int viewId = view.getId();

            Calendar selectedCalendar;

            if (viewId == R.id.start_container) {
                selectedCalendar = start;
            } else {
                selectedCalendar = end;
            }

            RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                    .setOnTimeSetListener(new RadialTimePickerDialogFragment.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
                            Calendar calendar;

                            if (viewId == R.id.start_container) {
                                calendar = start;
                            } else {
                                calendar = end;
                            }

                            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            calendar.set(Calendar.MINUTE, minute);

                            AQuery aQuery = new AQuery(VolunteerHoursDialogFragment.this.view);

                            if (viewId == R.id.start_container) {
                                aQuery.id(R.id.start_time).text(timeFormatter.format(calendar.getTime()).toLowerCase());
                            } else {
                                aQuery.id(R.id.end_time).text(timeFormatter.format(calendar.getTime()).toLowerCase());
                            }
                        }
                    })
                    .setStartTime(selectedCalendar.get(Calendar.HOUR_OF_DAY), selectedCalendar.get(Calendar.MINUTE))
                    .setDoneText("SELECT")
                    .setCancelText("CANCEL");

            rtpd.show(getChildFragmentManager(), FRAG_TAG_TIME_PICKER);
        }
    };

    private Calendar getPreviousYears(int yearOffset) {
        Calendar prevYear = Calendar.getInstance();
        prevYear.add(Calendar.YEAR, -1 * yearOffset);
        return prevYear;
    }


    private class AsyncSubmitVolunteerHours extends AsyncTask<Void, Void, Void>{
        private EnteredTimeOnWaterEntry log;
        private Dialog dialog;

        public AsyncSubmitVolunteerHours(EnteredTimeOnWaterEntry givenLog, Dialog givenDialog){
            this.log = givenLog;
            this.dialog = givenDialog;
        }

        @Override
        protected Void doInBackground(Void... params) {
            sendVolunteerHours(log);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(VolunteerHoursDialogFragment.this.getContext(), "Time on Water Submitted", Toast.LENGTH_LONG).show();
            this.dialog.dismiss();
        }
    }
}

