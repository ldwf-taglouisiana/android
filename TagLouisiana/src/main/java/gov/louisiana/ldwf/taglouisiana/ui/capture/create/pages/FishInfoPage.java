package gov.louisiana.ldwf.taglouisiana.ui.capture.create.pages;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.androidquery.AQuery;
import com.raizlabs.android.dbflow.structure.database.transaction.QueryTransaction;

import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import gov.louisiana.ldwf.taglouisiana.R;
import gov.louisiana.ldwf.taglouisiana.models.options.RecaptureDispositionOption;
import gov.louisiana.ldwf.taglouisiana.models.species.Species;
import gov.louisiana.ldwf.taglouisiana.ui.capture.create.EnterCapturePage;
import gov.louisiana.ldwf.taglouisiana.ui.dialog.DispositionDialogFragment;
import gov.louisiana.ldwf.taglouisiana.ui.dialog.SpeciesDialogFragment;
import gov.louisiana.ldwf.taglouisiana.ui.util.MaterialDialogBuilder;
import gov.louisiana.ldwf.taglouisiana.ui.wizard.Page;
import gov.louisiana.ldwf.taglouisiana.utils.Constants;

public class FishInfoPage extends EnterCapturePage {

    private static final String KEY_RECAPTURE_ARG = "is_recapture";

    // loader management
    private List<Integer> doneLoaders;

    private AQuery aq;

    // form input data
    private Species species;
    private RecaptureDispositionOption recaptureDisposition;
    private Handler handler;

    public FishInfoPage() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.enter_capture_fish_info_fragment, container, false);
        aq = new AQuery(getActivity(), myFragmentView);

        aq.id(R.id.species_button).clicked(this, "speciesButtonClicked");
        aq.id(R.id.disposition_button).clicked(this, "dispositionButtonClicked");

        return myFragmentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        initCoachMarks(Constants.FISH_INFO_MARKS_SHOWN);
    }

    @Override
    public void onResume() {
        super.onResume();

        boolean recaptureArgument = (getArguments() != null && getArguments().getBoolean(KEY_RECAPTURE_ARG, false));
        boolean recaptureMarked = this.getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE).getBoolean(Constants.ENTER_CAPTURE_RECAPTURE, false);

        if (recaptureArgument || recaptureMarked) {
            aq.id(R.id.dispostion_text_view).visible();
            aq.id(R.id.disposition_button_container).visible();
        } else {
            aq.id(R.id.dispostion_text_view).gone();
            aq.id(R.id.disposition_button_container).gone();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SpeciesDialogFragment.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                getModelByIdAsync(Species.class, data.getLongExtra(SpeciesDialogFragment.INTENT_SPECIES_ID, -1), speciesQueryResultSingleCallback);
            }
        }

        if (requestCode == DispositionDialogFragment.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                getModelByIdAsync(RecaptureDispositionOption.class, data.getLongExtra(DispositionDialogFragment.INTENT_DISPOSITION_ID, -1), recaptureDispositionOptionQueryResultSingleCallback);
            }
        }
    }

    // ===============================================================================================
    // Click callbacks


    public void speciesButtonClicked(View v) {
        removeValidationBorder(v);

        FragmentManager fm = getChildFragmentManager();
        DialogFragment dialogFragment = SpeciesDialogFragment.newInstance(this);
        dialogFragment.show(fm, "FishInfoPage.dialog.species");
    }

    public void dispositionButtonClicked(View v) {
        removeValidationBorder(v);

        FragmentManager fm = getChildFragmentManager();
        DialogFragment dialogFragment = DispositionDialogFragment.newInstance(this);
        dialogFragment.show(fm, "FishInfoPage.dialog.dispositions");
    }

    @Override
    public void updateView() {
        // update based on species
        handler.post(new Runnable() {
            @Override
            public void run() {

                if (FishInfoPage.this.species != null) {
                    aq.id(R.id.species_button).text(FishInfoPage.this.species.commonName);
                } else {
                    MaterialDialogBuilder builder = new MaterialDialogBuilder(getActivity());
                    builder.setTitle("Species load error");
                    builder.setContent("There might have been an issue downloading the species list. Please try again is a few minutes, to allow the data to fully download.");
                    builder.setPositiveButton("DISMISS", new MaterialDialogBuilder.OnMaterialDialogItemClickListener() {
                        @Override
                        public void onClick(Dialog dialog) {
                            dialog.dismiss();
                        }
                    });
                }


                if (FishInfoPage.this.recaptureDisposition != null) {
                    aq.id(R.id.disposition_button).text(recaptureDisposition.description);
                }

            }
        });

    }

    @Override
    public void restoreValues() {
        SharedPreferences prefs = getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE);

        /*
            Get the entered length, if entered.
            If the value is 0.0f, then we need to set the hint value, to make it easier
            for the user to enter a length
         */
        float length = prefs.getFloat(Constants.ENTER_CAPTURE_MANUAL_LENGTH, 0.0f);
        EditText lengthEditText = aq.id(R.id.length_edit_text).getEditText();

        if (length <= 0.0f) {
            lengthEditText.setHint(String.valueOf(0.0f));
        } else {
            lengthEditText.setText(String.valueOf(length));
        }

        // get the other values
        long speciesId = prefs.getLong(Constants.ENTER_CAPTURE_SPECIES, -1);
        long recaptureDispositionId = prefs.getLong(Constants.ENTER_CAPTURE_DISPOSITION, -1);

        getModelByIdAsync(Species.class, speciesId, speciesQueryResultSingleCallback);
        getModelByIdAsync(RecaptureDispositionOption.class, recaptureDispositionId, recaptureDispositionOptionQueryResultSingleCallback);

        updateView();
    }

    @Override
    public void commitValues() {
        SharedPreferences prefs = this.getActivity().getSharedPreferences(getSharedPrefs(), Context.MODE_PRIVATE);
        Editor editor = prefs.edit();

        if (this.species != null)
            editor.putLong(Constants.ENTER_CAPTURE_SPECIES, this.species._id);


        if (this.recaptureDisposition != null)
            editor.putLong(Constants.ENTER_CAPTURE_DISPOSITION, this.recaptureDisposition._id);

        editor.putFloat(Constants.ENTER_CAPTURE_MANUAL_LENGTH, getLength());

        editor.apply();
    }

    @Override
    public boolean isValid() {
        boolean result = true;

        Crouton.cancelAllCroutons();

        if(species == null){
            result = false;

            Crouton.makeText(this.getActivity(), getResources().getString(R.string.validation_species_message), Style.ALERT).show();

            addValidationBorder(aq.id(R.id.species_button).getTextView());
        }

        return result;
    }

    private float getLength() {
        if (!TextUtils.isEmpty(aq.id(R.id.length_edit_text).getEditText().getText())) {
            return Float.parseFloat(aq.id(R.id.length_edit_text).getEditText().getText().toString());
        } else {
            return 0.0f;
        }
    }

    private QueryTransaction.QueryResultSingleCallback<RecaptureDispositionOption> recaptureDispositionOptionQueryResultSingleCallback = new QueryTransaction.QueryResultSingleCallback<RecaptureDispositionOption>() {
        @Override
        public void onSingleQueryResult(QueryTransaction transaction, @Nullable RecaptureDispositionOption result) {
            FishInfoPage.this.recaptureDisposition = result;
            updateView();
        }
    };

    private QueryTransaction.QueryResultSingleCallback<Species> speciesQueryResultSingleCallback = new QueryTransaction.QueryResultSingleCallback<Species>() {
        @Override
        public void onSingleQueryResult(QueryTransaction transaction, @Nullable Species result) {
            FishInfoPage.this.species = result;
            updateView();
        }
    };


    public static class Builder extends Page.Builder {

        private boolean isRecapture;

        @Override
        public Page create() {
            Page result = new FishInfoPage();
            result.setSharedPrefs(this.getPrefs());

            Bundle bundle = new Bundle();
            bundle.putBoolean(KEY_RECAPTURE_ARG, isRecapture);

            result.setArguments(bundle);

            return result;
        }

        public void setRecapture(boolean isRecapture) {
            this.isRecapture = isRecapture;
        }
    }

}