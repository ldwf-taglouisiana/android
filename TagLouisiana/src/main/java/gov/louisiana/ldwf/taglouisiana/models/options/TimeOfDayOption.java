package gov.louisiana.ldwf.taglouisiana.models.options;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gov.louisiana.ldwf.taglouisiana.database.AppDatabase;
import gov.louisiana.ldwf.taglouisiana.models.RemoteItem;

/**
 * @author Daniel Ward <drward3@uno.edu>
 * @since 8/28/14
 */
@Table(database = AppDatabase.class)
public class TimeOfDayOption extends RemoteItem {

    @Expose
    @SerializedName("time_of_day_option")
    @Column
    public String description;


    public String getTitle() {
        // the pattern to match "([0]9:01 AM - [1]2:00 PM)"
        final String PATTERN = "(^.+?)\\s\\(";

        // create the regex pattern
        final Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(this.description);

        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return "";
        }
    }

    public String getSubtitle() {
        // the pattern to match "([0]9:01 AM - [1]2:00 PM)"
        final String PATTERN = "\\((.+?)\\)";

        // create the regex pattern
        final Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(this.description);

        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return "";
        }
    }

    public static TimeOfDayOption getCurrentTime() {
        List<TimeOfDayOption> options = SQLite.select().from(TimeOfDayOption.class).orderBy(TimeOfDayOption_Table._id, true).queryList();
        return getCurrentFromList(options);
    }

    private static TimeOfDayOption getCurrentFromList(List<TimeOfDayOption> options) {
        // get the current time
        Calendar current = Calendar.getInstance();
        current.setTimeZone(TimeZone.getTimeZone("US/Central"));

        final int LONG_DATE_LENGTH = 8;

        // the pattern to match "([0]9:01 AM - [1]2:00 PM)"
        final String DATE_PATTERN = "^.+?\\((\\d{1,2}:\\d{1,2}\\s[A-Z]{1,2}).+?(\\d{1,2}:\\d{1,2}\\s[A-Z]{1,2})\\)$";

        // create the date formatters and set the time zone to a common timezone
        // the timezone the original data source is in, is Central time.
        // need to start in central time to make sure the ranges are correct.
        final SimpleDateFormat dateFormatOne = new SimpleDateFormat("h:mm aaa");
        final SimpleDateFormat dateFormatTwo = new SimpleDateFormat("h:mm aaa");
        dateFormatOne.setTimeZone(TimeZone.getTimeZone("US/Central"));
        dateFormatTwo.setTimeZone(TimeZone.getTimeZone("US/Central"));

        // create the regex pattern
        final Pattern pattern = Pattern.compile(DATE_PATTERN);

        for (TimeOfDayOption option : options) {

            // match the option to the pattern
            Matcher matcher = pattern.matcher(option.description);

            // if there is a match
            if (matcher.matches()) {

                try {
                    // pull out the two capture groups that we matched
                    String startDateString = matcher.group(1);
                    String endDateString = matcher.group(2);

                    // create a calendar for the first regex group
                    Date startDate = startDateString.length() == LONG_DATE_LENGTH ? dateFormatOne.parse(startDateString) : dateFormatTwo.parse(startDateString);
                    Calendar startCal = Calendar.getInstance();
                    startCal.setTimeZone(TimeZone.getTimeZone("US/Central"));
                    startCal.setTime(startDate);
                    startCal.set(current.get(Calendar.YEAR), current.get(Calendar.MONTH), current.get(Calendar.DAY_OF_MONTH));

                    // create a calendar for the second regex group
                    Date endDate = endDateString.length() == LONG_DATE_LENGTH ? dateFormatOne.parse(endDateString) : dateFormatTwo.parse(endDateString);
                    Calendar endCal = Calendar.getInstance();
                    endCal.setTimeZone(TimeZone.getTimeZone("US/Central"));
                    endCal.setTime(endDate);
                    endCal.set(current.get(Calendar.YEAR), current.get(Calendar.MONTH), current.get(Calendar.DAY_OF_MONTH));

                    // compare the current time with the twe regex matched times
                    if (current.after(startCal) && current.before(endCal)) {
                        // update the time of day option to the option that matches the current time
                        return option;
                    }

                } catch (ParseException e) {
                    // this catch is for the date formatter parsing of the string
                    e.printStackTrace();
                }

            } // end matcher if

        } // end for

        // a default return in case there is an issue not being able to find the correct option.
        return options.size() == 0 ? getCurrentTime() : options.get(0);
    }

}
