-keep class * extends gov.louisiana.ldwf.taglouisiana.gson.wrappers.Wrapper { *; }
-keep class * extends gov.louisiana.ldwf.taglouisiana.models.BaseModel { *; }
-keep class * extends gov.louisiana.ldwf.taglouisiana.ui.wizard.Page { *; }

-dontwarn android.support.v7.**
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }
-keepattributes SourceFile,LineNumberTable,*Annotation*
-keep public class * extends java.lang.Exception
-printmapping mapping.txt